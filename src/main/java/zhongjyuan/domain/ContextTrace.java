package zhongjyuan.domain;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 * @className: ContextTrace
 * @description: 上下文跟踪对象
 * @author: zhongjyuan
 * @date: 2022年11月24日 下午3:04:49
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ContextTrace extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 唯一标识
	 */
	protected String id = UUID.randomUUID().toString();

	/**
	 * @fields no: 编号
	 */
	protected String no;

	/**
	 * @fields content: 正文
	 */
	protected String content;

	/**
	 * @fields className: 类名
	 */
	protected String className;

	/**
	 * @fields classSimpleName: 简要类名
	 */
	protected String classSimpleName;

	/**
	 * @fields superClassName: 父级类名
	 */
	protected String superClassName;

	/**
	 * @fields superClassSimpleName: 简要父级类名
	 */
	protected String superClassSimpleName;

	/**
	 * @fields methodName: 方法名
	 */
	protected String methodName;

	/**
	 * @fields methodParameters: 方法参数
	 */
	protected String methodParameters;

	/**
	 * @fields currentTime: 当前时间
	 */
	protected LocalDateTime currentTime = LocalDateTime.now();

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param content 正文
	 * @throws
	 */
	public ContextTrace(String content) {
		this.setContent(content);
	}

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param content 正文
	 * @param classObject 类对象
	 * @throws
	 */
	public ContextTrace(String content, Class<?> classObject) {
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}
	}

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param content 正文
	 * @param classObject 类对象
	 * @param method 方法对象
	 * @throws
	 */
	public ContextTrace(String content, Class<?> classObject, Method method) {
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}

		if (method != null) {
			this.setMethodName(method.getName());
		}
	}

	/**
	 * @title: ContextTrace
	 * @author: zhongjyuan
	 * @description: 上下文跟踪对象
	 * @param content 正文
	 * @param classObject 类对象
	 * @param methodName 方法名称
	 * @throws
	 */
	public ContextTrace(String content, Class<?> classObject, String methodName) {
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}

		if (StringUtils.isNotBlank(methodName)) {
			this.setMethodName(methodName);
		}
	}

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param no 编号
	 * @param content 正文
	 * @throws
	 */
	public ContextTrace(String no, String content) {
		this.setNo(no);
		this.setContent(content);
	}

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param no 编号
	 * @param content 正文
	 * @param classObject 类对象
	 * @throws
	 */
	public ContextTrace(String no, String content, Class<?> classObject) {
		this.setNo(no);
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}
	}

	/**
	 * @title ContextTrace
	 * @author zhongjyuan
	 * @description 上下文跟踪对象
	 * @param no 编号
	 * @param content 正文
	 * @param classObject 类对象
	 * @param method 方法对象
	 * @throws
	 */
	public ContextTrace(String no, String content, Class<?> classObject, Method method) {
		this.setNo(no);
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}

		if (method != null) {
			this.setMethodName(method.getName());
		}
	}

	/**
	 * @title: ContextTrace
	 * @author: zhongjyuan
	 * @description 上下文跟踪对象
	 * @param no 编号
	 * @param content 正文
	 * @param classObject 类对象
	 * @param methodName 方法名称
	 * @throws
	 */
	public ContextTrace(String no, String content, Class<?> classObject, String methodName) {
		this.setNo(no);
		this.setContent(content);

		if (classObject != null) {
			this.setClassName(classObject.getName());
			this.setClassSimpleName(classObject.getSimpleName());

			Class<?> superClassObject = classObject.getSuperclass();

			if (superClassObject != null) {
				this.setSuperClassName(superClassObject.getName());
				this.setSuperClassSimpleName(superClassObject.getSimpleName());
			}
		}

		if (StringUtils.isNotBlank(methodName)) {
			this.setMethodName(methodName);
		}
	}

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getNo
	 * @author: zhongjyuan
	 * @description: 获取编号
	 * @return 编号
	 * @throws
	 */
	public String getNo() {
		return no;
	}

	/**
	 * @title: setNo
	 * @author: zhongjyuan
	 * @description: 设置编号
	 * @param no 编号
	 * @throws
	 */
	public void setNo(String no) {
		this.no = no;
	}

	/**
	 * @title: getContent
	 * @author: zhongjyuan
	 * @description: 获取正文
	 * @return 正文
	 * @throws
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @title: setContent
	 * @author: zhongjyuan
	 * @description: 设置正文
	 * @param content 正文
	 * @throws
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @title: getClassName
	 * @author: zhongjyuan
	 * @description: 获取类名
	 * @return 类名
	 * @throws
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @title: setClassName
	 * @author: zhongjyuan
	 * @description: 设置类名
	 * @param className 类名
	 * @throws
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @title: getClassSimpleName
	 * @author: zhongjyuan
	 * @description: 获取简要类名
	 * @return 简要类名
	 * @throws
	 */
	public String getClassSimpleName() {
		return classSimpleName;
	}

	/**
	 * @title: setClassSimpleName
	 * @author: zhongjyuan
	 * @description: 设置简要类名
	 * @param classSimpleName 简要类名
	 * @throws
	 */
	public void setClassSimpleName(String classSimpleName) {
		this.classSimpleName = classSimpleName;
	}

	/**
	 * @title: getSuperClassName
	 * @author: zhongjyuan
	 * @description: 获取父级类名
	 * @return 父级类名
	 * @throws
	 */
	public String getSuperClassName() {
		return superClassName;
	}

	/**
	 * @title: setSuperClassName
	 * @author: zhongjyuan
	 * @description: 设置父级类名
	 * @param superClassName 父级类名
	 * @throws
	 */
	public void setSuperClassName(String superClassName) {
		this.superClassName = superClassName;
	}

	/**
	 * @title: getSuperClassSimpleName
	 * @author: zhongjyuan
	 * @description: 获取父级简要类名
	 * @return 父级简要类名
	 * @throws
	 */
	public String getSuperClassSimpleName() {
		return superClassSimpleName;
	}

	/**
	 * @title: setSuperClassSimpleName
	 * @author: zhongjyuan
	 * @description: 设置父级简要类名
	 * @param superClassSimpleName 父级简要类名
	 * @throws
	 */
	public void setSuperClassSimpleName(String superClassSimpleName) {
		this.superClassSimpleName = superClassSimpleName;
	}

	/**
	 * @title: getMethodName
	 * @author: zhongjyuan
	 * @description: 获取方法名称
	 * @return 方法名称
	 * @throws
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @title: setMethodName
	 * @author: zhongjyuan
	 * @description: 设置方法名称
	 * @param methodName 方法名称
	 * @throws
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @title: getMethodParameters
	 * @author: zhongjyuan
	 * @description: 获取方法参数
	 * @return 方法参数
	 * @throws
	 */
	public String getMethodParameters() {
		return methodParameters;
	}

	/**
	 * @title: setMethodParameters
	 * @author: zhongjyuan
	 * @description: 设置方法参数
	 * @param methodParameters 方法参数
	 * @throws
	 */
	public void setMethodParameters(String methodParameters) {
		this.methodParameters = methodParameters;
	}

	/**
	 * @title: getCurrentTime
	 * @author: zhongjyuan
	 * @description: 获取当前时间
	 * @return 当前时间
	 * @throws
	 */
	public LocalDateTime getCurrentTime() {
		return currentTime;
	}

	/**
	 * @title: setCurrentTime
	 * @author: zhongjyuan
	 * @description: 设置当前时间
	 * @param currentTime 当前时间
	 * @throws
	 */
	public void setCurrentTime(LocalDateTime currentTime) {
		this.currentTime = currentTime;
	}
}
