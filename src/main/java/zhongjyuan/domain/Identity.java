package zhongjyuan.domain;

import java.util.Map;

/**
 * @className: Identity
 * @description: 身份模型对象
 * @author: zhongjyuan
 * @date: 2022年10月26日 下午4:20:15
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Identity extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields account: 账号
	 */
	protected String account;

	/**
	 * @fields sex: 性别
	 */
	protected String sex;

	/**
	 * @fields icon: 头像
	 */
	protected String icon;

	/**
	 * @fields email: 邮箱
	 */
	protected String email;

	/**
	 * @fields mobile: 手机号码
	 */
	protected String mobile;

	/**
	 * @fields telephone: 电话号码
	 */
	protected String telephone;

	/**
	 * @fields tenantId: 租户主键
	 */
	protected String tenantId;

	/**
	 * @fields tenantCode: 租户编码
	 */
	protected String tenantCode;

	/**
	 * @fields tenantName: 租户名称
	 */
	protected String tenantName;

	/**
	 * @fields systemId: 系统主键
	 */
	protected String systemId;

	/**
	 * @fields systemCode: 系统编码
	 */
	protected String systemCode;

	/**
	 * @fields systemName: 系统名称
	 */
	protected String systemName;

	/**
	 * @fields moduleId: 模块主键
	 */
	protected String moduleId;

	/**
	 * @fields moduleCode: 模块编码
	 */
	protected String moduleCode;

	/**
	 * @fields moduleName: 模块名称
	 */
	protected String moduleName;

	/**
	 * @fields modelId: 模型主键
	 */
	protected String modelId;

	/**
	 * @fields modelCode: 模型编码
	 */
	protected String modelCode;

	/**
	 * @fields modelName: 模型名称
	 */
	protected String modelName;

	/**
	 * @fields appId: 应用主键
	 */
	protected String appId;

	/**
	 * @fields appCode: 应用编码
	 */
	protected String appCode;

	/**
	 * @fields appName: 应用名称
	 */
	protected String appName;

	/**
	 * @fields pageId: 页面主键
	 */
	protected String pageId;

	/**
	 * @fields pageCode: 页面编码
	 */
	protected String pageCode;

	/**
	 * @fields pageName: 页面名称
	 */
	protected String pageName;

	/**
	 * @fields ticket: 票据
	 */
	protected String ticket;

	/**
	 * @fields authorization: 令牌
	 */
	protected String authorization;

	/**
	 * @fields claims: 属性集
	 */
	protected Map<Object, Object> claims;

	/**
	 * @title Identity
	 * @author zhongjyuan
	 * @description 身份模型对象
	 * @throws
	 */
	public Identity() {

	}

	/**
	 * @title Identity
	 * @author zhongjyuan
	 * @description 身份模型对象
	 * @param id 主键
	 * @param name 名称
	 * @param account 账号
	 * @param tenantId 租户主键
	 * @param tenantCode 租户编码
	 * @param tenantName 租户名称
	 * @param systemId 系统主键
	 * @param systemCode 系统编码
	 * @param systemName 系统名称
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param moduleId 模块主键
	 * @param moduleCode 模块编码
	 * @param moduleName 模块名称
	 * @param modelId 模型主键
	 * @param modelCode 模型编码
	 * @param modelName 模型名称
	 * @param pageId 页面主键
	 * @param pageCode 页面编码
	 * @param pageName 页面名称
	 * @throws
	 */
	public Identity(String id, String name, String account, String tenantId, String tenantCode, String tenantName, String systemId, String systemCode, String systemName, String appId, String appCode, String appName, String moduleId, String moduleCode, String moduleName, String modelId, String modelCode, String modelName, String pageId, String pageCode, String pageName) {
		this.setId(id);
		this.setAppName(name);
		this.setAccount(account);

		this.setTenantId(tenantId);
		this.setTenantCode(tenantCode);
		this.setTenantName(tenantName);

		this.setSystemId(systemId);
		this.setSystemCode(systemCode);
		this.setSystemName(systemName);

		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setModelId(modelId);
		this.setModelCode(modelCode);
		this.setModelName(modelName);

		this.setPageId(pageId);
		this.setPageCode(pageCode);
		this.setPageName(pageName);
	}

	/**
	 * @title Identity
	 * @author zhongjyuan
	 * @description 身份模型对象
	 * @param id 主键
	 * @param name 名称
	 * @param account 账号
	 * @param tenantId 租户主键
	 * @param tenantCode 租户编码
	 * @param tenantName 租户名称
	 * @param systemId 系统主键
	 * @param systemCode 系统编码
	 * @param systemName 系统名称
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param moduleId 模块主键
	 * @param moduleCode 模块编码
	 * @param moduleName 模块名称
	 * @param modelId 模型主键
	 * @param modelCode 模型编码
	 * @param modelName 模型名称
	 * @param pageId 页面主键
	 * @param pageCode 页面编码
	 * @param pageName 页面名称
	 * @param ticket 票据
	 * @param authorization 令牌
	 * @param claims 属性集
	 * @throws
	 */
	public Identity(String id, String name, String account, String tenantId, String tenantCode, String tenantName, String systemId, String systemCode, String systemName, String appId, String appCode, String appName, String moduleId, String moduleCode, String moduleName, String modelId, String modelCode, String modelName, String pageId, String pageCode, String pageName, String ticket, String authorization, Map<Object, Object> claims) {
		this.setId(id);
		this.setAppName(name);
		this.setAccount(account);

		this.setTenantId(tenantId);
		this.setTenantCode(tenantCode);
		this.setTenantName(tenantName);

		this.setSystemId(systemId);
		this.setSystemCode(systemCode);
		this.setSystemName(systemName);

		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setModelId(modelId);
		this.setModelCode(modelCode);
		this.setModelName(modelName);

		this.setPageId(pageId);
		this.setPageCode(pageCode);
		this.setPageName(pageName);

		this.setTicket(ticket);
		this.setAuthorization(authorization);
		this.setClaims(claims);
	}

	/**
	 * @title Identity
	 * @author zhongjyuan
	 * @description 身份模型对象
	 * @param id 主键
	 * @param name 名称
	 * @param account 账号
	 * @param sex 性别
	 * @param icon 头像
	 * @param email 邮箱
	 * @param mobile 手机号码
	 * @param telephone 电话号码
	 * @param tenantId 租户主键
	 * @param tenantCode 租户编码
	 * @param tenantName 租户名称
	 * @param systemId 系统主键
	 * @param systemCode 系统编码
	 * @param systemName 系统名称
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param moduleId 模块主键
	 * @param moduleCode 模块编码
	 * @param moduleName 模块名称
	 * @param modelId 模型主键
	 * @param modelCode 模型编码
	 * @param modelName 模型名称
	 * @param pageId 页面主键
	 * @param pageCode 页面编码
	 * @param pageName 页面名称
	 * @throws
	 */
	public Identity(String id, String name, String account, String sex, String icon, String email, String mobile, String telephone, String tenantId, String tenantCode, String tenantName, String systemId, String systemCode, String systemName, String appId, String appCode, String appName, String moduleId, String moduleCode, String moduleName, String modelId, String modelCode, String modelName, String pageId, String pageCode, String pageName) {
		this.setId(id);
		this.setAppName(name);
		this.setAccount(account);

		this.setSex(sex);
		this.setIcon(icon);
		this.setEmail(email);
		this.setMobile(mobile);
		this.setTelephone(telephone);

		this.setTenantId(tenantId);
		this.setTenantCode(tenantCode);
		this.setTenantName(tenantName);

		this.setSystemId(systemId);
		this.setSystemCode(systemCode);
		this.setSystemName(systemName);

		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setModelId(modelId);
		this.setModelCode(modelCode);
		this.setModelName(modelName);

		this.setPageId(pageId);
		this.setPageCode(pageCode);
		this.setPageName(pageName);
	}

	/**
	 * @title Identity
	 * @author zhongjyuan
	 * @description 身份模型对象
	 * @param id 主键
	 * @param name 名称
	 * @param account 账号
	 * @param sex 性别
	 * @param icon 头像
	 * @param email 邮箱
	 * @param mobile 手机号码
	 * @param telephone 电话号码
	 * @param tenantId 租户主键
	 * @param tenantCode 租户编码
	 * @param tenantName 租户名称
	 * @param systemId 系统主键
	 * @param systemCode 系统编码
	 * @param systemName 系统名称
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param moduleId 模块主键
	 * @param moduleCode 模块编码
	 * @param moduleName 模块名称
	 * @param modelId 模型主键
	 * @param modelCode 模型编码
	 * @param modelName 模型名称
	 * @param pageId 页面主键
	 * @param pageCode 页面编码
	 * @param pageName 页面名称
	 * @param ticket 票据
	 * @param authorization 令牌
	 * @param claims 属性集
	 * @throws
	 */
	public Identity(String id, String name, String account, String sex, String icon, String email, String mobile, String telephone, String tenantId, String tenantCode, String tenantName, String systemId, String systemCode, String systemName, String appId, String appCode, String appName, String moduleId, String moduleCode, String moduleName, String modelId, String modelCode, String modelName, String pageId, String pageCode, String pageName, String ticket, String authorization, Map<Object, Object> claims) {
		this.setId(id);
		this.setAppName(name);
		this.setAccount(account);

		this.setSex(sex);
		this.setIcon(icon);
		this.setEmail(email);
		this.setMobile(mobile);
		this.setTelephone(telephone);

		this.setTenantId(tenantId);
		this.setTenantCode(tenantCode);
		this.setTenantName(tenantName);

		this.setSystemId(systemId);
		this.setSystemCode(systemCode);
		this.setSystemName(systemName);

		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setModelId(modelId);
		this.setModelCode(modelCode);
		this.setModelName(modelName);

		this.setPageId(pageId);
		this.setPageCode(pageCode);
		this.setPageName(pageName);

		this.setTicket(ticket);
		this.setAuthorization(authorization);
		this.setClaims(claims);
	}

	/**
	 * @title getId
	 * @description 获取主键
	 * @return 主键
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title setId
	 * @description 设置主键
	 * @param id 主键
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title getName
	 * @description 获取名称
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title setName
	 * @description 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title getAccount
	 * @description 获取账号
	 * @return 账号
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @title setAccount
	 * @description 设置账号
	 * @param account 账号
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @title getSex
	 * @description 获取性别
	 * @return 性别
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @title setSex
	 * @description 设置性别
	 * @param sex 性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @title getIcon
	 * @description 获取头像
	 * @return 头像
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @title setIcon
	 * @description 设置头像
	 * @param icon 头像
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @title getEmail
	 * @description 获取邮箱
	 * @return 邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @title setEmail
	 * @description 设置邮箱
	 * @param email 邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @title getMobile
	 * @description 获取手机号码
	 * @return 手机号码
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @title setMobile
	 * @description 设置手机号码
	 * @param mobile 手机号码
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @title getTelephone
	 * @description 获取电话号码
	 * @return 电话号码
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @title setTelephone
	 * @description 设置电话号码
	 * @param telephone 电话号码
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @title getTenantId
	 * @description 获取租户主键
	 * @return 租户主键
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @title setTenantId
	 * @description 设置租户主键
	 * @param tenantId 租户主键
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @title getTenantCode
	 * @description 获取租户编码
	 * @return 租户编码
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @title setTenantCode
	 * @description 设置租户编码
	 * @param tenantCode 租户编码
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	/**
	 * @title getTenantName
	 * @description 获取租户名称
	 * @return 租户名称
	 */
	public String getTenantName() {
		return tenantName;
	}

	/**
	 * @title setTenantName
	 * @description 设置租户名称
	 * @param tenantName 租户名称
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 * @title getSystemId
	 * @description 获取系统主键
	 * @return 系统主键
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @title setSystemId
	 * @description 设置系统主键
	 * @param systemId 系统主键
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @title getSystemCode
	 * @description 获取系统编码
	 * @return 系统编码
	 */
	public String getSystemCode() {
		return systemCode;
	}

	/**
	 * @title setSystemCode
	 * @description 设置系统编码
	 * @param systemCode 系统编码
	 */
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	/**
	 * @title getSystemName
	 * @description 获取系统名称
	 * @return 系统名称
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @title setSystemName
	 * @description 设置系统名称
	 * @param systemName 系统名称
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @title getModuleId
	 * @description 获取模块主键
	 * @return 模块主键
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @title setModuleId
	 * @description 设置模块主键
	 * @param moduleId 模块主键
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @title getModuleCode
	 * @description 获取模块编码
	 * @return 模块编码
	 */
	public String getModuleCode() {
		return moduleCode;
	}

	/**
	 * @title setModuleCode
	 * @description 设置模块编码
	 * @param moduleCode 模块编码
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**
	 * @title getModuleName
	 * @description 获取模块名称
	 * @return 模块名称
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * @title setModuleName
	 * @description 设置模块名称
	 * @param moduleName 模块名称
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * @title getModelId
	 * @description 获取模型主键
	 * @return 模型主键
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * @title setModelId
	 * @description 设置模型主键
	 * @param modelId 模型主键
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * @title getModelCode
	 * @description 获取模型编码
	 * @return 模型编码
	 */
	public String getModelCode() {
		return modelCode;
	}

	/**
	 * @title setModelCode
	 * @description 设置模型编码
	 * @param modelCode 模型编码
	 */
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	/**
	 * @title getModelName
	 * @description 获取模型名称
	 * @return 模型名称
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * @title setModelName
	 * @description 设置模型名称
	 * @param modelName 模型名称
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @title getAppId
	 * @description 获取应用主键
	 * @return 应用主键
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @title setAppId
	 * @description 设置应用主键
	 * @param appId 应用主键
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @title getAppCode
	 * @description 获取应用编码
	 * @return 应用编码
	 */
	public String getAppCode() {
		return appCode;
	}

	/**
	 * @title setAppCode
	 * @description 设置应用编码
	 * @param appCode 应用编码
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	/**
	 * @title getAppName
	 * @description 获取应用名称
	 * @return 应用名称
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @title setAppName
	 * @description 设置应用名称
	 * @param appName 应用名称
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * @title getPageId
	 * @description 获取页面主键
	 * @return 页面主键
	 */
	public String getPageId() {
		return pageId;
	}

	/**
	 * @title setPageId
	 * @description 设置页面主键
	 * @param pageId 页面主键
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	/**
	 * @title getPageCode
	 * @description 获取页面编码
	 * @return 页面编码
	 */
	public String getPageCode() {
		return pageCode;
	}

	/**
	 * @title setPageCode
	 * @description 设置页面编码
	 * @param pageCode 页面编码
	 */
	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	/**
	 * @title getPageName
	 * @description 获取页面名称
	 * @return 页面名称
	 */
	public String getPageName() {
		return pageName;
	}

	/**
	 * @title setPageName
	 * @description 设置页面名称
	 * @param pageName 页面名称
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * @title getTicket
	 * @description 获取票据
	 * @return 票据
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @title setTicket
	 * @description 设置票据
	 * @param ticket 票据
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @title getAuthorization
	 * @description 获取令牌
	 * @return 令牌
	 */
	public String getAuthorization() {
		return authorization;
	}

	/**
	 * @title setAuthorization
	 * @description 设置令牌
	 * @param authorization 令牌
	 */
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	/**
	 * @title getClaims
	 * @description 获取属性集
	 * @return 属性集
	 */
	public Map<Object, Object> getClaims() {
		return claims;
	}

	/**
	 * @title setClaims
	 * @description 设置属性集
	 * @param claims 属性集
	 */
	public void setClaims(Map<Object, Object> claims) {
		this.claims = claims;
	}
}
