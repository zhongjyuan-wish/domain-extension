package zhongjyuan.domain.database;

import java.util.Map;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: ConnectConfig
 * @description: 连接配置对象
 * @author: zhongjyuan
 * @date: 2022年11月24日 上午9:27:51
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ConnectionConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields ip: IP
	 */
	protected String ip;

	/**
	 * @fields port: 端口
	 */
	protected Integer port;

	/**
	 * @fields driver: 驱动
	 */
	protected String driver;

	/**
	 * @fields dbName: DB名称
	 */
	protected String dbName;

	/**
	 * @fields userName: 用户名
	 */
	protected String userName;

	/**
	 * @fields password: 密码
	 */
	protected String password;

	/**
	 * @fields connectString: 连接字符串
	 */
	protected String connectString;

	/**
	 * @fields properties: 属性Map集
	 */
	protected Map<String, Object> properties;

	/**
	 * @title ConnectConfig
	 * @author zhongjyuan
	 * @description 连接配置对象
	 * @throws
	 */
	public ConnectionConfigurate() {

	}

	/**
	 * @title ConnectConfig
	 * @author zhongjyuan
	 * @description 连接配置对象
	 * @param properties 配置集合
	 * @throws
	 */
	public ConnectionConfigurate(Map<String, Object> properties) {
		if (properties != null && properties.containsKey("ip"))
			setDriver(properties.get("ip").toString());

		if (properties != null && properties.containsKey("port"))
			setDbName(properties.get("port").toString());

		if (properties != null && properties.containsKey("driver"))
			setDriver(properties.get("driver").toString());

		if (properties != null && properties.containsKey("dbName"))
			setDbName(properties.get("dbName").toString());

		if (properties != null && properties.containsKey("userName"))
			setUserName(properties.get("userName").toString());

		if (properties != null && properties.containsKey("password"))
			setPassword(properties.get("password").toString());

		if (properties != null && properties.containsKey("jdbcUrl"))
			setConnectString(properties.get("jdbcUrl").toString());

		this.setProperties(properties);
	}

	/**
	 * @title:  getIp
	 * @description: 获取IP
	 * @return: IP
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @title:  setIp
	 * @description: 设置IP
	 * @param ip IP
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @title:  getPort
	 * @description: 获取端口
	 * @return: 端口
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * @title:  setPort
	 * @description: 设置端口
	 * @param port 端口
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * @title:  getDriver
	 * @description: 获取驱动
	 * @return: 驱动
	 */
	public String getDriver() {
		return driver;
	}

	/**
	 * @title:  setDriver
	 * @description: 设置驱动
	 * @param driver 驱动
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * @title:  getDbName
	 * @description: 获取DB名称
	 * @return: DB名称
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @title:  setDbName
	 * @description: 设置DB名称
	 * @param dbName DB名称
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * @title:  getUserName
	 * @description: 获取用户名
	 * @return: 用户名
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @title:  setUserName
	 * @description: 设置用户名
	 * @param userName 用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @title:  getPassword
	 * @description: 获取密码
	 * @return: 密码 
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @title:  setPassword
	 * @description: 设置密码
	 * @param password 密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @title:  getConnectString
	 * @description: 获取连接字符串
	 * @return: 连接字符串
	 */
	public String getConnectString() {
		return connectString;
	}

	/**
	 * @title:  setConnectString
	 * @description: 设置连接字符串
	 * @param connectString 连接字符串
	 */
	public void setConnectString(String connectString) {
		this.connectString = connectString;
	}

	/**
	 * @title: getPropertValue
	 * @author: zhongjyuan
	 * @description: 获取属性值
	 * @param key 键
	 * @return 值
	 * @throws
	 */
	public Object getPropertValue(String key) {
		return properties.get(key);
	}

	/**
	 * @title: setPropert
	 * @author: zhongjyuan
	 * @description: 设置属性
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void setPropert(String key, Object value) {
		properties.put(key, value);
	}

	/**
	 * @title:  getProperties
	 * @description: 获取属性Map集
	 * @return: 属性Map集
	 */
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * @title:  setProperties
	 * @description: 设置属性Map集
	 * @param properties 属性Map集
	 */
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
}
