package zhongjyuan.domain.database;

/**
 * @className: RelateShip
 * @description: 关联关系枚举对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:54:50
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum RelateShip {

	/**
	 * @fields ONE2ONE: 一对一
	 */
	ONE2ONE(1, "一对一"),

	/**
	 * @fields ONE2MORE: 一对多
	 */
	ONE2MORE(2, "一对多");

	/**
	 * @fields code: 编码
	 */
	private Integer code;

	/**
	 * @fields description: 描述
	 */
	private String description;

	/**
	 * @title: RelateShip
	 * @author: zhongjyuan
	 * @description: 关联关系枚举对象
	 * @param code 编码
	 * @param description 描述
	 * @throws
	 */
	private RelateShip(Integer code, String description) {
		this.setCode(code);
		this.setDescription(description);
	}

	/**
	 * @title: valueOf
	 * @author: zhongjyuan
	 * @description: valueOf
	 * @param value 编码[1|2]
	 * @return
	 * @throws
	 */
	public static RelateShip valueOf(int value) {
		switch (value) {
			case 1:
				return ONE2ONE;
			case 2:
				return ONE2MORE;
			default:
				return null;
		}
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @title: getDescription
	 * @author: zhongjyuan
	 * @description: 获取描述
	 * @return 描述
	 * @throws
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title: setDescription
	 * @author: zhongjyuan
	 * @description: 设置描述
	 * @param description 描述
	 * @throws
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
