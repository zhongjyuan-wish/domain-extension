package zhongjyuan.domain.database;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: TableColumn
 * @description: 表列对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:55:34
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TableColumn extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields type: 类型
	 */
	protected int type;

	/**
	 * @fields length: 长度
	 */
	protected int length;

	/**
	 * @fields precision: 精度
	 */
	protected int precision;

	/**
	 * @fields value: 值
	 */
	protected Object value;

	/**
	 * @fields defaultValue: 默认值
	 */
	protected Object defaultValue;

	/**
	 * @fields isNullable: 是否可空
	 */
	protected boolean isNullable = false;

	/**
	 * @fields isPrimaryKey: 是否主键
	 */
	protected boolean isPrimaryKey = false;

	/**
	 * @fields relateId: 关联字段主键
	 */
	protected String relateId;

	/**
	 * @fields relateCode: 关联字段编码
	 */
	protected String relateCode;

	/**
	 * @fields relateName: 关联字段名称
	 */
	protected String relateName;

	/**
	 * @fields index: 索引
	 */
	protected int index;

	/**
	 * @title: TableColumn
	 * @author: zhongjyuan
	 * @description: 表列对象
	 * @throws
	 */
	public TableColumn() {

	}

	/**
	 * @title: TableColumn
	 * @author: zhongjyuan
	 * @description: 表列对象
	 * @param index 索引
	 * @param code 编码
	 * @param type 类型
	 * @throws
	 */
	public TableColumn(int index, String code, int type) {
		this.index = index;
		this.code = code;
		this.type = type;
	}

	/**
	 * @title: TableColumn
	 * @author: zhongjyuan
	 * @description: 表列对象
	 * @param code 编码
	 * @param type 类型
	 * @param length 长度
	 * @param isNullable 是否可空
	 * @param isPrimaryKey 是否主键
	 * @throws
	 */
	public TableColumn(String code, int type, int length, boolean isNullable, boolean isPrimaryKey) {
		this.setCode(code);
		this.setType(type);
		this.setLength(length);
		this.setNullable(isNullable);
		this.setPrimaryKey(isPrimaryKey);
	}

	/**
	 * @title: TableColumn
	 * @author: zhongjyuan
	 * @description: 表列对象
	 * @param code 编码
	 * @param type 类型
	 * @param length 长度
	 * @param isNullable 是否可空
	 * @param isPrimaryKey 是否主键
	 * @param defaultValue 默认值
	 * @throws
	 */
	public TableColumn(String code, int type, int length, boolean isNullable, boolean isPrimaryKey, String defaultValue) {
		this.setCode(code);
		this.setType(type);
		this.setLength(length);
		this.setNullable(isNullable);
		this.setPrimaryKey(isPrimaryKey);
		this.setDefaultValue(defaultValue);
	}

	/**
	 * @title: TableColumn
	 * @author: zhongjyuan
	 * @description: 表列对象
	 * @param code 编码
	 * @param type 类型
	 * @param length 长度
	 * @param precision 精度
	 * @param isNullable 是否可空
	 * @param isPrimaryKey 是否主键
	 * @throws
	 */
	public TableColumn(String code, int type, int length, int precision, boolean isNullable, boolean isPrimaryKey) {
		this.setCode(code);
		this.setType(type);
		this.setLength(length);
		this.setPrecision(precision);
		this.setNullable(isNullable);
		this.setPrimaryKey(isPrimaryKey);
	}

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取主键
	 * @return 主键
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置主键
	 * @param id 主键
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getType
	 * @author: zhongjyuan
	 * @description: 获取类型
	 * @return 类型
	 * @throws
	 */
	public int getType() {
		return type;
	}

	/**
	 * @title: setType
	 * @author: zhongjyuan
	 * @description: 设置类型
	 * @param type 类型
	 * @throws
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @title: getLength
	 * @author: zhongjyuan
	 * @description: 获取长度
	 * @return 长度
	 * @throws
	 */
	public Integer getLength() {
		return length;
	}

	/**
	 * @title: setLength
	 * @author: zhongjyuan
	 * @description: 设置长度
	 * @param length 长度
	 * @throws
	 */
	public void setLength(Integer length) {
		this.length = length;
	}

	/**
	 * @title: getPrecision
	 * @author: zhongjyuan
	 * @description: 获取精度
	 * @return 精度
	 * @throws
	 */
	public Integer getPrecision() {
		return precision;
	}

	/**
	 * @title: setPrecision
	 * @author: zhongjyuan
	 * @description: 设置精度
	 * @param precision 精度
	 * @throws
	 */
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	/**
	 * @title: getValue
	 * @author: zhongjyuan
	 * @description: 获取值
	 * @return 值
	 * @throws
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @title: setValue
	 * @author: zhongjyuan
	 * @description: 设置值
	 * @param value 值
	 * @throws
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @title: getDefaultValue
	 * @author: zhongjyuan
	 * @description: 获取默认值
	 * @return 默认值
	 * @throws
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @title: setDefaultValue
	 * @author: zhongjyuan
	 * @description: 设置默认值
	 * @param defaultValue 默认值
	 * @throws
	 */
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @title: isNullable
	 * @author: zhongjyuan
	 * @description: 是否可空
	 * @return 是否可空
	 * @throws
	 */
	public boolean isNullable() {
		return isNullable;
	}

	/**
	 * @title: setNullable
	 * @author: zhongjyuan
	 * @description: 设置是否可空 
	 * @param isNullable 是否可空
	 * @throws
	 */
	public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	/**
	 * @title: isPrimaryKey
	 * @author: zhongjyuan
	 * @description: 是否主键
	 * @return 是否主键
	 * @throws
	 */
	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	/**
	 * @title: setPrimaryKey
	 * @author: zhongjyuan
	 * @description: 设置是否主键
	 * @param isPrimaryKey 是否主键
	 * @throws
	 */
	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	/**
	 * @title: getRelateId
	 * @author: zhongjyuan
	 * @description: 获取关联字段主键
	 * @return 关联字段主键
	 * @throws
	 */
	public String getRelateId() {
		return relateId;
	}

	/**
	 * @title: setRelateId
	 * @author: zhongjyuan
	 * @description: 设置关联字段主键
	 * @param relateId 关联字段主键
	 * @throws
	 */
	public void setRelateId(String relateId) {
		this.relateId = relateId;
	}

	/**
	 * @title: getRelateCode
	 * @author: zhongjyuan
	 * @description: 获取关联字段编码
	 * @return 关联字段编码
	 * @throws
	 */
	public String getRelateCode() {
		return relateCode;
	}

	/**
	 * @title: setRelateCode
	 * @author: zhongjyuan
	 * @description: 设置关联字段编码
	 * @param relateCode 关联字段编码
	 * @throws
	 */
	public void setRelateCode(String relateCode) {
		this.relateCode = relateCode;
	}

	/**
	 * @title: getRelateName
	 * @author: zhongjyuan
	 * @description: 获取关联字段名称
	 * @return 关联字段名称
	 * @throws
	 */
	public String getRelateName() {
		return relateName;
	}

	/**
	 * @title: setRelateName
	 * @author: zhongjyuan
	 * @description: 设置关联字段名称
	 * @param relateName 关联字段名称
	 * @throws
	 */
	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}

	/**
	 * @title: getIndex
	 * @author: zhongjyuan
	 * @description: 获取索引
	 * @return 索引
	 * @throws
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @title: setIndex
	 * @author: zhongjyuan
	 * @description: 设置索引
	 * @param index 索引
	 * @throws
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
