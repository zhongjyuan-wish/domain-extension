package zhongjyuan.domain.database;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @className: Database
 * @description: 数据库枚举对象
 * @author: zhongjyuan
 * @date: 2022年11月22日 下午5:59:39
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum Database {

	/**
	 * @fields MySql: MySql
	 */
	MySql("mysql", "com.mysql.jdbc.Driver"),

	/**
	 * @fields Tddl: Tddl
	 */
	Tddl("mysql", "com.mysql.jdbc.Driver"),

	/**
	 * @fields DRDS: DRDS
	 */
	DRDS("drds", "com.mysql.jdbc.Driver"),

	/**
	 * @fields Oracle: Oracle
	 */
	Oracle("oracle", "oracle.jdbc.OracleDriver"),

	/**
	 * @fields SQLServer: SQLServer
	 */
	SQLServer("sqlserver", "com.microsoft.sqlserver.jdbc.SQLServerDriver"),

	/**
	 * @fields PostgreSQL: PostgreSQL
	 */
	PostgreSQL("postgresql", "org.postgresql.Driver"),

	/**
	 * @fields DB2: DB2
	 */
	DB2("db2", "com.ibm.db2.jcc.DB2Driver"),

	/**
	 * @fields ADS: ADS
	 */
	ADS("ads", "com.mysql.jdbc.Driver"),

	/**
	 * @fields ClickHouse: ClickHouse
	 */
	ClickHouse("clickhouse", "ru.yandex.clickhouse.ClickHouseDriver"),

	/**
	 * @fields KingbaseES: KingbaseES
	 */
	KingbaseES("kingbasees", "com.kingbase8.Driver");

	/**
	 * @fields typeName: 类型名称
	 */
	private String typeName;

	/**
	 * @fields driverClassName: 驱动类名
	 */
	private String driverClassName;

	/**
	 * @fields mysqlPattern: Mysql匹配正则
	 */
	private static Pattern mysqlPattern = Pattern.compile("jdbc:mysql://(.+):\\d+/.+");

	/**
	 * @fields oraclePattern: Oracle匹配正则
	 */
	private static Pattern oraclePattern = Pattern.compile("jdbc:oracle:thin:@(.+):\\d+:.+");

	/**
	 * @title: Database
	 * @author: zhongjyuan
	 * @description: 数据库枚举对象
	 * @param typeName 类型名称
	 * @param driverClassName 驱动类名
	 * @throws
	 */
	Database(String typeName, String driverClassName) {
		this.setTypeName(typeName);
		this.setDriverClassName(driverClassName);
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String appendJDBCSuffixForReader(String jdbc) throws Exception {
		String result = jdbc;
		String suffix = null;
		switch (this) {
			case MySql:
			case DRDS:
				suffix = "yearIsDateType=false&zeroDateTimeBehavior=convertToNull&tinyInt1isBit=false&rewriteBatchedStatements=true";
				if (jdbc.contains("?")) {
					result = jdbc + "&" + suffix;
				} else {
					result = jdbc + "?" + suffix;
				}
				break;
			case Oracle:
				break;
			case SQLServer:
				break;
			case DB2:
				break;
			case PostgreSQL:
				break;
			case ClickHouse:
				break;
			case KingbaseES:
				break;
			default:
				throw new Exception("unsupported database type.");
		}

		return result;
	}

	public String appendJDBCSuffixForWriter(String jdbc) throws Exception {
		String result = jdbc;
		String suffix = null;
		switch (this) {
			case MySql:
				suffix = "yearIsDateType=false&zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true&tinyInt1isBit=false";
				if (jdbc.contains("?")) {
					result = jdbc + "&" + suffix;
				} else {
					result = jdbc + "?" + suffix;
				}
				break;
			case DRDS:
				suffix = "yearIsDateType=false&zeroDateTimeBehavior=convertToNull";
				if (jdbc.contains("?")) {
					result = jdbc + "&" + suffix;
				} else {
					result = jdbc + "?" + suffix;
				}
				break;
			case Oracle:
				break;
			case SQLServer:
				break;
			case DB2:
				break;
			case PostgreSQL:
				break;
			case ClickHouse:
				break;
			case KingbaseES:
				break;
			default:
				throw new Exception("unsupported database type.");
		}

		return result;
	}

	public String formatPk(String splitPk) throws Exception {
		String result = splitPk;

		switch (this) {
			case MySql:
			case Oracle:
				if (splitPk.length() >= 2 && splitPk.startsWith("`") && splitPk.endsWith("`")) {
					result = splitPk.substring(1, splitPk.length() - 1).toLowerCase();
				}
				break;
			case SQLServer:
				if (splitPk.length() >= 2 && splitPk.startsWith("[") && splitPk.endsWith("]")) {
					result = splitPk.substring(1, splitPk.length() - 1).toLowerCase();
				}
				break;
			case DB2:
			case PostgreSQL:
			case KingbaseES:
				break;
			default:
				throw new Exception("unsupported database type.");
		}

		return result;
	}

	public String quoteColumnName(String columnName) throws Exception {
		String result = columnName;

		switch (this) {
			case MySql:
				result = "`" + columnName.replace("`", "``") + "`";
				break;
			case Oracle:
				break;
			case SQLServer:
				result = "[" + columnName + "]";
				break;
			case DB2:
			case PostgreSQL:
			case KingbaseES:
				break;
			default:
				throw new Exception("unsupported database type");
		}

		return result;
	}

	public String quoteTableName(String tableName) throws Exception {
		String result = tableName;

		switch (this) {
			case MySql:
				result = "`" + tableName.replace("`", "``") + "`";
				break;
			case Oracle:
				break;
			case SQLServer:
				break;
			case DB2:
				break;
			case PostgreSQL:
				break;
			case KingbaseES:
				break;
			default:
				throw new Exception("unsupported database type");
		}

		return result;
	}

	/**
	 * 注意：目前只实现了从 mysql/oracle 中识别出ip 信息.未识别到则返回 null.
	 */
	public static String parseIpFromJdbcUrl(String jdbcUrl) {
		Matcher mysql = mysqlPattern.matcher(jdbcUrl);
		if (mysql.matches()) {
			return mysql.group(1);
		}
		Matcher oracle = oraclePattern.matcher(jdbcUrl);
		if (oracle.matches()) {
			return oracle.group(1);
		}
		return null;
	}
}
