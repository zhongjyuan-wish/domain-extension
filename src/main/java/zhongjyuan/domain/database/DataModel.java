package zhongjyuan.domain.database;

import java.util.List;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: DataModel
 * @description: 数据模型对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:54:36
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class DataModel extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields tables: 表集合
	 */
	protected List<Table> tables;

	/**
	 * @fields masterTableCode: 主表编码
	 */
	protected String masterTableCode;

	/**
	 * @fields masterTableName: 主表名称
	 */
	protected String masterTableName;

	/**
	 * @fields relates: 表关联集合
	 */
	protected List<TableRelate> relates;

	/**
	 * @fields connectConfig: 连接配置
	 */
	protected ConnectionConfigurate connectConfig;

	/**
	 * @title:  getTables
	 * @description: 获取表集合
	 * @return: 表集合
	 */
	public List<Table> getTables() {
		return tables;
	}

	/**
	 * @title:  setTable
	 * @description: 设置表
	 * @param table 表
	 */
	public void setTable(Table table) {
		this.tables.add(table);
	}

	/**
	 * @title:  setTables
	 * @description: 设置表集合
	 * @param tables 表集合
	 */
	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	/**
	 * @title:  getMasterTableCode
	 * @description: 获取主表编码
	 * @return: 主表编码
	 */
	public String getMasterTableCode() {
		return masterTableCode;
	}

	/**
	 * @title:  setMasterTableCode
	 * @description: 设置主表编码
	 * @param masterTableCode 主表编码
	 */
	public void setMasterTableCode(String masterTableCode) {
		this.masterTableCode = masterTableCode;
	}

	/**
	 * @title:  getMasterTableName
	 * @description: 获取主表名称
	 * @return: 主表名称
	 */
	public String getMasterTableName() {
		return masterTableName;
	}

	/**
	 * @title:  setMasterTableName
	 * @description: 设置主表名称
	 * @param masterTableName 主表名称
	 */
	public void setMasterTableName(String masterTableName) {
		this.masterTableName = masterTableName;
	}

	/**
	 * @title:  getRelates
	 * @description: 获取表关联集
	 * @return: 表关联集
	 */
	public List<TableRelate> getRelates() {
		return relates;
	}

	/**
	 * @title:  setRelate
	 * @description: 设置表关联
	 * @param relate 表关联
	 */
	public void setRelates(TableRelate relate) {
		this.relates.add(relate);
	}

	/**
	 * @title:  setRelates
	 * @description: 设置表关联集
	 * @param relates 表关联集
	 */
	public void setRelates(List<TableRelate> relates) {
		this.relates = relates;
	}

	/**
	 * @title:  getConnectConfig
	 * @description: 获取连接配置
	 * @return: 连接配置
	 */
	public ConnectionConfigurate getConnectConfig() {
		return connectConfig;
	}

	/**
	 * @title:  setConnectConfig
	 * @description: 设置连接配置
	 * @param connectConfig 连接配置
	 */
	public void setConnectConfig(ConnectionConfigurate connectConfig) {
		this.connectConfig = connectConfig;
	}
}
