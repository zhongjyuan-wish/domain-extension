package zhongjyuan.domain.database;

import java.util.Map;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: TableRelate
 * @description: 表关联对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:55:46
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TableRelate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	// 主表 名称
	/**
	 * @fields mainTableCode: 主编编码
	 */
	protected String mainTableCode;

	// 主表 名称
	/**
	 * @fields mainTableName: 主表名称
	 */
	protected String mainTableName;

	// 子表
	/**
	 * @fields slaveTableCode: 从表编码
	 */
	protected String slaveTableCode;

	// 子表
	/**
	 * @fields slaveTableName: 从表名称
	 */
	protected String slaveTableName;

	// 表关系
	/**
	 * @fields relateShip: 关联关系
	 */
	protected RelateShip relateShip;

	// 主表，外键表 字段关系
	/**
	 * @fields keys: 关联字段Map集
	 */
	protected Map<String, String> keys;

	// 外键表额外条件
	/**
	 * @fields slaveFilters: 从表条件
	 */
	protected Map<String, Object> slaveFilters;

	/**
	 * @title:  getMainTableCode
	 * @description: 获取主表编码
	 * @return: 主表编码
	 */
	public String getMainTableCode() {
		return mainTableCode;
	}

	/**
	 * @title:  setMainTableCode
	 * @description: 设置主表编码
	 * @param mainTableCode 主表编码
	 */
	public void setMainTableCode(String mainTableCode) {
		this.mainTableCode = mainTableCode;
	}

	/**
	 * @title:  getMainTableName
	 * @description: 获取主表名称
	 * @return: 主表名称
	 */
	public String getMainTableName() {
		return mainTableName;
	}

	/**
	 * @title:  setMainTableName
	 * @description: 设置主表名称
	 * @param mainTableName 主表名称
	 */
	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
	}

	/**
	 * @title:  getSlaveTableCode
	 * @description: 获取从表编码
	 * @return: 从表编码
	 */
	public String getSlaveTableCode() {
		return slaveTableCode;
	}

	/**
	 * @title:  setSlaveTableCode
	 * @description: 设置从表编码
	 * @param slaveTableCode 从表编码
	 */
	public void setSlaveTableCode(String slaveTableCode) {
		this.slaveTableCode = slaveTableCode;
	}

	/**
	 * @title:  getSlaveTableName
	 * @description: 获取从表名称
	 * @return: 从表名称
	 */
	public String getSlaveTableName() {
		return slaveTableName;
	}

	/**
	 * @title:  setSlaveTableName
	 * @description: 设置从表名称
	 * @param slaveTableName 从表名称
	 */
	public void setSlaveTableName(String slaveTableName) {
		this.slaveTableName = slaveTableName;
	}

	/**
	 * @title:  getRelateShip
	 * @description: 获取关联关系
	 * @return: 关联关系
	 */
	public RelateShip getRelateShip() {
		return relateShip;
	}

	/**
	 * @title:  setRelateShip
	 * @description: 设置关联关系
	 * @param relateShip 关联关系
	 */
	public void setRelateShip(RelateShip relateShip) {
		this.relateShip = relateShip;
	}

	/**
	 * @title:  getKeys
	 * @description: 获取关联键Map集
	 * @return: 关联键Map集
	 */
	public Map<String, String> getKeys() {
		return keys;
	}

	/**
	 * @title: getKey
	 * @author: zhongjyuan
	 * @description: 获取关联键
	 * @param key 键
	 * @return 值
	 * @throws
	 */
	public String getKey(String key) {
		return keys.get(key);
	}

	/**
	 * @title:  setKeys
	 * @description: 设置关联键Map集
	 * @param keys 关联键Map集
	 */
	public void setKeys(Map<String, String> keys) {
		this.keys = keys;
	}

	/**
	 * @title: setKey
	 * @author: zhongjyuan
	 * @description: 设置关联键
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void setKey(String key, String value) {
		keys.put(key, value);
	}

	/**
	 * @title:  getSlaveFilters
	 * @description: 获取从表条件Map集
	 * @return: 从表条件Map集
	 */
	public Map<String, Object> getSlaveFilters() {
		return slaveFilters;
	}

	/**
	 * @title: getSlaveFilter
	 * @author: zhongjyuan
	 * @description: 获取从表条件
	 * @param key 键
	 * @return 值
	 * @throws
	 */
	public Object getSlaveFilter(String key) {
		return slaveFilters.get(key);
	}

	/**
	 * @title:  setSlaveFilters
	 * @description: 设置从表条件Map集
	 * @param slaveFilters 从表条件Map集
	 */
	public void setSlaveFilters(Map<String, Object> slaveFilters) {
		this.slaveFilters = slaveFilters;
	}

	/**
	 * @title: setSlaveFilter
	 * @author: zhongjyuan
	 * @description: 设置从表条件
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void setSlaveFilter(String key, Object value) {
		slaveFilters.put(key, value);
	}
}
