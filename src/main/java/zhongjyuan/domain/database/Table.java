package zhongjyuan.domain.database;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.alibaba.fastjson.JSONObject;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: Table
 * @description: 表对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:55:20
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Table extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields rows: 行集合
	 */
	protected List<TableRow> rows;

	/**
	 * @fields columns: 列集合
	 */
	protected List<TableColumn> columns;

	/**
	 * @title: Table
	 * @author: zhongjyuan
	 * @description: 表对象
	 * @throws
	 */
	public Table() {
		this.rows = new ArrayList<TableRow>();
		this.columns = new ArrayList<TableColumn>();
	}

	/**
	 * @title: Table
	 * @author: zhongjyuan
	 * @description: 表对象
	 * @param columns 列集合
	 * @throws
	 */
	public Table(List<TableColumn> columns) {
		this.columns = columns;
		this.rows = new ArrayList<TableRow>();
	}

	/**
	 * @title: Table
	 * @author: zhongjyuan
	 * @description: 表对象
	 * @param code 编码
	 * @param columns 列集合
	 * @throws
	 */
	public Table(String code, List<TableColumn> columns) {
		this.code = code;
		this.columns = columns;
		this.rows = new ArrayList<TableRow>();
	}

	/**
	 * @title: Table
	 * @author: zhongjyuan
	 * @description: 表对象
	 * @param code 编码
	 * @param name 名称
	 * @param columns 列集合
	 * @throws
	 */
	public Table(String code, String name, List<TableColumn> columns) {
		this.code = code;
		this.name = name;
		this.columns = columns;
		this.rows = new ArrayList<TableRow>();
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取 
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getRows
	 * @author: zhongjyuan
	 * @description: 获取行集合
	 * @return 行集合
	 * @throws
	 */
	public List<TableRow> getRows() {
		return this.rows;
	}

	/**
	 * @title: setRow
	 * @author: zhongjyuan
	 * @description: 设置行
	 * @param row 行对象
	 * @throws
	 */
	public void setRow(TableRow row) {
		if (this.rows == null || this.rows.size() < 1) {
			this.rows = new ArrayList<TableRow>();
		}
		this.rows.add(row);
	}

	/**
	 * @title: setRow
	 * @author: zhongjyuan
	 * @description: 设置行 
	 * @param element 元素对象
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws
	 */
	public void setRow(JSONObject element) throws IllegalAccessException, InvocationTargetException {
		List<TableColumn> columns = new ArrayList<TableColumn>();

		for (TableColumn column : this.columns) {
			TableColumn tableColumn = new TableColumn();
			BeanUtils.copyProperties(column, tableColumn);

			if (element.containsKey(column.getCode())) {
				column.setValue(element.getObject(column.getCode(), getType(column.getType())));
			}
			columns.add(column);
		}

		setRow(new TableRow(columns));
	}

	/**
	 * @title: createRow
	 * @author: zhongjyuan
	 * @description: 创建行
	 * @return 行对象
	 * @throws
	 */
	public TableRow createRow() {
		return new TableRow(this.columns);
	}

	/**
	 * @title: getColumns
	 * @author: zhongjyuan
	 * @description: 获取列集合
	 * @return 列集合
	 * @throws
	 */
	public List<TableColumn> getColumns() {
		return this.columns;
	}

	/**
	 * @title: setColumns
	 * @author: zhongjyuan
	 * @description: 设置列集合
	 * @param columns 列集合
	 * @throws
	 */
	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}

	/**
	 * @title: getColumn
	 * @author: zhongjyuan
	 * @description: 获取列
	 * @param index 下标
	 * @return 列对象
	 * @throws
	 */
	public TableColumn getColumn(int index) {
		return this.columns.get(index);
	}

	/**
	 * @title: getPrimaryColumns
	 * @author: zhongjyuan
	 * @description: 获取主键列集合
	 * @return 主键列集合
	 * @throws
	 */
	public List<TableColumn> getPrimaryColumns() {

		List<TableColumn> primaryKeys = new ArrayList<TableColumn>();
		if (columns != null && columns.size() > 0) {
			for (int i = 0; i < columns.size(); i++) {

				TableColumn column = columns.get(i);
				if (column.isPrimaryKey()) {
					primaryKeys.add(column);
				}
			}
		}
		return primaryKeys;
	}

	/**
	 * @title: getType
	 * @author: zhongjyuan
	 * @description: 获取数据类型
	 * @param sqlType SQL数据类型
	 * @return 数据类型
	 * @throws
	 */
	public Type getType(int sqlType) {
		switch (sqlType) {
			case Types.CHAR:
			case Types.NCHAR:
			case Types.VARCHAR:
			case Types.LONGVARCHAR:
			case Types.NVARCHAR:
			case Types.LONGNVARCHAR:
				return String.class;
			case Types.SMALLINT:
			case Types.TINYINT:
			case Types.INTEGER:
			case Types.BIGINT:
				return Long.class;
			case Types.NUMERIC:
			case Types.DECIMAL:
			case Types.FLOAT:
			case Types.REAL:
			case Types.DOUBLE:
				return Double.class;
			case Types.TIME:
			case Types.DATE:
			case Types.TIMESTAMP:
				return LocalDateTime.class;
			case Types.BOOLEAN:
			case Types.BIT:
				return Boolean.class;
		}
		return String.class;
	}
}
