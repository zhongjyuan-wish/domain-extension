package zhongjyuan.domain.database;

import java.util.List;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: TableRow
 * @description: 表行对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:56:01
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TableRow extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields columns: 列集合
	 */
	private List<TableColumn> columns;

	/**
	 * @title: TableRow
	 * @author: zhongjyuan
	 * @description: 表行对象
	 * @param columns 列集合
	 * @throws
	 */
	public TableRow(List<TableColumn> columns) {
		this.columns = columns;
	}

	/**
	 * @title: getColumns
	 * @author: zhongjyuan
	 * @description: 获取列集合
	 * @return 列集合
	 * @throws
	 */
	public List<TableColumn> getColumns() {
		return columns;
	}
}
