package zhongjyuan.domain.spring.config;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: WishConfigurate
 * @description: Wish节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:08:02
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class WishConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Common common;

	protected Server server;

	protected Tenant tenant;

	protected Cors cors;

	protected Trace trace;

	protected Thread thread;

	protected Swagger swagger;

	protected Gateway gateway;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Common extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Integer pageIndex;

		protected Integer pageSize;

		protected Integer pageSpan;

		protected String tokenSplit;

		protected String serverSuffix;

		protected List<String> productActives;

		protected String licenseFile;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Server extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String code;

		protected String name;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Tenant extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String mode;

		protected String defaultCode;

		protected String defaultName;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Cors extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String allowedOrigins;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Trace extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean showFilter;

		protected Integer capacity;

		protected Integer maxCapacity;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Thread extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Pools pools;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Pools extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Integer corePoolSize;

			protected Integer keepAliveTime;

			protected Integer queueCapacity;

			protected Integer maximumPoolSize;
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Gateway extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected List<String> swaggerUrls;

		protected Boolean whiteUrlsEnabled;

		protected List<String> whiteUrls;

		protected Boolean blackUrlsEnabled;

		protected List<String> blackUrls;

		protected List<String> customServers;

		protected List<String> websocketServers;

		protected DynamicRoute dynamicRoute;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class DynamicRoute extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Boolean enabled;

			protected String mode;

			protected List<NacosRoute> nacosRoutes;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class NacosRoute extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected String group;

				protected String dataId;
			}
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Swagger extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean enabled;

		protected List<String> defaultGroups;

		protected List<CustomGroup> customGroups;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class CustomGroup extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String name;

			protected String packageName;

			protected String annotationName;
		}
	}
}