package zhongjyuan.domain.spring.config;

import java.util.Arrays;
import java.util.List;

import zhongjyuan.domain.zhongjyuan;

/**
 * @className: ConfigConstant
 * @description: 配置常量对象
 * @author: zhongjyuan
 * @date: 2022年12月26日 下午5:39:53
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ConfigConstant implements zhongjyuan {

	private static final long serialVersionUID = 1L;

	// ----- Server -----

	public static final String SERVER_PORT_CONFIG_NAME = "server.port";

	public static final Integer SERVER_PORT_DEFAULT_CONFIG_VALUE = 7077;

	public static final String SERVER_SERVLET_CONTEXT_PATH_CONFIG_NAME = "server.servlet.context-path";

	public static final String SERVER_SERVLET_CONTEXT_PATH_DEFAULT_CONFIG_VALUE = "/wish";

	// ----- Logging -----

	public static final String LOGGING_CONFIG_CONFIG_NAME = "logging.config";

	public static final String LOGGING_CONFIG_DEFAULT_CONFIG_VALUE = "classpath:logback-spring.xml";

	// ------ Spring -----

	public static final String SPRING_APPLICATION_NAME_CONFIG_NAME = "spring.application.name";

	public static final String SPRING_APPLICATION_NAME_DEFAULT_CONFIG_VALUE = "wish-server";

	public static final String SPRING_PROFILES_ACTIVE_CONFIG_NAME = "spring.profiles.active";

	public static final List<String> SPRING_PROFILES_ACTIVE_DEFAULT_CONFIG_VALUE = Arrays.asList("produce");

	public static final String SPRING_DATASOURCE_DRIVERCLASSNAME_CONFIG_NAME = "spring.datasource.driverClassName";

	public static final String SPRING_DATASOURCE_DRIVERCLASSNAME_DEFAULT_CONFIG_VALUE = "com.mysql.jdbc.Driver";

	public static final String SPRING_DATASOURCE_TYPE_CONFIG_NAME = "spring.datasource.type";

	public static final String SPRING_DATASOURCE_TYPE_DEFAULT_CONFIG_VALUE = "com.alibaba.druid.pool.DruidDataSource";

	public static final String SPRING_REST_TEMPLATE_CONN_READTIMEOUT_CONFIG_NAME = "spring.rest.template.conn.read-timeout";

	public static final Integer SPRING_REST_TEMPLATE_CONN_READTIMEOUT_DEFAULT_CONFIG_VALUE = 5000;

	public static final String SPRING_REST_TEMPLATE_CONN_CONNECTTIMEOUT_CONFIG_NAME = "spring.rest.template.conn.connect-timeout";

	public static final Integer SPRING_REST_TEMPLATE_CONN_CONNECTTIMEOUT_DEFAULT_CONFIG_VALUE = 15000;

	public static final String SPRING_SERVLET_MULTIPART_MAXFILESIZE_CONFIG_NAME = "spring.servlet.multipart.max-file-size";

	public static final String SPRING_SERVLET_MULTIPART_MAXFILESIZE_DEFAULT_CONFIG_VALUE = "100MB";

	public static final String SPRING_SERVLET_MULTIPART_MAXREQUESTSIZE_CONFIG_NAME = "spring.servlet.multipart.max-request-size";

	public static final String SPRING_SERVLET_MULTIPART_MAXREQUESTSIZE_DEFAULT_CONFIG_VALUE = "100MB";

	// ----- Mybatis -----

	public static final String MYBATIS_CONFIGURATION_MAPUNDERSCORETOCAMELCASE_CONFIG_NAME = "mybatis.configuration.map-underscore-to-camel-case";

	public static final Boolean MYBATIS_CONFIGURATION_MAPUNDERSCORETOCAMELCASE_DEFAULT_CONFIG_VALUE = true;

	// ----- Pagehelper -----

	public static final String PAGEHELPER_REASONABLE_CONFIG_NAME = "pagehelper.reasonable";

	public static final Boolean PAGEHELPER_REASONABLE_DEFAULT_CONFIG_VALUE = true;

	public static final String PAGEHELPER_PAGESIZEZERO_CONFIG_NAME = "pagehelper.pageSizeZero";

	public static final Boolean PAGEHELPER_PAGESIZEZERO_DEFAULT_CONFIG_VALUE = true;

	public static final String PAGEHELPER_SUPPORTMETHODSARGUMENTS_CONFIG_NAME = "pagehelper.supportMethodsArguments";

	public static final Boolean PAGEHELPER_SUPPORTMETHODSARGUMENTS_DEFAULT_CONFIG_VALUE = true;

	// ----- Feign -----

	public static final String FEIGN_HTTPCLIENT_CONNECTION_TIMEOUT_CONFIG_NAME = "feign.httpclient.connection-timeout";

	public static final Integer FEIGN_HTTPCLIENT_CONNECTION_TIMEOUT_DEFAULT_CONFIG_VALUE = 3000;

	// ----- Hystrix -----

	// ----- Ribbon -----

	// ----- Management -----

	// ----- Jasypt -----

	public static final String JASYPT_ENCRYPTOR_PASSWORD_CONFIG_NAME = "jasypt.encryptor.password";

	public static final String JASYPT_ENCRYPTOR_PASSWORD_DEFAULT_CONFIG_VALUE = "EbfYkitulv73I2p0mXI50JMXoaxZTKJ7";

	// ----- WISH -----

	public static final String WISH_COMMON_PAGE_INDEX_CONFIG_NAME = "wish.common.page-index";

	public static final Integer WISH_COMMON_PAGE_INDEX_DEFAULT_CONFIG_VALUE = 1;

	public static final String WISH_COMMON_PAGE_SIZE_CONFIG_NAME = "wish.common.page-size";

	public static final Integer WISH_COMMON_PAGE_SIZE_DEFAULT_CONFIG_VALUE = 20;

	public static final String WISH_COMMON_PAGE_SPAN_CONFIG_NAME = "wish.common.page-span";

	public static final Integer WISH_COMMON_PAGE_SPAN_DEFAULT_CONFIG_VALUE = 10;

	public static final String WISH_COMMON_TOKEN_SPLIT_CONFIG_NAME = "wish.common.token-split";

	public static final String WISH_COMMON_TOKEN_SPLIT_DEFAULT_CONFIG_VALUE = "@@@@@";

	public static final String WISH_COMMON_SERVER_SUFFIX_CONFIG_NAME = "wish.common.server-suffix";

	public static final String WISH_COMMON_SERVER_SUFFIX_DEFAULT_CONFIG_VALUE = "-server";

	public static final String WISH_COMMON_PRODUCE_ACTIVES_CONFIG_NAME = "wish.common.produce-actives";

	public static final List<String> WISH_COMMON_PRODUCE_ACTIVES_DEFAULT_CONFIG_VALUE = Arrays.asList("pro", "produce", "product");

	public static final String WISH_COMMON_LICENSE_FILE_CONFIG_NAME = "wish.common.license-file";

	public static final String WISH_COMMON_LICENSE_FILE_DEFAULT_CONFIG_VALUE = "/home/wish/lic/license.ini";

	public static final String WISH_SERVER_CODE_CONFIG_NAME = "wish.server.code";

	public static final String WISH_SERVER_CODE_DEFAULT_CONFIG_VALUE = "F000";

	public static final String WISH_SERVER_NAME_CONFIG_NAME = "wish.server.name";

	public static final String WISH_SERVER_NAME_DEFAULT_CONFIG_VALUE = "Framework";

	public static final String WISH_TENANT_MODE_CONFIG_NAME = "wish.tenant.mode";

	public static final String WISH_TENANT_MODE_DEFAULT_CONFIG_VALUE = "NONE";

	public static final String WISH_TENANT_DEFAULT_CODE_CONFIG_NAME = "wish.tenant.default-code";

	public static final String WISH_TENANT_DEFAULT_CODE_DEFAULT_CONFIG_VALUE = "wish";

	public static final String WISH_TENANT_DEFAULT_NAME_CONFIG_NAME = "wish.tenant.default-name";

	public static final String WISH_TENANT_DEFAULT_NAME_DEFAULT_CONFIG_VALUE = "wish";

	public static final String WISH_CORS_ALLOWED_ORIGINS_CONFIG_NAME = "wish.cors.allowed-origins";

	public static final String WISH_CORS_ALLOWED_ORIGINS_DEFAULT_CONFIG_VALUE = "*";

	public static final String WISH_TRACE_SHOW_FILTER_CONFIG_NAME = "wish.trace.show-filter";

	public static final Boolean WISH_TRACE_SHOW_FILTER_DEFAULT_CONFIG_VALUE = false;

	public static final String WISH_TRACE_CAPACITY_CONFIG_NAME = "wish.trace.capacity";

	public static final Integer WISH_TRACE_CAPACITY_DEFAULT_CONFIG_VALUE = 500;

	public static final String WISH_TRACE_MAX_CAPACITY_CONFIG_NAME = "wish.trace.max-capacity";

	public static final Integer WISH_TRACE_MAX_CAPACITY_DEFAULT_CONFIG_VALUE = 5000;

	public static final String WISH_THREAD_POOLS_QUEUE_CAPACITY_CONFIG_NAME = "wish.thread.pools.queue-capacity";

	public static final Integer WISH_THREAD_POOLS_QUEUE_CAPACITY_DEFAULT_CONFIG_VALUE = 500;

	public static final String WISH_THREAD_POOLS_KEEP_ALIVE_TIME_CONFIG_NAME = "wish.thread.pools.keep-alive-time";

	public static final Long WISH_THREAD_POOLS_KEEP_ALIVE_TIME_DEFAULT_CONFIG_VALUE = 1L;

	public static final String WISH_THREAD_POOLS_CORE_POOL_SIZE_CONFIG_NAME = "wish.thread.pools.core-pool-size";

	public static final Integer WISH_THREAD_POOLS_CORE_POOL_SIZE_DEFAULT_CONFIG_VALUE = 10;

	public static final String WISH_THREAD_POOLS_MAXIMUM_POOL_SIZE_CONFIG_NAME = "wish.thread.pools.maximum-pool-size";

	public static final Integer WISH_THREAD_POOLS_MAXIMUM_POOL_SIZE_DEFAULT_CONFIG_VALUE = 15;

	public static final String WISH_GATEWAY_SWAGGER_URLS_CONFIG_NAME = "wish.gateway.swagger-urls";

	public static final List<String> WISH_GATEWAY_SWAGGER_URLS_DEFAULT_CONFIG_VALUE = Arrays.asList("/v2/api-docs", "/swagger-ui.html", "/swagger-resources/**");

	public static final String WISH_GATEWAY_WHITE_URLS_ENABLED_CONFIG_NAME = "wish.gateway.white-urls-enabled";

	public static final Boolean WISH_GATEWAY_WHITE_URLS_ENABLED_DEFAULT_CONFIG_VALUE = true;

	public static final String WISH_GATEWAY_WHITE_URLS_CONFIG_NAME = "wish.gateway.white-urls";

	public static final List<String> WISH_GATEWAY_WHITE_URLS_DEFAULT_CONFIG_VALUE = Arrays.asList("/anonymous/**");

	public static final String WISH_GATEWAY_BLACK_URLS_ENABLED_CONFIG_NAME = "wish.gateway.black-urls-enabled";

	public static final Boolean WISH_GATEWAY_BLACK_URLS_ENABLED_DEFAULT_CONFIG_VALUE = true;

	public static final String WISH_GATEWAY_BLACK_URLS_CONFIG_NAME = "wish.gateway.black-urls";

	public static final List<String> WISH_GATEWAY_BLACK_URLS_DEFAULT_CONFIG_VALUE = Arrays.asList("/environments/**");

	public static final String WISH_GATEWAY_CUSTOM_SERVERS_CONFIG_NAME = "wish.gateway.custom-servers";

	public static final List<String> WISH_GATEWAY_CUSTOM_SERVERS_DEFAULT_CONFIG_VALUE = Arrays.asList("wish-server");

	public static final String WISH_GATEWAY_WEBSOCKET_SERVERS_CONFIG_NAME = "wish.gateway.websocket-servers";

	public static final List<String> WISH_GATEWAY_WEBSOCKET_SERVERS_DEFAULT_CONFIG_VALUE = Arrays.asList("ws-server", "wss-server");

	public static final String WISH_GATEWAY_DYNAMIC_ROUTE_ENABLED_CONFIG_NAME = "wish.gateway.dynamic-route.enabled";

	public static final Boolean WISH_GATEWAY_DYNAMIC_ROUTE_ENABLED_DEFAULT_CONFIG_VALUE = false;

	public static final String WISH_GATEWAY_DYNAMIC_ROUTE_MODE_CONFIG_NAME = "wish.gateway.dynamic-route.mode";

	public static final String WISH_GATEWAY_DYNAMIC_ROUTE_MODE_DEFAULT_CONFIG_VALUE = "nacos";

	public static final String WISH_GATEWAY_DYNAMIC_ROUTE_NACOS_CONFIG_NAME = "wish.gateway.dynamic-route.nacos";

	public static final String WISH_SWAGGER_ENABLED_CONFIG_NAME = "wish.swagger.enabled";

	public static final Boolean WISH_SWAGGER_ENABLED_DEFAULT_CONFIG_VALUE = false;

	public static final String WISH_SWAGGER_DEFAULT_GROUP_CONFIG_NAME = "wish.swagger.default-groups";

	public static final List<String> WISH_SWAGGER_DEFAULT_GROUP_DEFAULT_CONFIG_VALUE = Arrays.asList("all", "error", "actuate");

	public static final String WISH_SWAGGER_CUSTOM_GROUP_CONFIG_NAME = "wish.swagger.custom-groups";
}
