package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: FeignConfigurate
 * @description: Feign节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:06:14
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class FeignConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Httpclient httpclient;

	protected Okhttp okhttp;

	protected Hystrix hystrix;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Httpclient extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean enabled;

		protected Integer connectionTimeout;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Okhttp extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean enabled;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Hystrix extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean enabled;
	}
}