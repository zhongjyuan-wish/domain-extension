package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: ManagementConfigurate
 * @description: Management节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:07:26
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class ManagementConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Endpoint endpoint;

	protected Endpoints endpoints;

	protected Health health;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Endpoint extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Web web;

		protected Health health;

		protected Shutdown shutdown;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Web extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String basePath;
		}

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Health extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String showDetails;
		}

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Shutdown extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Boolean enabled;
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Endpoints extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Web web;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Web extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Exposure exposure;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class Exposure extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected String include;
			}
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Health extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Mail mail;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Mail extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Boolean enabled;
		}
	}
}