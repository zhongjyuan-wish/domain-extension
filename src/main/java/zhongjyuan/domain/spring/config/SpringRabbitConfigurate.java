package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringRabbitConfigurate
 * @description: Spring.RabbitMQ节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:03:50
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringRabbitConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected String credential;

	protected String host;

	protected String port;

	protected String username;

	protected String password;

	protected String virtualHost;

	protected Boolean publisherConfirms;

	protected Boolean publisherReturns;

	protected Listener listener;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Listener extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Simple simple;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Simple extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String acknowledgeMode;
		}
	}
}