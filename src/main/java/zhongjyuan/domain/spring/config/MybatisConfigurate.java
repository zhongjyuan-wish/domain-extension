package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: MybatisConfigurate
 * @description: Mybatis节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:04:49
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class MybatisConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Configuration configuration;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Configuration extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean mapUnderscoreToCamelCase;
	}
}