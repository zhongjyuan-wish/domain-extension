package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringDataSourceConfigurate
 * @description: Spring.DataSource节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:02:57
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringDataSourceConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected String type;

	protected String driverClassName;

	protected String url;

	protected String jdbcUrl;

	protected String username;

	protected String password;

	protected Druid druid;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Druid extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String url;

		protected String username;

		protected String password;

		protected Integer initialSize;

		protected Integer minIdle;

		protected Integer maxActive;

		protected Integer maxWait;

		protected Integer timeBetweenEvictionRunsMillis;

		protected Integer minEvictableIdleTimeMillis;

		protected Integer maxEvictableIdleTimeMillis;
	}
}