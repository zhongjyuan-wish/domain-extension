package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: RibbonConfigurate
 * @description: Ribbon节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:06:49
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class RibbonConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Integer readTimeout;

	protected Integer connectTimeout;

	protected Boolean okToRetryOnAllOperations;

	protected Integer maxAutoRetries;

	protected Integer maxAutoRetriesNextServer;
}