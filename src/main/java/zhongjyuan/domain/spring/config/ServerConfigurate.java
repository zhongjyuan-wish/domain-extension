package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: ServerConfigurate
 * @description: Server节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:01:29
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class ServerConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected String port;

	protected Servlet servlet;

	protected SSL ssl;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Servlet extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String contextPath;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class SSL extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Boolean enabled;

		protected String keyStore;

		protected String keyStoreType;

		protected String keyStorePassword;
	}
}