package zhongjyuan.domain.spring.config;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringConfigurate
 * @description: Spring节点配置对象[简单]
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:02:30
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Application application;

	protected ProFiles proFiles;

	protected Jackson jackson;

	protected Http http;

	protected Mvc mvc;

	protected Rest rest;

	protected Servlet servlet;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Application extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String name;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class ProFiles extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected List<String> active;
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Jackson extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Serialization serialization;

		protected String timeZone;

		protected String dateFormat;

		protected String defaultPropertyInclusion;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Serialization extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Boolean writeDatesAsTimestamps;
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Http extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Encoding encoding;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Encoding extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String charset;

			protected Boolean enabled;

			protected Boolean force;
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Mvc extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Pathmatch pathmatch;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Pathmatch extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String matchingStrategy;
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Rest extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Template template;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Template extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Conn conn;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class Conn extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Integer readTimeout;

				protected Integer connectTimeout;
			}
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public class Servlet extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Multipart multipart;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public class Multipart extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected String maxFileSize;

			protected String maxRequestSize;
		}
	}
}