package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringRedisConfigurate
 * @description: Spring.Redis节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:03:32
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringRedisConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected String host;

	protected String port;

	protected String password;

	protected Integer database;

	protected Poll poll;

	protected Integer timeout;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Poll extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Integer maxActive;

		protected Integer maxWait;

		protected Integer maxIdle;

		protected Integer minIdle;
	}
}