package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringMailConfigurate
 * @description: Spring.Mail节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:04:09
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringMailConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected String host;

	protected String port;

	protected String name;

	protected String protocol;

	protected String username;

	protected String password;

	protected String defaultEncoding;

	protected Properties properties;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Properties extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Email email;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Email extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Smtp smtp;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class Smtp extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Boolean auth;

				protected SSL ssl;

				protected SocketFactory socketFactory;

				protected Starttls starttls;

				protected Integer connectiontimeout;

				protected Integer timeout;

				protected Integer writetimeout;

				@Data
				@EqualsAndHashCode(
					callSuper = true)
				public static class SSL extends AbstractModel {

					private static final long serialVersionUID = 1L;

					protected Boolean enable;
				}

				@Data
				@EqualsAndHashCode(
					callSuper = true)
				public static class SocketFactory extends AbstractModel {

					private static final long serialVersionUID = 1L;

					protected Boolean fallback;
				}

				@Data
				@EqualsAndHashCode(
					callSuper = true)
				public static class Starttls extends AbstractModel {

					private static final long serialVersionUID = 1L;

					protected Boolean enable;

					protected Boolean required;
				}
			}
		}
	}
}