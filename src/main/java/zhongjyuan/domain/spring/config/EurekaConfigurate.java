package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: EurekaConfigurate
 * @description: Eureka节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:07:08
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class EurekaConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields client: 客户端配置对象
	 */
	protected Client client;

	/**
	 * @fields instance: 实例配置对象
	 */
	protected Instance instance;

	/**
	 * @className: Client
	 * @description: Eureka客户端配置对象
	 * @author: zhongjyuan
	 * @date: 2022年12月27日 下午3:07:08
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Client extends AbstractModel {

		private static final long serialVersionUID = 1L;

		/**
		 * @fields healthcheck: 健康检测配置对象
		 */
		protected Healthcheck healthcheck;

		/**
		 * @className: Healthcheck
		 * @description: 健康检测配置对象
		 * @author: zhongjyuan
		* @date: 2022年12月27日 下午3:07:08
		 * @version: 2023.02.01
		 * @copyright: Copyright (c) 2023 zhongjyuan.com
		 */
		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Healthcheck extends AbstractModel {

			protected static final long serialVersionUID = 1L;

			/**
			 * @fields enabled: 是否启用
			 */
			protected Boolean enabled;
		}
	}

	/**
	 * @className: Instance
	 * @description: Eureka实例配置对象
	 * @author: zhongjyuan
		* @date: 2022年12月27日 下午3:07:08
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Instance extends AbstractModel {

		private static final long serialVersionUID = 1L;

		/**
		 * @fields hostname: 主机名称
		 */
		protected String hostname;

		/**
		 * @fields instanceId: 实例主键
		 */
		protected String instanceId;

		/**
		 * @fields preferIpAddress: preferIpAddress
		 */
		protected Boolean preferIpAddress;

		/**
		 * @fields healthCheckUrlPath: healthCheckUrlPath
		 */
		protected String healthCheckUrlPath;

		/**
		 * @fields leaseRenewalIntervalInSeconds: leaseRenewalIntervalInSeconds
		 */
		protected Integer leaseRenewalIntervalInSeconds;

		/**
		 * @fields leaseExpirationDurationInSeconds: leaseExpirationDurationInSeconds
		 */
		protected Integer leaseExpirationDurationInSeconds;
	}
}