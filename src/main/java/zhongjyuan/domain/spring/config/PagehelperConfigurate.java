package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: PagehelperConfigurate
 * @description: Pagehelper节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:05:56
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class PagehelperConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Boolean reasonable;

	protected Boolean pageSizeZero;

	protected String helperDialect;

	protected String params;

	protected Boolean supportMethodsArguments;
}