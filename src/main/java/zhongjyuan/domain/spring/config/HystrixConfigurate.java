package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: HystrixConfigurate
 * @description: Hystrix节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:06:27
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class HystrixConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Threadpool threadpool;

	protected Boolean shareSecurityContext;

	protected Command command;

	public static class Threadpool extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Default default1;

		public void setDefault(Default default1) {
			this.default1 = default1;
		}

		public Default getDefault() {
			return this.default1;
		}

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Default extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Integer coreSize;

			protected Integer maximumSize;

			protected Integer maxQueueSize;

			protected Boolean allowMaximumSizeToDivergeFromCoreSize;
		}
	}

	public static class Command extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Default default1;

		public void setDefault(Default default1) {
			this.default1 = default1;
		}

		public Default getDefault() {
			return this.default1;
		}

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Default extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected CircuitBreaker circuitBreaker;

			protected Execution execution;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class CircuitBreaker extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Boolean foreOpen;
			}

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class Execution extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Timeout timeout;

				protected Isolation isolation;

				@Data
				@EqualsAndHashCode(
					callSuper = true)
				public static class Timeout extends AbstractModel {

					private static final long serialVersionUID = 1L;

					protected Boolean enabled;
				}

				@Data
				@EqualsAndHashCode(
					callSuper = true)
				public static class Isolation extends AbstractModel {

					private static final long serialVersionUID = 1L;

					protected Thread thread;

					@Data
					@EqualsAndHashCode(
						callSuper = true)
					public static class Thread extends AbstractModel {

						private static final long serialVersionUID = 1L;

						protected Integer timeoutInMilliseconds;
					}
				}
			}
		}
	}
}