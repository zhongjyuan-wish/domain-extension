package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: JasyptConfigurate
 * @description: Jasypt节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:07:41
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class JasyptConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Encryptor encryptor;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Encryptor extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected String password;
	}
}