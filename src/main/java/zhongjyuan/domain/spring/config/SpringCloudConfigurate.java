package zhongjyuan.domain.spring.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zhongjyuan.domain.AbstractModel;

/**
 * @className: SpringCloudConfigurate
 * @description: Spring.Cloud节点配置对象
 * @author: zhongjyuan
 * @date: 2022年12月27日 下午3:04:31
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Data
@EqualsAndHashCode(
	callSuper = true)
public class SpringCloudConfigurate extends AbstractModel {

	private static final long serialVersionUID = 1L;

	protected Gateway gateway;

	protected Loadbalancer loadbalancer;

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Gateway extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Discovery discovery;

		protected Httpclient httpclient;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Discovery extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Locator locator;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class Locator extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Boolean lowerCaseServiceId;
			}
		}

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Httpclient extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected SSL ssl;

			@Data
			@EqualsAndHashCode(
				callSuper = true)
			public static class SSL extends AbstractModel {

				private static final long serialVersionUID = 1L;

				protected Boolean useInsecureTrustManager;
			}
		}
	}

	@Data
	@EqualsAndHashCode(
		callSuper = true)
	public static class Loadbalancer extends AbstractModel {

		private static final long serialVersionUID = 1L;

		protected Retry retry;

		@Data
		@EqualsAndHashCode(
			callSuper = true)
		public static class Retry extends AbstractModel {

			private static final long serialVersionUID = 1L;

			protected Boolean enabled;
		}
	}
}