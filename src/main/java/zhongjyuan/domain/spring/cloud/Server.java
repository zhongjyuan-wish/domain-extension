package zhongjyuan.domain.spring.cloud;

import java.util.List;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: Server
 * @description: 服务对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午5:19:36
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Server extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields key: 唯一标识
	 */
	protected String key;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields type: 类型
	 */
	protected Integer type;

	/**
	 * @fields active: 激活
	 */
	protected String active;

	/**
	 * @fields status: 状态
	 */
	protected String status;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @fields instances: 实例集合
	 */
	protected List<ServerInstance> instances;

	/**
	 * @fields resources: 资源集合
	 */
	protected List<ServerResource> resources;

	/**
	 * @title: getKey
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title: setKey
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param key 唯一标识
	 * @throws
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getType
	 * @author: zhongjyuan
	 * @description: 获取类型
	 * @return 类型
	 * @throws
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @title: setType
	 * @author: zhongjyuan
	 * @description: 设置类型
	 * @param type 类型
	 * @throws
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @title: getActive
	 * @author: zhongjyuan
	 * @description: 获取激活
	 * @return 激活
	 * @throws
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @title: setActive
	 * @author: zhongjyuan
	 * @description: 设置激活
	 * @param active 激活
	 * @throws
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * @title: getStatus
	 * @author: zhongjyuan
	 * @description: 获取状态
	 * @return 状态
	 * @throws
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @title: setStatus
	 * @author: zhongjyuan
	 * @description: 设置状态
	 * @param status 状态
	 * @throws
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title: getInstances
	 * @author: zhongjyuan
	 * @description: 获取实例集
	 * @return 实例集
	 * @throws
	 */
	public List<ServerInstance> getInstances() {
		return instances;
	}

	/**
	 * @title: setInstances
	 * @author: zhongjyuan
	 * @description: 设置实例集
	 * @param instances 实例集
	 * @throws
	 */
	public void setInstances(List<ServerInstance> instances) {
		this.instances = instances;
	}

	/**
	 * @title: getResources
	 * @author: zhongjyuan
	 * @description: 获取资源集
	 * @return 资源集
	 * @throws
	 */
	public List<ServerResource> getResources() {
		return resources;
	}

	/**
	 * @title: setResources
	 * @author: zhongjyuan
	 * @description: 设置资源集
	 * @param resources 资源集
	 * @throws
	 */
	public void setResources(List<ServerResource> resources) {
		this.resources = resources;
	}
}
