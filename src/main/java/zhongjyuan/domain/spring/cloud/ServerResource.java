package zhongjyuan.domain.spring.cloud;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: ServerResource
 * @description: 服务资源对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午5:19:08
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ServerResource extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields key: 唯一标识
	 */
	protected String key;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields path: 路径
	 */
	protected String path;

	/**
	 * @fields method: 方式
	 */
	protected String method;

	/**
	 * @fields pattern: 正则表达式
	 */
	protected String pattern;

	/**
	 * @fields description: 描述
	 */
	protected String description;

	/**
	 * @fields serverKey: 服务唯一标识
	 */
	protected String serverKey;

	/**
	 * @fields serverCode: 服务编码
	 */
	protected String serverCode;

	/**
	 * @fields serverName: 服务名称
	 */
	protected String serverName;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @title: getKey
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title: setKey
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param key 唯一标识
	 * @throws
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getPath
	 * @author: zhongjyuan
	 * @description: 获取路径
	 * @return 路径
	 * @throws
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @title: setPath
	 * @author: zhongjyuan
	 * @description: 设置路径
	 * @param path 路径
	 * @throws
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @title: getMethod
	 * @author: zhongjyuan
	 * @description: 获取方式
	 * @return 方式
	 * @throws
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @title: setMethod
	 * @author: zhongjyuan
	 * @description: 设置方式
	 * @param method 方式
	 * @throws
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @title: getPattern
	 * @author: zhongjyuan
	 * @description: 获取正则表达式
	 * @return 正则表达式
	 * @throws
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @title: setPattern
	 * @author: zhongjyuan
	 * @description: 设置正则表达式
	 * @param pattern 正则表达式
	 * @throws
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @title: getDescription
	 * @author: zhongjyuan
	 * @description: 获取描述
	 * @return 描述
	 * @throws
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title: setDescription
	 * @author: zhongjyuan
	 * @description: 设置描述
	 * @param description 描述
	 * @throws
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @title: getServerKey
	 * @author: zhongjyuan
	 * @description: 获取服务唯一标识
	 * @return 服务唯一标识
	 * @throws
	 */
	public String getServerKey() {
		return serverKey;
	}

	/**
	 * @title: setServerKey
	 * @author: zhongjyuan
	 * @description: 设置服务唯一标识
	 * @param serverKey 服务唯一标识
	 * @throws
	 */
	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}

	/**
	 * @title: getServerCode
	 * @author: zhongjyuan
	 * @description: 获取服务编码
	 * @return 服务编码
	 * @throws
	 */
	public String getServerCode() {
		return serverCode;
	}

	/**
	 * @title: setServerCode
	 * @author: zhongjyuan
	 * @description: 设置服务编码
	 * @param serverCode 服务编码
	 * @throws
	 */
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}

	/**
	 * @title: getServerName
	 * @author: zhongjyuan
	 * @description: 获取服务名称
	 * @return 服务名称
	 * @throws
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @title: setServerName
	 * @author: zhongjyuan
	 * @description: 设置服务名称
	 * @param serverName 服务名称
	 * @throws
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
