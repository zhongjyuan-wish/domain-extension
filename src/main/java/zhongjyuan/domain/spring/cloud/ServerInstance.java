package zhongjyuan.domain.spring.cloud;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: ServerInstance
 * @description: 服务实例对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午5:19:23
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ServerInstance extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields host: 主机
	 */
	protected String host;

	/**
	 * @fields port: 端口
	 */
	protected String port;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields ipAddress: ip地址
	 */
	protected String ipAddress;

	/**
	 * @fields status: 状态
	 */
	protected String status;

	/**
	 * @fields page: 页面
	 */
	protected String page;

	/**
	 * @fields indexPage: 首页
	 */
	protected String indexPage;

	/**
	 * @fields statusPage: 状态页
	 */
	protected String statusPage;

	/**
	 * @fields healthPage: 健康页
	 */
	protected String healthPage;

	/**
	 * @fields serverKey: 服务唯一标识
	 */
	protected String serverKey;

	/**
	 * @fields serverCode: 服务编码
	 */
	protected String serverCode;

	/**
	 * @fields serverName: 服务名称
	 */
	protected String serverName;

	/**
	 * @fields rowIndex: 行号
	 */
	protected String rowIndex;

	/**
	 * @title: getHost
	 * @author: zhongjyuan
	 * @description: 获取主机
	 * @return 主机
	 * @throws
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @title: setHost
	 * @author: zhongjyuan
	 * @description: 设置主机
	 * @param host 主机
	 * @throws
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @title: getPort
	 * @author: zhongjyuan
	 * @description: 获取端口
	 * @return 端口
	 * @throws
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @title: setPort
	 * @author: zhongjyuan
	 * @description: 设置端口
	 * @param port 端口
	 * @throws
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getIpAddress
	 * @author: zhongjyuan
	 * @description: 获取ip地址
	 * @return ip地址
	 * @throws
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @title: setIpAddress
	 * @author: zhongjyuan
	 * @description: 设置ip地址
	 * @param ipAddress ip地址
	 * @throws
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @title: getStatus
	 * @author: zhongjyuan
	 * @description: 获取状态
	 * @return 状态
	 * @throws
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @title: setStatus
	 * @author: zhongjyuan
	 * @description: 设置状态
	 * @param status 状态
	 * @throws
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @title: getPage
	 * @author: zhongjyuan
	 * @description: 获取页面
	 * @return 页面
	 * @throws
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @title: setPage
	 * @author: zhongjyuan
	 * @description: 设置页面
	 * @param page 页面
	 * @throws
	 */
	public void setPage(String page) {
		this.page = page;
	}

	/**
	 * @title: getIndexPage
	 * @author: zhongjyuan
	 * @description: 获取首页
	 * @return 首页
	 * @throws
	 */
	public String getIndexPage() {
		return indexPage;
	}

	/**
	 * @title: setIndexPage
	 * @author: zhongjyuan
	 * @description: 设置首页
	 * @param indexPage 首页
	 * @throws
	 */
	public void setIndexPage(String indexPage) {
		this.indexPage = indexPage;
	}

	/**
	 * @title: getStatusPage
	 * @author: zhongjyuan
	 * @description: 获取状态页
	 * @return 状态页
	 * @throws
	 */
	public String getStatusPage() {
		return statusPage;
	}

	/**
	 * @title: setStatusPage
	 * @author: zhongjyuan
	 * @description: 设置状态页
	 * @param statusPage 状态页
	 * @throws
	 */
	public void setStatusPage(String statusPage) {
		this.statusPage = statusPage;
	}

	/**
	 * @title: getHealthPage
	 * @author: zhongjyuan
	 * @description: 获取健康页
	 * @return 健康页
	 * @throws
	 */
	public String getHealthPage() {
		return healthPage;
	}

	/**
	 * @title: setHealthPage
	 * @author: zhongjyuan
	 * @description: 设置健康页
	 * @param healthPage 健康页
	 * @throws
	 */
	public void setHealthPage(String healthPage) {
		this.healthPage = healthPage;
	}

	/**
	 * @title: getServerKey
	 * @author: zhongjyuan
	 * @description: 获取服务唯一标识
	 * @return 服务唯一标识
	 * @throws
	 */
	public String getServerKey() {
		return serverKey;
	}

	/**
	 * @title: setServerKey
	 * @author: zhongjyuan
	 * @description: 设置服务唯一标识
	 * @param serverKey 服务唯一标识
	 * @throws
	 */
	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}

	/**
	 * @title: getServerCode
	 * @author: zhongjyuan
	 * @description: 获取服务编码
	 * @return 服务编码
	 * @throws
	 */
	public String getServerCode() {
		return serverCode;
	}

	/**
	 * @title: setServerCode
	 * @author: zhongjyuan
	 * @description: 设置服务编码
	 * @param serverCode 服务编码
	 * @throws
	 */
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}

	/**
	 * @title: getServerName
	 * @author: zhongjyuan
	 * @description: 获取服务名称
	 * @return 服务名称
	 * @throws
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @title: setServerName
	 * @author: zhongjyuan
	 * @description: 设置服务名称
	 * @param serverName 服务名称
	 * @throws
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public String getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(String rowIndex) {
		this.rowIndex = rowIndex;
	}
}
