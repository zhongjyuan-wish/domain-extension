package zhongjyuan.domain.spring.route;

import static org.springframework.util.StringUtils.tokenizeToStringArray;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: DynamicPredicateDefinition
 * @description: 动态路由定义断言对象
 * @author: zhongjyuan
 * @date: 2022年7月8日 下午12:38:43
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Validated
public class DynamicPredicateDefinition extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields name: 名称
	 */
	@NotNull
	protected String name;

	/**
	 * @fields args: 参数Map集合
	 */
	protected Map<String, String> args = new LinkedHashMap<>();

	/**
	 * @title DynamicPredicateDefinition
	 * @author zhongjyuan
	 * @description 动态路由定义断言对象
	 * @throws
	 */
	public DynamicPredicateDefinition() {
	}

	/**
	 * @title DynamicPredicateDefinition
	 * @author zhongjyuan
	 * @description 动态路由定义断言对象
	 * @param text 配置文本
	 * @throws
	 */
	public DynamicPredicateDefinition(String text) {
		int eqIdx = text.indexOf('=');
		if (eqIdx <= 0) {
			throw new ValidationException("Unable to parse PredicateDefinition text '" + text + "'" + ", must be of the form name=value");
		}
		setName(text.substring(0, eqIdx));

		String[] args = tokenizeToStringArray(text.substring(eqIdx + 1), ",");

		for (int i = 0; i < args.length; i++) {
			this.args.put(generateName(i), args[i]);
		}
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getArgs
	 * @author: zhongjyuan
	 * @description: 获取参数Map集
	 * @return 参数Map集
	 * @throws
	 */
	public Map<String, String> getArgs() {
		return args;
	}

	/**
	 * @title: setArgs
	 * @author: zhongjyuan
	 * @description: 设置参数Map集
	 * @param args 参数Map集
	 * @throws
	 */
	public void setArgs(Map<String, String> args) {
		this.args = args;
	}

	/**
	 * @title: addArg
	 * @author: zhongjyuan
	 * @description: 增加参数
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void addArg(String key, String value) {
		this.args.put(key, value);
	}

	/**
	 * @title: equals
	 * @author: zhongjyuan
	 * @description: equals
	 * @param o 对象
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DynamicPredicateDefinition that = (DynamicPredicateDefinition) o;
		return Objects.equals(name, that.name) && Objects.equals(args, that.args);
	}

	/**
	 * @title: hashCode
	 * @author: zhongjyuan
	 * @description: hashCode
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(name, args);
	}

	/**
	 * @title: toString
	 * @author: zhongjyuan
	 * @description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("DynamicPredicateDefinition{");
		sb.append("name='").append(name).append('\'');
		sb.append(", args=").append(args);
		sb.append('}');
		return sb.toString();
	}

	/**
	 * @title: generateName
	 * @author: zhongjyuan
	 * @description: 生成名称
	 * @param i 下标
	 * @return 名称
	 * @throws
	 */
	public String generateName(int i) {
		return "_genkey_" + i;
	}
}