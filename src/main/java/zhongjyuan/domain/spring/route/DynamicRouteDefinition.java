package zhongjyuan.domain.spring.route;

import static org.springframework.util.StringUtils.tokenizeToStringArray;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;

import zhongjyuan.domain.AbstractModel;

/**
 * @className: DynamicRouteDefinition
 * @description: 动态路由定义对象
 * @author: zhongjyuan
 * @date: 2022年7月6日 下午5:06:06
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
/**
 * @className: DynamicRouteDefinition
 * @description: DynamicRouteDefinition
 * @author: zhongjyuan
 * @date: 2023年10月11日 下午2:56:39
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Validated
public class DynamicRouteDefinition extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 唯一标识
	 */
	protected String id;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields path: 路径
	 */
	protected String path;

	/**
	 * @fields method: 请求方式
	 */
	protected String method;

	/**
	 * @fields pattern: 匹配正则
	 */
	protected String pattern;

	/**
	 * @fields serverId: 服务主键
	 */
	protected String serverId;

	/**
	 * @fields serverCode: 服务编码
	 */
	protected String serverCode;

	/**
	 * @fields serverName: 服务名称
	 */
	protected String serverName;

	/**
	 * @fields serverHost: 服务主机
	 */
	protected String serverHost;

	/**
	 * @fields sourcePath: 源路径
	 */
	protected String sourcePath;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @fields uri: uri
	 */
	@NotNull
	protected URI uri;

	/**
	 * @fields order: 序号
	 */
	protected int order = 0;

	/**
	 * @fields metadata: 元数据Map集
	 */
	protected Map<String, Object> metadata = new HashMap<String, Object>();

	/**
	 * @fields filters: 动态路由过滤器定义集合
	 */
	@Valid
	protected List<DynamicFilterDefinition> filters = new ArrayList<DynamicFilterDefinition>();

	/**
	 * @fields predicates: 动态路由断言定义集合
	 */
	@Valid
	@NotEmpty
	protected List<DynamicPredicateDefinition> predicates = new ArrayList<DynamicPredicateDefinition>();

	/**
	 * @title DynamicRouteDefinition
	 * @author zhongjyuan
	 * @description 动态路由定义对象
	 * @throws
	 */
	public DynamicRouteDefinition() {

	}

	/**
	 * @title DynamicRouteDefinition
	 * @author zhongjyuan
	 * @description 动态路由定义对象
	 * @param text 配置文本
	 * @throws
	 */
	public DynamicRouteDefinition(String text) {
		int eqIdx = text.indexOf('=');
		if (eqIdx <= 0) {
			throw new ValidationException("Unable to parse RouteDefinition text '" + text + "'" + ", must be of the form name=value");
		}

		setId(text.substring(0, eqIdx));

		String[] args = tokenizeToStringArray(text.substring(eqIdx + 1), ",");

		setUri(URI.create(args[0]));

		for (int i = 1; i < args.length; i++) {
			this.predicates.add(new DynamicPredicateDefinition(args[i]));
		}
	}

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return StringUtils.isBlank(id) ? code : id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
		this.code = id;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return StringUtils.isBlank(code) ? id : code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.id = code;
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getPath
	 * @author: zhongjyuan
	 * @description: 获取路径
	 * @return 路径
	 * @throws
	 */
	public String getPath() {
		return StringUtils.isBlank(path) ? (uri == null ? null : uri.getPath()) : path;
	}

	/**
	 * @title: setPath
	 * @author: zhongjyuan
	 * @description: 设置路径
	 * @param path 路径
	 * @throws
	 */
	public void setPath(String path) {
		this.path = path;
		try {
			this.uri = new URI(path);
		} catch (URISyntaxException e) {

		}
	}

	/**
	 * @title: getMethod
	 * @author: zhongjyuan
	 * @description: 获取请求方式
	 * @return 请求方式
	 * @throws
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @title: setMethod
	 * @author: zhongjyuan
	 * @description: 设置请求方式
	 * @param method 请求方式
	 * @throws
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @title: getPattern
	 * @author: zhongjyuan
	 * @description: 获取匹配规则
	 * @return 匹配规则
	 * @throws
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @title: setPattern
	 * @author: zhongjyuan
	 * @description: 设置匹配规则
	 * @param pattern 匹配规则
	 * @throws
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @title: getServerId
	 * @author: zhongjyuan
	 * @description: 获取服务唯一标识
	 * @return 服务唯一标识
	 * @throws
	 */
	public String getServerId() {
		return serverId;
	}

	/**
	 * @title: setServerId
	 * @author: zhongjyuan
	 * @description: 设置服务唯一标识
	 * @param serverId 服务唯一标识
	 * @throws
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	/**
	 * @title: getServerCode
	 * @author: zhongjyuan
	 * @description: 获取服务编码
	 * @return 服务编码
	 * @throws
	 */
	public String getServerCode() {
		return serverCode;
	}

	/**
	 * @title: setServerCode
	 * @author: zhongjyuan
	 * @description: 设置服务编码
	 * @param serverCode 服务编码
	 * @throws
	 */
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}

	/**
	 * @title: getServerName
	 * @author: zhongjyuan
	 * @description: 获取服务名称
	 * @return 服务名称
	 * @throws
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @title: setServerName
	 * @author: zhongjyuan
	 * @description: 设置服务名称
	 * @param serverName 服务名称
	 * @throws
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @title: getServerHost
	 * @author: zhongjyuan
	 * @description: 获取服务主机
	 * @return 服务主机
	 * @throws
	 */
	public String getServerHost() {
		return serverHost;
	}

	/**
	 * @title: setServerHost
	 * @author: zhongjyuan
	 * @description: 设置服务主机
	 * @param serverHost 服务主机
	 * @throws
	 */
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	/**
	 * @title: getSourcePath
	 * @author: zhongjyuan
	 * @description: 获取源路径
	 * @return 源路径
	 * @throws
	 */
	public String getSourcePath() {
		return sourcePath;
	}

	/**
	 * @title: setSourcePath
	 * @author: zhongjyuan
	 * @description: 设置源路径
	 * @param sourcePath 源路径
	 * @throws
	 */
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title: getFilters
	 * @author: zhongjyuan
	 * @description: 获取动态路由过滤器定义集合
	 * @return 动态路由过滤器定义集合
	 * @throws
	 */
	public List<DynamicFilterDefinition> getFilters() {
		return filters;
	}

	/**
	 * @title: setFilters
	 * @author: zhongjyuan
	 * @description: 设置动态路由过滤器定义集合
	 * @param filters 动态路由过滤器定义集合
	 * @throws
	 */
	public void setFilters(List<DynamicFilterDefinition> filters) {
		this.filters = filters;
	}

	/**
	 * @title: getPredicates
	 * @author: zhongjyuan
	 * @description: 获取动态路由断言定义集合
	 * @return 动态路由断言定义集合
	 * @throws
	 */
	public List<DynamicPredicateDefinition> getPredicates() {
		return predicates;
	}

	/**
	 * @title: setPredicates
	 * @author: zhongjyuan
	 * @description: 设置动态路由断言定义集合
	 * @param predicates 动态路由断言定义集合
	 * @throws
	 */
	public void setPredicates(List<DynamicPredicateDefinition> predicates) {
		this.predicates = predicates;
	}

	/**
	 * @title: getUri
	 * @author: zhongjyuan
	 * @description: 获取URI
	 * @return URI
	 * @throws
	 */
	public URI getUri() {
		try {
			return uri == null ? (StringUtils.isNotBlank(path) ? new URI(path) : uri) : uri;
		} catch (URISyntaxException e) {
			return null;
		}
	}

	/**
	 * @title: setUri
	 * @author: zhongjyuan
	 * @description: 设置URI
	 * @param uri URI
	 * @throws
	 */
	public void setUri(URI uri) {
		this.uri = uri;
		this.path = uri == null ? null : uri.getPath();
	}

	/**
	 * @title: getOrder
	 * @author: zhongjyuan
	 * @description: 获取序号
	 * @return 序号
	 * @throws
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @title: setOrder
	 * @author: zhongjyuan
	 * @description: 设置序号
	 * @param order 序号
	 * @throws
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * @title: getMetadata
	 * @author: zhongjyuan
	 * @description: 获取元数据集
	 * @return 元数据集
	 * @throws
	 */
	public Map<String, Object> getMetadata() {
		return metadata;
	}

	/**
	 * @title: setMetadata
	 * @author: zhongjyuan
	 * @description: 设置元数据集
	 * @param metadata 元数据集
	 * @throws
	 */
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}

	/**
	 * @title: equals
	 * @author: zhongjyuan
	 * @description: equals
	 * @param o
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DynamicRouteDefinition that = (DynamicRouteDefinition) o;
		return this.order == that.order && Objects.equals(this.id, that.id) && Objects.equals(this.predicates, that.predicates) && Objects.equals(this.filters, that.filters) && Objects.equals(this.uri, that.uri) && Objects.equals(this.metadata, that.metadata);
	}

	/**
	 * @title: hashCode
	 * @author: zhongjyuan
	 * @description: hashCode
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.predicates, this.filters, this.uri, this.metadata, this.order);
	}

	/**
	 * @title: toString
	 * @author: zhongjyuan
	 * @description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DynamicRouteDefinition{" + "id='" + id + '\'' + ", predicates=" + predicates + ", filters=" + filters + ", uri=" + uri + ", order=" + order + ", metadata=" + metadata + '}';
	}
}
