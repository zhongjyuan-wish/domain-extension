package zhongjyuan.domain.plugin;

import zhongjyuan.domain.Context;
import zhongjyuan.domain.zhongjyuan;
import zhongjyuan.domain.exception.PluginException;

/**
 * @className: ITestProvider
 * @description: 测试提供者接口对象
 * @author: zhongjyuan
 * @date: 2022年11月24日 下午1:44:34
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface ITestProvider extends zhongjyuan {

	/**
	 * @title context
	 * @author zhongjyuan
	 * @description 获取上下文对象
	 * @return 上下文对象
	 * @throws
	 */
	Context context() throws PluginException;

	/**
	 * @title buildContext
	 * @author zhongjyuan
	 * @description 构建上下文对象
	 * @param @return
	 * @return Context 上下文对象
	 * @throws
	 */
	Context buildContext() throws PluginException;
}
