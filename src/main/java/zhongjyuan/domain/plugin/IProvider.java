package zhongjyuan.domain.plugin;

import java.util.List;
import java.util.Map;

import org.pf4j.ExtensionPoint;

import zhongjyuan.domain.Configuration;
import zhongjyuan.domain.Context;
import zhongjyuan.domain.zhongjyuan;
import zhongjyuan.domain.exception.PluginException;

/**
 * @className: IProvider
 * @description: 提供者接口对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 下午5:18:59
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IProvider extends zhongjyuan, ExtensionPoint {

	/**
	 * @title context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @return 上下文对象
	 * @throws
	 */
	Context context() throws PluginException;

	/**
	 * @title load
	 * @author zhongjyuan
	 * @description 装载
	 * @param context 上下文对象
	 * @throws
	 */
	void load(Context context) throws PluginException;

	/**
	 * @title load
	 * @author zhongjyuan
	 * @description 装载
	 * @param properties 属性Map集
	 * @throws
	 */
	void load(Map<String, Object> properties) throws PluginException;

	/**
	 * @title configs
	 * @author zhongjyuan
	 * @description 配置对象集合
	 * @return  配置对象集合
	 * @throws
	 */
	List<Configuration> configs() throws PluginException;
}
