package zhongjyuan.domain.utils.format;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

/**
 * @ClassName: HumpUtil
 * @author zhongjyuan
 * @date 2021年1月25日 上午10:50:52
 * @Description: 駝峰處理工具
 * 
 */
public class HumpUtil {
	private static Pattern humpPattern = Pattern.compile("[A-Z]");
	private static Pattern linePattern = Pattern.compile("_(\\w)");

	/**
	 * @Title lineToHump
	 * @Author zhongjyuan
	 * @Description 下划线转驼峰
	 * @Param @param str
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * @Title lineToHump
	 * @Author zhongjyuan
	 * @Description 下划线转驼峰[lineToHump("ABC_EFF",true)=abcEfg;lineToHump("ABC_EFF",fasle)=AbcEfg]
	 * @Param @param str
	 * @Param @param firstLower
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String lineToHump(String str, Boolean firstLower) {
		char[] chars = { '_' };
		Boolean firstLowerTemp = firstLower;
		firstLowerTemp = firstLowerTemp == null ? true : firstLowerTemp;
		String result = StringUtils.remove(WordUtils.capitalizeFully(str, chars), "_");
		return firstLowerTemp ? StringUtils.uncapitalize(result) : result;
	}

	/**
	 * @Title humpToLine
	 * @Author zhongjyuan
	 * @Description 驼峰转下划线
	 * @Param @param str
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String humpToLine(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
}
