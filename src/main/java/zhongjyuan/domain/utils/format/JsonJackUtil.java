package zhongjyuan.domain.utils.format;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @ClassName: JsonJackUtil
 * @Description: (JsonJack工具)
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午5:01:34
 * @Copyright: @2021 zhongjyuan.com
 */
public class JsonJackUtil {

	private static final String DATE_FORMAT_STR = "yyyy-MM-dd";
	private static final String DATETIME_FORMAT_STR = "yyyy-MM-dd HH:mm:ss[.SSS]";

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss[.SSS]");
	private static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss[.SSS]");

	// 日期序列化
	public static class SQLTimestampSerializer extends JsonSerializer<Timestamp> {

		@Override
		public void serialize(Timestamp value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMAT_STR);
			gen.writeString(dateFormat.format(value));
		}
	}

	// 日期反序列化
	public static class SQLTimestampDeserializer extends JsonDeserializer<Timestamp> {

		@Override
		public Timestamp deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			return Timestamp.valueOf(p.getValueAsString());
		}
	}

	// 日期序列化
	public static class UtilDateSerializer extends JsonSerializer<java.util.Date> {
		@Override
		public void serialize(java.util.Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMAT_STR);
			gen.writeString(dateFormat.format(value));
		}
	}

	// 日期反序列化
	public static class UtilDateDeserializer extends JsonDeserializer<java.util.Date> {

		@Override
		public java.util.Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMAT_STR);
			try {
				return dateFormat.parse(p.getValueAsString());
			} catch (ParseException e) {
				return null;
			}
		}
	}

	// 日期序列化
	public static class SQLDateSerializer extends JsonSerializer<java.sql.Date> {
		@Override
		public void serialize(java.sql.Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STR);
			gen.writeString(dateFormat.format(value));
		}
	}

	// 日期反序列化
	public static class SQLDateDeserializer extends JsonDeserializer<java.sql.Date> {
		@Override
		public java.sql.Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			return java.sql.Date.valueOf(p.getValueAsString());
		}
	}

	// 日期序列化
	public static class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

		@Override
		public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(DATETIME_FORMAT));
		}
	}

	// 日期反序列化
	public static class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

		@Override
		public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			return LocalDateTime.parse(p.getValueAsString(), DATETIME_FORMAT);
		}
	}

	// 日期序列化
	public static class LocalDateSerializer extends JsonSerializer<LocalDate> {

		@Override
		public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(DATE_FORMAT));
		}
	}

	// 日期反序列化
	public static class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

		@Override
		public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			return LocalDate.parse(p.getValueAsString(), DATE_FORMAT);
		}
	}

	// 日期序列化
	public static class LocalTimeSerializer extends JsonSerializer<LocalTime> {

		@Override
		public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(TIME_FORMAT));
		}
	}

	// 日期反序列化
	public static class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {

		@Override
		public LocalTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (StringUtils.isEmpty(p.getValueAsString())) {
				return null;
			}
			return LocalTime.parse(p.getValueAsString(), TIME_FORMAT);
		}
	}

	public static ObjectMapper getObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();

		JavaTimeModule javaTimeModule = new JavaTimeModule();
		javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
		javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer());
		javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer());
		javaTimeModule.addSerializer(Timestamp.class, new SQLTimestampSerializer());
		javaTimeModule.addSerializer(java.sql.Date.class, new SQLDateSerializer());
		javaTimeModule.addSerializer(java.util.Date.class, new UtilDateSerializer());

		// 反序列化日期格式
		javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
		javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());
		javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
		javaTimeModule.addDeserializer(Timestamp.class, new SQLTimestampDeserializer());
		javaTimeModule.addDeserializer(java.sql.Date.class, new SQLDateDeserializer());
		javaTimeModule.addDeserializer(java.util.Date.class, new UtilDateDeserializer());

		objectMapper.registerModule(javaTimeModule);

		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapper.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));

		return objectMapper;
	}

	// 将JSON字符串转换为相应的JavaBean对象
	public static <T> T readValue(String content, Class<T> valueType) {
		if (StringUtils.isEmpty(content) || valueType == null) {
			return null;
		}

		try {
			return getObjectMapper().readValue(content, valueType);
		} catch (Exception e) {
			return null;
		}
	}

	// 将JSON字符串转换为指定java对象
	public static <T> T toJavaObject(String content, Class<T> valueType) {
		if (StringUtils.isEmpty(content) || valueType == null) {
			return null;
		}
		try {
			return getObjectMapper().readValue(content, valueType);
		} catch (Exception e) {
			return null;
		}
	}

	// 将JSON字符串转换为指定java对象
	public static <T> T toJavaObject(String content, TypeReference<T> valueTypeRef) {
		if (StringUtils.isEmpty(content) || valueTypeRef == null) {
			return null;
		}
		try {
			return getObjectMapper().readValue(content, valueTypeRef);
		} catch (Exception e) {
			return null;
		}
	}

	// 将JavaBean转换为json字符串
	public static String toJSon(Object object) {
		try {
			return getObjectMapper().writeValueAsString(object);
		} catch (Exception e) {
			return null;
		}
	}
}
