package zhongjyuan.domain.utils;

import java.lang.Character.UnicodeBlock;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.bitwalker.useragentutils.*;
import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: HttpServletUtils
 * @author zhongjyuan
 * @date 2020年9月22日 上午9:57:10
 * @Description: HttpServlet工具类
 * 
 */
/**
 * @ClassName HttpServletUtil
 * @Description (描述这个类的作用)
 * @Author zhongjyuan
 * @Date 2022年10月28日 下午3:12:52
 * @Copyright zhongjyuan.com
 */
public class HttpServletUtil {

	public static final String IPV4 = "ipv4";

	public static final String IPV6 = "ipv6";

	/**
	 * @author zhongjyuan
	 * @data 2020年7月14日下午1:59:51
	 * @Fields mobileGateWayHeaders : Wap网关Via头信息中特有的描述信息
	 */
	private static String mobileGateWayHeaders[] = new String[] { "ZXWAP", // 中兴提供的wap网关的via信息，例如：Via=ZXWAP GateWayZTE
			// Technologies，
			"chinamobile.com", // 中国移动的诺基亚wap网关，例如：Via=WTP/1.1 GDSZ-PB-GW003-WAP07.gd.chinamobile.com (Nokia
			// WAP Gateway 4.1 CD1/ECD13_D/4.1.04)
			"monternet.com", // 移动梦网的网关，例如：Via=WTP/1.1 BJBJ-PS-WAP1-GW08.bj1.monternet.com. (Nokia WAP
			// Gateway 4.1 CD1/ECD13_E/4.1.05)
			"infoX", // 华为提供的wap网关，例如：Via=HTTP/1.1 GDGZ-PS-GW011-WAP2 (infoX-WISG Huawei
			// Technologies)，或Via=infoX WAP Gateway V300R001 Huawei Technologies
			"XMS 724Solutions HTG", // 国外电信运营商的wap网关，不知道是哪一家
			"wap.lizongbo.com", // 自己测试时模拟的头信息
			"Bytemobile",// 貌似是一个给移动互联网提供解决方案提高网络运行效率的，例如：Via=1.1 Bytemobile OSN WebProxy/5.1
	};

	/**
	 * @author zhongjyuan
	 * @data 2020年7月14日下午2:00:08
	 * @Fields pcHeaders : 电脑上的IE或Firefox浏览器等的User-Agent关键词
	 */
	private static String[] pcHeaders = new String[] { "Windows 98", "Windows ME", "Windows 2000", "Windows XP", "Windows NT", "Ubuntu" };

	/**
	 * @author zhongjyuan
	 * @data 2020年7月14日下午2:00:52
	 * @Fields mobileUserAgents : 手机浏览器的User-Agent里的关键词
	 */
	private static String[] mobileUserAgents = new String[] { "Nokia", // 诺基亚，有山寨机也写这个的，总还算是手机，Mozilla/5.0 (Nokia5800
			// XpressMusic)UC AppleWebkit(like Gecko)
			// Safari/530
			"SAMSUNG", // 三星手机
			// SAMSUNG-GT-B7722/1.0+SHP/VPP/R5+Dolfin/1.5+Nextreaming+SMM-MMS/1.2.0+profile/MIDP-2.1+configuration/CLDC-1.1
			"MIDP-2", // j2me2.0，Mozilla/5.0 (SymbianOS/9.3; U; Series60/3.2 NokiaE75-1 /110.48.125
			// Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML like Gecko)
			// Safari/413
			"CLDC1.1", // M600/MIDP2.0/CLDC1.1/Screen-240X320
			"SymbianOS", // 塞班系统的，
			"MAUI", // MTK山寨机默认ua
			"UNTRUSTED/1.0", // 疑似山寨机的ua，基本可以确定还是手机
			"Windows CE", // Windows CE，Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 7.11)
			"iPhone", // iPhone是否也转wap？不管它，先区分出来再说。Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac
			// OS X; zh-cn) AppleWebKit/532.9 (KHTML like Gecko) Mobile/8B117
			"iPad", // iPad的ua，Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; zh-cn)
			// AppleWebKit/531.21.10 (KHTML like Gecko) Version/4.0.4 Mobile/7B367
			// Safari/531.21.10
			"Android", // Android是否也转wap？Mozilla/5.0 (Linux; U; Android 2.1-update1; zh-cn; XT800
			// Build/TITA_M2_16.22.7) AppleWebKit/530.17 (KHTML like Gecko) Version/4.0
			// Mobile Safari/530.17
			"BlackBerry", // BlackBerry8310/2.7.0.106-4.5.0.182
			"UCWEB", // ucweb是否只给wap页面？ Nokia5800 XpressMusic/UCWEB7.5.0.66/50/999
			"ucweb", // 小写的ucweb貌似是uc的代理服务器Mozilla/6.0 (compatible; MSIE 6.0;) Opera ucweb-squid
			"BREW", // 很奇怪的ua，例如：REW-Applet/0x20068888 (BREW/3.1.5.20; DeviceId: 40105; Lang: zhcn)
			// ucweb-squid
			"J2ME", // 很奇怪的ua，只有J2ME四个字母
			"YULONG", // 宇龙手机，YULONG-CoolpadN68/10.14 IPANEL/2.0 CTC/1.0
			"YuLong", // 还是宇龙
			"COOLPAD", // 宇龙酷派YL-COOLPADS100/08.10.S100 POLARIS/2.9 CTC/1.0
			"TIANYU", // 天语手机TIANYU-KTOUCH/V209/MIDP2.0/CLDC1.1/Screen-240X320
			"TY-", // 天语，TY-F6229/701116_6215_V0230 JUPITOR/2.2 CTC/1.0
			"K-Touch", // 还是天语K-Touch_N2200_CMCC/TBG110022_1223_V0801 MTK/6223 Release/30.07.2008
			// Browser/WAP2.0
			"Haier", // 海尔手机，Haier-HG-M217_CMCC/3.0 Release/12.1.2007 Browser/WAP2.0
			"DOPOD", // 多普达手机
			"Lenovo", // 联想手机，Lenovo-P650WG/S100 LMP/LML Release/2010.02.22 Profile/MIDP2.0
			// Configuration/CLDC1.1
			"LENOVO", // 联想手机，比如：LENOVO-P780/176A
			"HUAQIN", // 华勤手机
			"AIGO-", // 爱国者居然也出过手机，AIGO-800C/2.04 TMSS-BROWSER/1.0.0 CTC/1.0
			"CTC/1.0", // 含义不明
			"CTC/2.0", // 含义不明
			"CMCC", // 移动定制手机，K-Touch_N2200_CMCC/TBG110022_1223_V0801 MTK/6223 Release/30.07.2008
			// Browser/WAP2.0
			"DAXIAN", // 大显手机DAXIAN X180 UP.Browser/6.2.3.2(GUI) MMP/2.0
			"MOT-", // 摩托罗拉，MOT-MOTOROKRE6/1.0 LinuxOS/2.4.20 Release/8.4.2006 Browser/Opera8.00
			// Profile/MIDP2.0 Configuration/CLDC1.1 Software/R533_G_11.10.54R
			"SonyEricsson", // 索爱手机，SonyEricssonP990i/R100 Mozilla/4.0 (compatible; MSIE 6.0; Symbian OS;
			// 405) Opera 8.65 [zh-CN]
			"GIONEE", // 金立手机
			"HTC", // HTC手机
			"ZTE", // 中兴手机，ZTE-A211/P109A2V1.0.0/WAP2.0 Profile
			"HUAWEI", // 华为手机，
			"webOS", // palm手机，Mozilla/5.0 (webOS/1.4.5; U; zh-CN) AppleWebKit/532.2 (KHTML like
			// Gecko) Version/1.0 Safari/532.2 Pre/1.0
			"GoBrowser", // 3g GoBrowser.User-Agent=Nokia5230/GoBrowser/2.0.290 Safari
			"IEMobile", // Windows CE手机自带浏览器，
			"WAP2.0"// 支持wap 2.0的
	};

	/**
	 * @author zhongjyuan
	 * @data 2020年9月22日
	 * @Fields httpServletRequest : HttpServletRequest对象
	 */
	static HttpServletRequest httpServletRequest;

	/**
	 * @author zhongjyuan
	 * @data 2020年11月9日上午9:59:56
	 * @Fields httpServletResponse : HttpServletResponse对象
	 */
	static HttpServletResponse httpServletResponse;

	/**
	 * @Title HttpServletUtil
	 * @Author zhongjyuan
	 * @Description HttpServlet工具类
	 * @Param @param request
	 * @Param @param response
	 * @Throws
	 */
	public HttpServletUtil(HttpServletRequest request, HttpServletResponse response) {
		httpServletRequest = request;
		httpServletResponse = response;
	}

	/**
	 * @Title isMobileRequest
	 * @Author zhongjyuan
	 * @Description 是否移动设备请求
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean isMobileRequest() {
		return isMobileRequest(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title isMobileRequest
	 * @Author zhongjyuan
	 * @Description 是否移动设备请求
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean isMobileRequest(HttpServletRequest request, HttpServletResponse response) {

		boolean pcFlag = false;
		boolean mobileFlag = false;
		String via = request.getHeader("Via");
		String userAgent = request.getHeader("user-agent");

		for (int i = 0; via != null && !via.trim().equals("") && i < mobileGateWayHeaders.length; i++) {
			if (via.contains(mobileGateWayHeaders[i])) {
				mobileFlag = true;
				break;
			}
		}

		for (int i = 0; !mobileFlag && userAgent != null && !userAgent.trim().equals("") && i < mobileUserAgents.length; i++) {
			if (userAgent.contains(mobileUserAgents[i])) {
				mobileFlag = true;
				break;
			}
		}

		for (int i = 0; userAgent != null && !userAgent.trim().equals("") && i < pcHeaders.length; i++) {
			if (userAgent.contains(pcHeaders[i])) {
				pcFlag = true;
				break;
			}
		}

		if (mobileFlag == true && mobileFlag != pcFlag) {
			return true;
		}

		return false;
	}

	/**
	 * @Title getHeaders
	 * @Author zhongjyuan
	 * @Description 获取请求Header集合
	 * @Param @return
	 * @Return Map<String,Object> Header集合
	 * @Throws
	 */
	public static Map<String, Object> getHeaders() {
		return getHeaders(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getHeaders
	 * @Author zhongjyuan
	 * @Description 获取请求Header集合
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return Map<String,Object> Header集合
	 * @Throws
	 */
	public static Map<String, Object> getHeaders(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();

		if (null == request) {
			return result;
		}

		Enumeration<String> reqHeadInfos = request.getHeaderNames();// 获取所有的请求头
		while (reqHeadInfos.hasMoreElements()) {
			String headName = (String) reqHeadInfos.nextElement();
			String headValue = request.getHeader(headName);// 根据请求头的名字获取对应的请求头的值
			result.put(headName, headValue);
		}

		return result;
	}

	/**
	 * @Title getQuerys
	 * @Author zhongjyuan
	 * @Description 获取请求参数集合
	 * @Param @return
	 * @Return Map<String,Object> 参数集合
	 * @Throws
	 */
	public static Map<String, Object> getQuerys() {
		return getQuerys(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getQuerys
	 * @Author zhongjyuan
	 * @Description 获取请求参数集合
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return Map<String,Object> 参数集合
	 * @Throws
	 */
	public static Map<String, Object> getQuerys(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();

		if (null == request) {
			return result;
		}

		Map<String, String[]> paramMap = request.getParameterMap();
		for (Map.Entry<String, String[]> entry : paramMap.entrySet()) {
			String paramName = entry.getKey();
			String paramValue = "";
			String[] paramValueArr = entry.getValue();
			for (int i = 0; paramValueArr != null && i < paramValueArr.length; i++) {
				if (i == paramValueArr.length - 1) {
					paramValue += paramValueArr[i];
				} else {
					paramValue += paramValueArr[i] + ",";
				}
			}
			result.put(paramName, paramValue);
		}

		return result;
	}

	/**
	 * @Title getSessions
	 * @Author zhongjyuan
	 * @Description 获取请求Session集合
	 * @Param @return
	 * @Return Map<String,Object> Session集合
	 * @Throws
	 */
	public static Map<String, Object> getSessions() {
		return getSessions(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getSessions
	 * @Author zhongjyuan
	 * @Description 获取请求Session集合
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return Map<String,Object> Session集合
	 * @Throws
	 */
	public static Map<String, Object> getSessions(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();

		if (null == request) {
			return result;
		}

		HttpSession session = request.getSession();
		Enumeration<?> enumeration = session.getAttributeNames();
		while (enumeration.hasMoreElements()) {

			String name = enumeration.nextElement().toString();

			Object value = session.getAttribute(name);

			result.put(name, value);
		}

		return result;
	}

	/**
	 * @Title getCookies
	 * @Author zhongjyuan
	 * @Description 获取请求Cookie集合
	 * @Param @return
	 * @Return Map<String,Object> Cookie集合
	 * @Throws
	 */
	public static Map<String, Object> getCookies() {
		return getCookies(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getCookies
	 * @Author zhongjyuan
	 * @Description 获取请求Cookie集合
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return Map<String,Object> Cookie集合
	 * @Throws
	 */
	public static Map<String, Object> getCookies(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();

		if (null == request) {
			return result;
		}

		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {

			String name = cookie.getName();

			Object value = cookie.getValue();

			result.put(name, value);
		}

		return result;
	}

	/**
	 * @Title appendUrl
	 * @Author zhongjyuan
	 * @Description 追加参数
	 * @Param @param url 请求url
	 * @Param @param params 参数Map
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String appendUrl(String url, Map<String, Object> params) {
		if (StringUtils.isBlank(url)) {
			return "";
		} else if (params == null || params.size() == 0) {
			return url.trim();
		} else {
			StringBuffer sb = new StringBuffer("");

			Set<String> keys = params.keySet();
			for (String key : keys) {
				sb.append(key).append("=").append(params.get(key)).append("&");
			}
			sb.deleteCharAt(sb.length() - 1);

			url = url.trim();
			int length = url.length();
			int index1 = url.indexOf("?");
			int index2 = url.indexOf("%3f");
			int index3 = url.indexOf("%3F");
			if (index1 > -1 || index2 > -1 || index3 > -1) {// url说明有问号
				if ((length - 1) == index1 || (length - 1) == index2 || (length - 1) == index3) {// url最后一个符号为？，如：http://wwww.baidu.com?
					url += sb.toString();
				} else {// 情况为：http://wwww.baidu.com?aa=11
					url += "&" + sb.toString();
				}
			} else {// url后面没有问号，如：http://wwww.baidu.com
				url += "?" + sb.toString();
			}
			return url;
		}
	}

	/**
	 * @Title appendUrl
	 * @Author zhongjyuan
	 * @Description 追加参数
	 * @Param @param url 请求url
	 * @Param @param name 参数名
	 * @Param @param value 参数值
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String appendUrl(String url, String name, String value) {
		if (StringUtils.isBlank(url)) {
			return "";
		} else if (StringUtils.isBlank(name) || StringUtils.isBlank(value)) {
			return url.trim();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(name, value);
			return appendUrl(url, params);
		}
	}

	/**
	 * @Title getQuery
	 * @Author zhongjyuan
	 * @Description 获取参数值
	 * @Param @param url 请求url
	 * @Param @param name 参数名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getQuery(String url, String name) {

		if (StringUtils.isBlank(url) || StringUtils.isBlank(name)) {
			return null;
		}

		url += "&";
		String pattern = "(\\?|&){1}#{0,1}" + name + "=[a-zA-Z0-9]*(&{1})";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(url);

		if (m.find()) {
			return m.group(0).split("=")[1].replace("&", "");
		} else {
			return null;
		}
	}

	/**
	 * @Title getQuerys
	 * @Author zhongjyuan
	 * @Description 获取参数Map
	 * @Param @param url 请求url
	 * @Param @return
	 * @Return Map<String,String> 参数Map
	 * @Throws
	 */
	public static Map<String, String> getQuerys(String url) {

		Map<String, String> paramMap = new LinkedHashMap<String, String>();

		if (StringUtils.isBlank(url)) {
			return paramMap;
		}

		String regEx = "(\\?|&+)(.+?)=([^&]*)";// 匹配参数名和参数值的正则表达式
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(url);

		// LinkedHashMap是有序的Map集合，遍历时会按照加入的顺序遍历输出
		while (m.find()) {
			String paramName = m.group(2);// 获取参数名
			String paramVal = m.group(3);// 获取参数值
			paramMap.put(paramName, paramVal);
		}

		return paramMap;
	}

	/**
	 * @Title removeQuerys
	 * @Author zhongjyuan
	 * @Description 移除参数
	 * @Param @param url 请求url
	 * @Param @param paramNames 参数名集合
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String removeQuerys(String url, String... paramNames) {
		if (StringUtils.isBlank(url)) {
			return "";
		} else if (paramNames == null || paramNames.length == 0) {
			return url.trim();
		} else {
			url = url.trim();
			int length = url.length();
			int index = url.indexOf("?");
			if (index > -1) {// url说明有问号
				if ((length - 1) == index) {// url最后一个符号为？，如：http://wwww.baidu.com?
					return url;
				} else {// 情况为：http://wwww.baidu.com?aa=11或http://wwww.baidu.com?aa=或http://wwww.baidu.com?aa
					String baseUrl = url.substring(0, index);
					String paramsString = url.substring(index + 1);
					String[] params = paramsString.split("&");
					if (params != null && params.length > 0) {
						Map<String, String> paramsMap = new HashMap<String, String>();
						for (String param : params) {
							if (!StringUtils.isBlank(param)) {
								String[] oneParam = param.split("=");
								String paramName = oneParam[0];
								int count = 0;
								for (int i = 0; i < paramNames.length; i++) {
									if (paramNames[i].equals(paramName)) {
										break;
									}
									count++;
								}
								if (count == paramNames.length) {
									paramsMap.put(paramName, (oneParam.length > 1) ? oneParam[1] : "");
								}
							}
						}
						if (paramsMap != null && paramsMap.size() > 0) {
							StringBuffer paramBuffer = new StringBuffer(baseUrl);
							paramBuffer.append("?");
							Set<String> set = paramsMap.keySet();
							for (String paramName : set) {
								paramBuffer.append(paramName).append("=").append(paramsMap.get(paramName)).append("&");
							}
							paramBuffer.deleteCharAt(paramBuffer.length() - 1);
							return paramBuffer.toString();
						}
						return baseUrl;
					}
				}
			}
			return url;
		}
	}

	/**
	 * @Title getRequestUrl
	 * @Author zhongjyuan
	 * @Description 获取请求的URL地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRequestUrl() {
		return getRequestUrl(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getRequestUrl
	 * @Author zhongjyuan
	 * @Description 获取请求的URL地址
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRequestUrl(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getRequestURL().toString();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getQueryString
	 * @Author zhongjyuan
	 * @Description 获取请求参数字符串
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getQueryString() {
		return getQueryString(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getQueryString
	 * @Author zhongjyuan
	 * @Description 获取请求参数字符串
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getQueryString(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getQueryString();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getRemoteAddress
	 * @Author zhongjyuan
	 * @Description 获取来访IP地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteAddress() {
		return getRemoteAddress(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getRemoteAddress
	 * @Author zhongjyuan
	 * @Description 获取来访IP地址
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteAddress(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getRemoteAddr();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getRemoteHost
	 * @Author zhongjyuan
	 * @Description 获取来访主机
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteHost() {
		return getRemoteHost(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getRemoteHost
	 * @Author zhongjyuan
	 * @Description 获取来访主机
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteHost(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getRemoteHost();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getRemotePort
	 * @Author zhongjyuan
	 * @Description 获取来访端口
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemotePort() {
		return getRemotePort(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getRemotePort
	 * @Author zhongjyuan
	 * @Description 获取来访端口
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemotePort(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return Integer.toString(request.getRemotePort());
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getRemoteUser
	 * @Author zhongjyuan
	 * @Description 获取来访用户
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteUser() {
		return getRemoteUser(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getRemoteUser
	 * @Author zhongjyuan
	 * @Description 获取来访用户
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getRemoteUser(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getRemoteUser();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getMethod
	 * @Author zhongjyuan
	 * @Description 获取请求方式
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getMethod() {
		return getMethod(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getMethod
	 * @Author zhongjyuan
	 * @Description 获取请求方式
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getMethod(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getMethod();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getLocalAddr
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器IP地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getLocalAddr() {
		return getLocalAddr(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getLocalAddr
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器IP地址
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getLocalAddr(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getLocalAddr();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getLocalIp4Addr
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器IPV4地址
	 * @Param @return
	 * @Return List<String>
	 * @Throws
	 */
	public static List<String> getLocalIp4Addr() {
		return getLocalIpAddr(IPV4);
	}

	/**
	 * @Title getLocalIp6Addr
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器IPV6地址
	 * @Param @return
	 * @Return List<String>
	 * @Throws
	 */
	public static List<String> getLocalIp6Addr() {
		return getLocalIpAddr(IPV6);
	}

	/**
	 * @Title getLocalIpAddr
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器IP地址
	 * @Param @param iptv_type
	 * @Param @return
	 * @Return List<String>
	 * @Throws
	 */
	private static List<String> getLocalIpAddr(String iptv_type) {
		List<String> resultList = new ArrayList<String>();
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface current = interfaces.nextElement();
				if (!current.isUp() || current.isLoopback() || current.isVirtual()) {
					continue;
				}
				Enumeration<InetAddress> addresses = current.getInetAddresses();
				while (addresses.hasMoreElements()) {
					InetAddress addr = addresses.nextElement();
					if (((addr instanceof Inet4Address) && IPV4.equals(iptv_type)) || ((addr instanceof Inet6Address) && IPV6.equals(iptv_type))) {
						resultList.add(addr.getHostAddress());
					}
				}
			}
		} catch (SocketException e) {

		}
		return resultList;
	}

	/**
	 * @Title getLocalName
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器主机名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getLocalName() {
		return getLocalName(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getLocalName
	 * @Author zhongjyuan
	 * @Description 获取WEB服务器主机名
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getLocalName(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null == request) {
				return "anonymous";
			}
			return request.getLocalName();
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getUserAgent
	 * @Author zhongjyuan
	 * @Description 获取userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getUserAgent() {
		return getUserAgent(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getUserAgent
	 * @Author zhongjyuan
	 * @Description 获取userAgent信息
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getUserAgent(HttpServletRequest request, HttpServletResponse response) {
		try {
			String userAgent = request.getHeader("User-Agent");
			return userAgent;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOsVersion
	 * @Author zhongjyuan
	 * @Description 解析出osVersion[通过userAgent]
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOsVersion() {
		try {
			String userAgent = getUserAgent();
			return getOsVersion(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOsVersion
	 * @Author zhongjyuan
	 * @Description 解析出osVersion
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOsVersion(String userAgent) {
		try {
			String osVersion = "";
			if (StringUtils.isBlank(userAgent))
				return osVersion;
			UserAgent agent = UserAgent.parseUserAgentString(userAgent);
			Version version = agent.getBrowserVersion();
			osVersion = version.getVersion();
			return osVersion;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOperatingSystem
	 * @Author zhongjyuan
	 * @Description 解析操作系统对象
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return OperatingSystem
	 * @Throws
	 */
	private static OperatingSystem getOperatingSystem(String userAgent) {
		try {
			UserAgent agent = UserAgent.parseUserAgentString(userAgent);
			OperatingSystem operatingSystem = agent.getOperatingSystem();
			return operatingSystem;
		} catch (Exception x) {
			return null;
		}

	}

	/**
	 * @Title getOs
	 * @Author zhongjyuan
	 * @Description 获取os：Windows/ios/Android
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOs() {
		try {
			String userAgent = getUserAgent();
			return getOs(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOs
	 * @Author zhongjyuan
	 * @Description 获取os：Windows/ios/Android
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOs(String userAgent) {
		try {
			OperatingSystem operatingSystem = getOperatingSystem(userAgent);
			String os = operatingSystem.getGroup().getName();
			return os;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getDevicetype
	 * @Author zhongjyuan
	 * @Description 获取deviceType
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getDevicetype() {
		try {
			String userAgent = getUserAgent();
			return getDevicetype(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getDevicetype
	 * @Author zhongjyuan
	 * @Description 获取deviceType
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getDevicetype(String userAgent) {
		try {
			OperatingSystem operatingSystem = getOperatingSystem(userAgent);
			String deviceType = operatingSystem.getDeviceType().toString();
			return deviceType;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getDevicetype
	 * @Author zhongjyuan
	 * @Description 获取deviceType
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getDevicetype(HttpServletRequest request, HttpServletResponse response) {
		try {
			String userAgent = getUserAgent(request, response);
			return getDevicetype(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOsName
	 * @Author zhongjyuan
	 * @Description 获取操作系统的名字
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOsName() {
		try {
			String userAgent = getUserAgent();
			return getOsName(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getOsName
	 * @Author zhongjyuan
	 * @Description 获取操作系统的名字
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getOsName(String userAgent) {
		try {
			OperatingSystem operatingSystem = getOperatingSystem(userAgent);
			String osName = operatingSystem.getName();
			return osName;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getDeviceManufacturer
	 * @Author zhongjyuan
	 * @Description 获取device的生产厂家
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getDeviceManufacturer() {
		try {
			String userAgent = getUserAgent();
			return getDeviceManufacturer(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getDeviceManufacturer
	 * @Author zhongjyuan
	 * @Description 获取device的生产厂家
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getDeviceManufacturer(String userAgent) {
		try {
			OperatingSystem operatingSystem = getOperatingSystem(userAgent);
			String deviceManufacturer = operatingSystem.getManufacturer().toString();
			return deviceManufacturer;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowser
	 * @Author zhongjyuan
	 * @Description 获取浏览器对象
	 * @Param @param agent
	 * @Param @return
	 * @Return Browser
	 * @Throws
	 */
	public static Browser getBrowser(String agent) {
		try {
			UserAgent userAgent = UserAgent.parseUserAgentString(agent);
			Browser browser = userAgent.getBrowser();
			return browser;
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title getBorderName
	 * @Author zhongjyuan
	 * @Description 获取browser名称
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderName() {
		try {
			String userAgent = getUserAgent();
			return getBorderName(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderName
	 * @Author zhongjyuan
	 * @Description 获取browser名称
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderName(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String borderName = browser.getName();
			return borderName;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderType
	 * @Author zhongjyuan
	 * @Description 获取浏览器的类型
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderType() {
		try {
			String userAgent = getUserAgent();
			return getBorderType(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderType
	 * @Author zhongjyuan
	 * @Description 获取浏览器的类型
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderType(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String borderType = browser.getBrowserType().getName();
			return borderType;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderGroup
	 * @Author zhongjyuan
	 * @Description 获取浏览器组： CHROME、IE
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderGroup() {
		try {
			String userAgent = getUserAgent();
			return getBorderGroup(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderGroup
	 * @Author zhongjyuan
	 * @Description 获取浏览器组： CHROME、IE
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderGroup(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String browerGroup = browser.getGroup().getName();
			return browerGroup;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowserManufacturer
	 * @Author zhongjyuan
	 * @Description 获取浏览器的生产厂商
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBrowserManufacturer() {
		try {
			String userAgent = getUserAgent();
			return getBrowserManufacturer(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowserManufacturer
	 * @Author zhongjyuan
	 * @Description 获取浏览器的生产厂商
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBrowserManufacturer(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String browserManufacturer = browser.getManufacturer().getName();
			return browserManufacturer;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderRenderingEngine
	 * @Author zhongjyuan
	 * @Description 获取浏览器使用的渲染引擎
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderRenderingEngine() {
		try {
			String userAgent = getUserAgent();
			return getBorderRenderingEngine(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBorderRenderingEngine
	 * @Author zhongjyuan
	 * @Description 获取浏览器使用的渲染引擎
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBorderRenderingEngine(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String renderingEngine = browser.getRenderingEngine().name();
			return renderingEngine;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowserVersion
	 * @Author zhongjyuan
	 * @Description 获取浏览器版本
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBrowserVersion() {
		try {
			String userAgent = getUserAgent();
			return getBrowserVersion(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowserVersion
	 * @Author zhongjyuan
	 * @Description 获取浏览器版本
	 * @Param @param userAgent userAgent信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBrowserVersion(String userAgent) {
		try {
			Browser browser = getBrowser(userAgent);
			String borderVersion = browser.getVersion(userAgent).toString();
			return borderVersion;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getBrowserVersion
	 * @Author zhongjyuan
	 * @Description 获取浏览器版本
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getBrowserVersion(HttpServletRequest request, HttpServletResponse response) {
		try {
			String userAgent = getUserAgent(request, response);
			return getBrowserVersion(userAgent);
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getResolvingPowerInfo
	 * @Author zhongjyuan
	 * @Description 获取屏幕分辨率信息
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getResolvingPowerInfo() {

		try {
			int screenWidth = ((int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
			int screenHeight = ((int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);

			return screenWidth + " x " + screenHeight;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getIPAddress
	 * @Author zhongjyuan
	 * @Description 获取IP地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getIPAddress() {
		return getIPAddress(httpServletRequest, httpServletResponse);
	}

	/**
	 * @Title getIPAddress
	 * @Author zhongjyuan
	 * @Description 获取IP地址
	 * @Param @param request
	 * @Param @param response
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getIPAddress(HttpServletRequest request, HttpServletResponse response) {
		try {
			String ip = null;

			// X-Forwarded-For：Squid 服务代理
			String ipAddresses = request.getHeader("X-Forwarded-For");

			if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
				// Proxy-Client-IP：apache 服务代理
				ipAddresses = request.getHeader("Proxy-Client-IP");
			}

			if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ipAddresses = request.getHeader("X-Forwarded-For");
			}

			if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
				// WL-Proxy-Client-IP：weblogic 服务代理
				ipAddresses = request.getHeader("WL-Proxy-Client-IP");
			}

			if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
				// HTTP_CLIENT_IP：有些代理服务器
				ipAddresses = request.getHeader("HTTP_CLIENT_IP");
			}

			if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
				// X-Real-IP：nginx服务代理
				ipAddresses = request.getHeader("X-Real-IP");
			}

			// 有些网络通过多层代理，那么获取到的ip就会有多个，一般都是通过逗号（,）分割开来，并且第一个ip为客户端的真实IP
			if (ipAddresses != null && ipAddresses.length() != 0) {
				ip = ipAddresses.split(",")[0];
			}

			// 还是不能获取到，最后再通过request.getRemoteAddr();获取
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
				ip = request.getRemoteAddr();
			}

			if (StringUtils.equals(ip, "0:0:0:0:0:0:0:1")) {
				if (request.getHeader("x-forwarded-for") == null) {
					return request.getRemoteAddr();
				}
				return request.getHeader("x-forwarded-for");
			}

			return ip;
		} catch (Exception x) {
			return "未知";
		}
	}

	/**
	 * @Title getHostIp
	 * @Author zhongjyuan
	 * @Description 获取主机IP
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getHostIp() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
		}
		return "127.0.0.1";
	}

	/**
	 * @Title getHostName
	 * @Author zhongjyuan
	 * @Description 获取主机名称
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}
		return "未知";
	}

	/**
	 * @Title utf8ToUnicode
	 * @Author zhongjyuan
	 * @Description utf-8转unicode
	 * @Param @param inStr
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String utf8ToUnicode(String inStr) {
		char[] myBuffer = inStr.toCharArray();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < inStr.length(); i++) {
			UnicodeBlock ub = UnicodeBlock.of(myBuffer[i]);
			if (ub == UnicodeBlock.BASIC_LATIN) {
				// 英文及数字等
				sb.append(myBuffer[i]);
			} else if (ub == UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
				// 全角半角字符
				int j = (int) myBuffer[i] - 65248;
				sb.append((char) j);
			} else {
				// 汉字
				short s = (short) myBuffer[i];
				String hexS = Integer.toHexString(s);
				String unicode = "\\u" + hexS;
				sb.append(unicode.toLowerCase());
			}
		}
		return sb.toString();
	}

	/**
	 * @Title unicodeToUtf8
	 * @Author zhongjyuan
	 * @Description unicode转utf-8
	 * @Param @param theString
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String unicodeToUtf8(String theString) {
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = theString.charAt(x++);
			if (aChar == '\\') {
				aChar = theString.charAt(x++);
				if (aChar == 'u') {
					// Read the xxxx
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = theString.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
						}
					}
					outBuffer.append((char) value);
				} else {
					if (aChar == 't')
						aChar = '\t';
					else if (aChar == 'r')
						aChar = '\r';
					else if (aChar == 'n')
						aChar = '\n';
					else if (aChar == 'f')
						aChar = '\f';
					outBuffer.append(aChar);
				}
			} else
				outBuffer.append(aChar);
		}
		return outBuffer.toString();
	}

	/**
	 * @Title internalIp
	 * @Author zhongjyuan
	 * @Description 是否内部IP
	 * @Param @param ip
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean internalIp(String ip) {
		byte[] addr = textToNumericFormatV4(ip);
		return internalIp(addr) || "127.0.0.1".equals(ip);
	}

	/**
	 * @Title internalIp
	 * @Author zhongjyuan
	 * @Description 是否内部IP
	 * @Param @param addr
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	private static boolean internalIp(byte[] addr) {
		if (addr == null || addr.length < 2) {
			return true;
		}
		final byte b0 = addr[0];
		final byte b1 = addr[1];
		// 10.x.x.x/8
		final byte SECTION_1 = 0x0A;
		// 172.16.x.x/12
		final byte SECTION_2 = (byte) 0xAC;
		final byte SECTION_3 = (byte) 0x10;
		final byte SECTION_4 = (byte) 0x1F;
		// 192.168.x.x/16
		final byte SECTION_5 = (byte) 0xC0;
		final byte SECTION_6 = (byte) 0xA8;
		switch (b0) {
		case SECTION_1:
			return true;
		case SECTION_2:
			if (b1 >= SECTION_3 && b1 <= SECTION_4) {
				return true;
			}
		case SECTION_5:
			switch (b1) {
			case SECTION_6:
				return true;
			}
		default:
			return false;
		}
	}

	/**
	 * @Title textToNumericFormatV4
	 * @Author zhongjyuan
	 * @Description 将IPv4地址转换成字节
	 * @Param @param text IPv4地址
	 * @Param @return
	 * @Return byte[] 字节
	 * @Throws
	 */
	private static byte[] textToNumericFormatV4(String text) {
		if (text.length() == 0) {
			return null;
		}

		byte[] bytes = new byte[4];
		String[] elements = text.split("\\.", -1);
		try {
			long l;
			int i;
			switch (elements.length) {
			case 1:
				l = Long.parseLong(elements[0]);
				if ((l < 0L) || (l > 4294967295L)) {
					return null;
				}
				bytes[0] = (byte) (int) (l >> 24 & 0xFF);
				bytes[1] = (byte) (int) ((l & 0xFFFFFF) >> 16 & 0xFF);
				bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
				bytes[3] = (byte) (int) (l & 0xFF);
				break;
			case 2:
				l = Integer.parseInt(elements[0]);
				if ((l < 0L) || (l > 255L)) {
					return null;
				}
				bytes[0] = (byte) (int) (l & 0xFF);
				l = Integer.parseInt(elements[1]);
				if ((l < 0L) || (l > 16777215L)) {
					return null;
				}
				bytes[1] = (byte) (int) (l >> 16 & 0xFF);
				bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
				bytes[3] = (byte) (int) (l & 0xFF);
				break;
			case 3:
				for (i = 0; i < 2; ++i) {
					l = Integer.parseInt(elements[i]);
					if ((l < 0L) || (l > 255L)) {
						return null;
					}
					bytes[i] = (byte) (int) (l & 0xFF);
				}
				l = Integer.parseInt(elements[2]);
				if ((l < 0L) || (l > 65535L)) {
					return null;
				}
				bytes[2] = (byte) (int) (l >> 8 & 0xFF);
				bytes[3] = (byte) (int) (l & 0xFF);
				break;
			case 4:
				for (i = 0; i < 4; ++i) {
					l = Integer.parseInt(elements[i]);
					if ((l < 0L) || (l > 255L)) {
						return null;
					}
					bytes[i] = (byte) (int) (l & 0xFF);
				}
				break;
			default:
				return null;
			}
		} catch (NumberFormatException e) {
			return null;
		}
		return bytes;
	}
}
