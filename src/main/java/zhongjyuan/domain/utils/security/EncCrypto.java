package zhongjyuan.domain.utils.security;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;

/**
 * @ClassName: EncUtil
 * @author zhongjyuan
 * @date 2021年2月20日 上午9:13:59
 * @Description: ENC工具类
 * 
 */
public class EncCrypto {

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext 明文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encrypt(String plaintext, String secretKeyt) {

		// 加密工具
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();

		// 加密配置
		EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();

		config.setAlgorithm("PBEWithMD5AndDES");

		// 生成秘钥的盐
		config.setPassword(secretKeyt);

		// 应用配置
		encryptor.setConfig(config);

		return encryptor.encrypt(plaintext);
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext 密文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decrypt(String ciphertext, String secretKeyt) {

		// 加密工具
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();

		// 加密配置
		EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();

		config.setAlgorithm("PBEWithMD5AndDES");

		// 生成秘钥的盐
		config.setPassword(secretKeyt);

		// 应用配置
		encryptor.setConfig(config);

		return encryptor.decrypt(ciphertext);
	}
}
