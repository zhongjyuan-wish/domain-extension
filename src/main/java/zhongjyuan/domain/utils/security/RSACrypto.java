package zhongjyuan.domain.utils.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: RSAUtil
 * @author zhongjyuan
 * @date 2021年2月20日 上午9:07:51
 * @Description: RSA工具类
 * 
 */
@Slf4j
public class RSACrypto {

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext 明文
	 * @Param @param publishKey 公钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encrypt(String plaintext, String publishKey) {

		RSAPublicKey rsaPublicKey;
		byte[] decoded = Base64.decodeBase64(publishKey);

		String ciphertext = "";
		try {
			rsaPublicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));

			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);

			ciphertext = Base64.encodeBase64String(cipher.doFinal(plaintext.getBytes("UTF-8")));

		} catch (IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			log.error("", e);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			log.error("", e);
		} catch (NoSuchPaddingException e) {
			log.error("", e);
		} catch (InvalidKeyException e) {
			log.error("", e);
		}

		return ciphertext;
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext 密文
	 * @Param @param privateKey 私钥
	 * @Param @return
	 * @Param @throws UnsupportedEncodingException
	 * @Param @throws InvalidKeySpecException
	 * @Param @throws NoSuchAlgorithmException
	 * @Param @throws NoSuchPaddingException
	 * @Param @throws InvalidKeyException
	 * @Param @throws IllegalBlockSizeException
	 * @Param @throws BadPaddingException
	 * @Return String
	 * @Throws
	 */
	public static String decrypt(String ciphertext, String privateKey) throws UnsupportedEncodingException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		byte[] decoded = Base64.decodeBase64(privateKey);

		byte[] inputByte = Base64.decodeBase64(ciphertext.getBytes("UTF-8"));

		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));

		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, rsaPrivateKey);

		return new String(cipher.doFinal(inputByte));
	}

	/**
	 * @Title getkeyPair
	 * @Author zhongjyuan
	 * @Description 获取钥对
	 * @Param @return
	 * @Return Map<Integer,String>
	 * @Throws
	 */
	public static Map<Integer, String> getkeyPair() {

		Map<Integer, String> keyMap = new HashMap<Integer, String>();

		KeyPairGenerator keyPairGen;
		try {
			keyPairGen = KeyPairGenerator.getInstance("RSA");
			keyPairGen.initialize(1024, new SecureRandom());

			KeyPair keyPair = keyPairGen.generateKeyPair();
			RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
			RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();

			String publicKeyString = new String(Base64.encodeBase64(rsaPublicKey.getEncoded()));
			String privateKeyString = new String(Base64.encodeBase64((rsaPrivateKey.getEncoded())));

			keyMap.put(0, publicKeyString); // 0表示公钥
			keyMap.put(1, privateKeyString); // 1表示私钥

		} catch (NoSuchAlgorithmException e) {
			log.error("", e);
		}

		return keyMap;
	}
}
