package zhongjyuan.domain.utils.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

/**
 * @Project: eap-security
 * @Package: com.idea.eap.security.sha
 * @ClassName: SHACrypto
 * @Description: SHA工具类
 * @author zhongjyuan
 * @date 2021年3月24日 下午4:56:20
 *
 */
public class SHACrypto {

	public static final String CHARSET = "UTF-8";

	public static final String SHA = "SHA";
	public static final String SHA_256 = "SHA-256";
	public static final String SHA_512 = "SHA-512";

	/**
	 * @Title eccrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext 明文
	 * @Param @param type 类型[SHA|SHA-256|SHA-512]
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String eccrypt(String plaintext, String type) {

		String result = null;
		try {
			MessageDigest messageDigest;
			messageDigest = MessageDigest.getInstance(type);
			byte[] hash = messageDigest.digest(plaintext.getBytes(CHARSET));
			result = Hex.encodeHexString(hash);
			return result;
		} catch (NoSuchAlgorithmException x) {
			return result;
		} catch (UnsupportedEncodingException x) {
			return result;
		} catch (Exception x) {
			return result;
		}
	}
}
