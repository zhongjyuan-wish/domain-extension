package zhongjyuan.domain.utils.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;

import com.alibaba.fastjson.JSON;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.Keys;

/**
 * @ClassName: JWTCrypto
 * @author zhongjyuan
 * @date 2021年1月27日 上午10:33:34
 * @Description: jwt工具类
 * 
 */
public class JWTCrypto {

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param identity 身份对象
	 * @Param @param exp 有效时长
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return Map<String,Long>
	 * @Throws
	 */
	public static Map<String, Long> encrypt(String identity, Integer exp, String secretKeyt) {
		Map<String, Long> result = new HashMap<String, Long>();

		String plaintext = JSON.toJSONString(identity);
		long endTime = new Date().getTime() + 1000 * 60 * (exp == null ? 30 : exp);

		result.put(Jwts.builder().setSubject(plaintext).setExpiration(new Date(endTime)).signWith(generalKey(secretKeyt)).compact(), endTime);

		// Token token = new Token();
		// token.setToken(Jwts.builder().setSubject(plaintext).setExpiration(new
		// Date(endTime)).signWith(generalKey(secretKeyt)).compact());
		// token.setExpiration(new
		// Date(endTime).toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime());

		return result;
	}

	/**
	 * @Title generalKey
	 * @Author zhongjyuan
	 * @Description 生成密钥
	 * @Param @param secretKeyt
	 * @Param @return
	 * @Return SecretKey
	 * @Throws
	 */
	private static SecretKey generalKey(String secretKeyt) {

		byte[] encodedKey = secretKeyt.getBytes();

		return Keys.hmacShaKeyFor(encodedKey);
	}

	/**
	 * @Title check
	 * @Author zhongjyuan
	 * @Description 校验
	 * @Param @param ciphertext 密文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	@SuppressWarnings("deprecation")
	public static boolean check(String ciphertext, String secretKeyt) {

		try {
			ciphertext = ciphertext.split("@@@@")[0];
			Claims claims = Jwts.parser().setSigningKey(generalKey(secretKeyt)).parseClaimsJws(ciphertext).getBody();
			claims.get("sub", String.class);
			return true;
		} catch (ExpiredJwtException e) {
			return false;
		} catch (MalformedJwtException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext 密文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return Claims
	 * @Throws
	 */
	@SuppressWarnings("deprecation")
	public static Claims decrypt(String ciphertext, String secretKeyt) {

		Claims claims = null;

		try {
			ciphertext = ciphertext.split("@@@@")[0];
			claims = Jwts.parser().setSigningKey(generalKey(secretKeyt)).parseClaimsJws(ciphertext).getBody();
		} catch (ExpiredJwtException e) {
			return null;
		}

		return claims;
	}
}
