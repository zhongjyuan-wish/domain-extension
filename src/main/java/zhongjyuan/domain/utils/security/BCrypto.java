package zhongjyuan.domain.utils.security;

import org.mindrot.jbcrypt.BCrypt;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: BCrypto
 * @author zhongjyuan
 * @date 2021年2月19日 下午6:46:38
 * @Description: 加密工具包含BCrypt
 * 
 */
@Slf4j
public class BCrypto {

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encrypt(String plaintext) {
		return BCrypt.hashpw(plaintext, BCrypt.gensalt());
	}

	/**
	 * @Title check
	 * @Author zhongjyuan
	 * @Description 校验
	 * @Param @param plaintext
	 * @Param @param ciphertext
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean check(String plaintext, String ciphertext) {

		boolean result = false;
		try {
			return BCrypt.checkpw(plaintext, ciphertext);
		} catch (IllegalArgumentException e) {
			log.debug("Bcrypt check fail!", e);
		}

		return result;
	}
}
