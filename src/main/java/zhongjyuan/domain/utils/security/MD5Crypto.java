package zhongjyuan.domain.utils.security;

import org.springframework.util.DigestUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: MD5Util
 * @author zhongjyuan
 * @date 2021年2月20日 上午9:12:59
 * @Description: MD5工具类
 * 
 */
@Slf4j
public class MD5Crypto {

	private static final String slat = "&%5123***&&%%$$#@";

	/**
	 * @Title encode
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param plaintext
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encode(String plaintext) {
		try {
			plaintext = plaintext.concat(slat);
			return DigestUtils.md5DigestAsHex(plaintext.getBytes());
		} catch (Exception e) {
			log.debug("MD5 encode fail!", e);
			return null;
		}
	}

	/**
	 * @Title encode
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param plaintext
	 * @Param @param secretKeyt
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encode(String plaintext, String secretKeyt) {
		try {
			return DigestUtils.md5DigestAsHex(plaintext.concat(secretKeyt).getBytes());
		} catch (Exception e) {
			log.debug("MD5 encode fail!", e);
			return null;
		}
	}

	/**
	 * @Title check
	 * @Author zhongjyuan
	 * @Description 验证
	 * @Param @param plaintext
	 * @Param @param ciphertext
	 * @Param @return
	 * @Return Boolean
	 * @Throws
	 */
	public static Boolean check(String plaintext, String ciphertext) {
		Boolean result = false;
		try {
			if (encode(plaintext).equalsIgnoreCase(ciphertext)) {
				return true;
			}
		} catch (Exception e) {
			log.debug("MD5 check fail!", e);
		}
		return result;
	}

	/**
	 * @Title check
	 * @Author zhongjyuan
	 * @Description 验证
	 * @Param @param plaintext
	 * @Param @param secretKeyt
	 * @Param @param ciphertext
	 * @Param @return
	 * @Return Boolean
	 * @Throws
	 */
	public static Boolean check(String plaintext, String secretKeyt, String ciphertext) {
		Boolean result = false;
		try {
			if (encode(plaintext, secretKeyt).equalsIgnoreCase(ciphertext)) {
				return true;
			}
		} catch (Exception e) {
			log.debug("MD5 check fail!", e);
		}
		return result;
	}
}
