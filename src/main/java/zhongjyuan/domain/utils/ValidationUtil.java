package zhongjyuan.domain.utils;

import java.util.Set;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @className: ValidationUtil
 * @description: 校验工具
 * @author: zhongjyuan
 * @date: 2022年10月29日 下午5:39:06
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ValidationUtil {

	private static final Pattern PATTERN_XML_NCNAME = Pattern.compile("[a-zA-Z_][\\-_.0-9_a-zA-Z$]*");

	private static final Pattern PATTERN_URL = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

	private static final Pattern PATTERN_MOBILE = Pattern.compile("^(?:(?:\\+|00)86)?1(?:(?:3[\\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\\d])|(?:9[189]))\\d{8}$");

	/**
	 * @title: isMobile
	 * @author: zhongjyuan
	 * @description: 是否移动端
	 * @param mobile
	 * @return 是否移动端
	 */
	public static boolean isMobile(String mobile) {
		return StringUtils.isNotBlank(mobile) && PATTERN_MOBILE.matcher(mobile).matches();
	}

	/**
	 * @title: isURL
	 * @author: zhongjyuan
	 * @description: 是否路径
	 * @param url
	 * @return 是否路径
	 */
	public static boolean isURL(String url) {
		return StringUtils.isNotBlank(url) && PATTERN_URL.matcher(url).matches();
	}

	/**
	 * @title: isXmlNCName
	 * @author: zhongjyuan
	 * @description: isXmlNCName
	 * @param str
	 * @return
	 */
	public static boolean isXmlNCName(String str) {
		return StringUtils.isNotBlank(str) && PATTERN_XML_NCNAME.matcher(str).matches();
	}

	/**
	 * @title: validate
	 * @author: zhongjyuan
	 * @description: validate
	 * @param validator
	 * @param object
	 * @param groups
	 */
	public static void validate(Validator validator, Object object, Class<?>... groups) {
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
		if (CollectionUtils.isNotEmpty(constraintViolations)) {
			throw new ConstraintViolationException(constraintViolations);
		}
	}
}
