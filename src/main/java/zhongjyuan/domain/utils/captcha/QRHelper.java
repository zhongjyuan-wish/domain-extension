package zhongjyuan.domain.utils.captcha;

import com.google.zxing.qrcode.encoder.ByteMatrix;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName: QRHelper
 * @Description: (二维码帮助类)
 * @author: zhongjyuan
 * @date: 2022年6月14日 上午11:37:16
 * @Copyright: zhongjyuan.com
 */
public class QRHelper extends AbstractModel {

	/**
	 * @Fields serialVersionUID: (这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 实际生成二维码的宽
	 */
	private int width;

	/**
	 * 实际生成二维码的高
	 */
	private int height;

	/**
	 * 左白边大小
	 */
	private int leftPadding;

	/**
	 * 上白边大小
	 */
	private int topPadding;

	/**
	 * 矩阵信息缩放比例
	 */
	private int multiple;

	private ByteMatrix byteMatrix;

	/**
	 * @title:  getWidth
	 * @description: getWidth
	 * @return: int
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @title:  setWidth
	 * @description: setWidth
	 * @param int
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @title:  getHeight
	 * @description: getHeight
	 * @return: int
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @title:  setHeight
	 * @description: setHeight
	 * @param int
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @title:  getLeftPadding
	 * @description: getLeftPadding
	 * @return: int
	 */
	public int getLeftPadding() {
		return leftPadding;
	}

	/**
	 * @title:  setLeftPadding
	 * @description: setLeftPadding
	 * @param int
	 */
	public void setLeftPadding(int leftPadding) {
		this.leftPadding = leftPadding;
	}

	/**
	 * @title:  getTopPadding
	 * @description: getTopPadding
	 * @return: int
	 */
	public int getTopPadding() {
		return topPadding;
	}

	/**
	 * @title:  setTopPadding
	 * @description: setTopPadding
	 * @param int
	 */
	public void setTopPadding(int topPadding) {
		this.topPadding = topPadding;
	}

	/**
	 * @title:  getMultiple
	 * @description: getMultiple
	 * @return: int
	 */
	public int getMultiple() {
		return multiple;
	}

	/**
	 * @title:  setMultiple
	 * @description: setMultiple
	 * @param int
	 */
	public void setMultiple(int multiple) {
		this.multiple = multiple;
	}

	/**
	 * @title:  getByteMatrix
	 * @description: getByteMatrix
	 * @return: ByteMatrix
	 */
	public ByteMatrix getByteMatrix() {
		return byteMatrix;
	}

	/**
	 * @title:  setByteMatrix
	 * @description: setByteMatrix
	 * @param ByteMatrix
	 */
	public void setByteMatrix(ByteMatrix byteMatrix) {
		this.byteMatrix = byteMatrix;
	}
}
