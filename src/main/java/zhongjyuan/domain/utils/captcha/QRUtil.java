package zhongjyuan.domain.utils.captcha;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.EnumMap;

import org.apache.commons.lang3.StringUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import zhongjyuan.domain.utils.image.ImageUtil;

/**
 * @ClassName: QRUtil
 * @Description: (二维码工具类)
 * @author: zhongjyuan
 * @date: 2022年6月14日 上午11:42:32
 * @Copyright: zhongjyuan.com
 */
public class QRUtil {

	private static final int QUIET_ZONE_SIZE = 4;

	private static final int BLACK = 0xFF000000;

	private static final int WHITE = 0xFFFFFFFF;

	private static final String CHARSET = "UTF-8";

	private static final ErrorCorrectionLevel ERRORCORRECTION = ErrorCorrectionLevel.H;

	/**
	 * <p>
	 * 功能描述: [解析二维码]
	 * </p>
	 * 
	 * @Title decode
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public static String decode(String path) throws NotFoundException, MalformedURLException, IOException {
		return QRUtil.decode(ImageUtil.getImageByPath(path));
	}

	/**
	 * <p>
	 * 功能描述: [解析二维码图片]
	 * </p>
	 * 
	 * @Title decode
	 * @param image
	 * @return
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public static String decode(BufferedImage image) throws NotFoundException {

		if (image == null) {
			return null;
		}

		BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);

		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

		Result result;

		EnumMap<DecodeHintType, Object> hints = new EnumMap<DecodeHintType, Object>(DecodeHintType.class);

		// 解码设置编码方式为：UTF-8，
		hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");

		// 优化精度
		hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);

		// 复杂模式，开启PURE_BARCODE模式
		hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

		result = new MultiFormatReader().decode(bitmap, hints);

		return result.getText();
	}

	/**
	 * <p>
	 * 功能描述: [创建二维码图片 (普通, 1.可设置二维码大小, 2.设置二维码前景色即常见二维码黑色部分, 3.设置二维码背景色,即常见二维码白色部分,
	 * 4.可设置二维码留白区域大小, 5.可添加logo 6.可添加背景图片)]
	 * </p>
	 * 
	 * @Title createImage
	 * @param content   二维码内容
	 * @param size      二维码大小(高和宽,默认取值一致)
	 * @param foreColor 前景色
	 * @param backColor 背景色
	 * @param margin    边距0~4
	 * @param logoPath  logo图片路径,没有填null即可
	 * @param backPath  背景图片路径,没有填null即可
	 * @return
	 * @throws WriterException
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public static BufferedImage createImage(String content, Integer size, Integer foreColor, Integer backColor, Integer margin, String logoPath, String backPath) throws WriterException, MalformedURLException, IOException {

		EnumMap<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		hints.put(EncodeHintType.ERROR_CORRECTION, ERRORCORRECTION);
		hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		hints.put(EncodeHintType.MARGIN, margin == null ? 1 : margin);

		BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, size, size, hints);
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		// 绘制的背景色
		//backColor = backColor == null ? WHITE : backColor;

		// 绘制前置色
		//foreColor = foreColor == null ? BLACK : foreColor;

		// 上色
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, bitMatrix.get(x, y) ? (foreColor == null ? BLACK : foreColor) : (backColor == null ? WHITE : backColor));
			}
		}

		// 一般这里先插入logo,便于控制尺寸,但不利于调整透明度显示
		if (!StringUtils.isBlank(logoPath)) {
			image = ImageUtil.insertLogoImage(image, logoPath);
		}

		if (!StringUtils.isBlank(backPath)) {
			image = ImageUtil.addBackImage(image, backPath);
		}

		return image;
	}

	/**
	 * <p>
	 * 功能描述: [ ]
	 * </p>
	 * 
	 * @Title createImageByBase64
	 * @param content
	 * @param size
	 * @param foreColor
	 * @param backColor
	 * @param margin
	 * @param logoPath  base64字符串
	 * @param backPath  base64字符串
	 * @return
	 * @throws WriterException
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public static BufferedImage createImageByBase64(String content, Integer size, Integer foreColor, Integer backColor, Integer margin, String logoPath, String backPath) throws WriterException, MalformedURLException, IOException {

		EnumMap<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		hints.put(EncodeHintType.ERROR_CORRECTION, ERRORCORRECTION);
		hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		hints.put(EncodeHintType.MARGIN, margin == null ? 1 : margin);

		BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, size, size, hints);
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		// 绘制的背景色
		// backColor = backColor == null ? WHITE : backColor;

		// 绘制前置色
		// foreColor = foreColor == null ? BLACK : foreColor;

		// 上色
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, bitMatrix.get(x, y) ? (foreColor == null ? BLACK : foreColor) : (backColor == null ? WHITE : backColor));
			}
		}

		// 一般这里先插入logo,便于控制尺寸,但不利于调整透明度显示
		if (!StringUtils.isBlank(logoPath)) {
			image = ImageUtil.insertLogoImage(image, logoPath);
		}

		if (!StringUtils.isBlank(backPath)) {
			image = ImageUtil.addBackImage(image, backPath);
		}

		return image;
	}

	/**
	 * <p>
	 * 功能描述: [创建二维码图片 (二版, 1.可设置二维码大小, 2.设置二维码前景色即常见二维码黑色部分, 3.设置二维码背景色,即常见二维码白色部分,
	 * 4.可设置二维码留白区域大小, 5.可添加logo, 6.可添加背景图片, 7.可选是否绘制背景,如不绘制默认使用所处环境的背景色,
	 * 8.可自定义探测框颜色 9.可自定义二维码形状(几何图形),常见二维码黑色部分)]
	 * </p>
	 * 
	 * @Title createImage
	 * @param content
	 * @param size
	 * @param foreColor
	 * @param backColor
	 * @param margin
	 * @param logoPath
	 * @param backPath
	 * @param detectOutColor 探测区外框颜色
	 * @param detectInColor  探测区内框颜色
	 * @param isBack         是否绘制背景图
	 * @param style          点图形状
	 * @return
	 * @throws WriterException
	 * @throws IOException
	 * @throws Exception
	 */
	public static BufferedImage createImage(String content, Integer size, Color foreColor, Color backColor, Integer margin, String logoPath, String backPath, Color detectOutColor, Color detectInnerColor, Boolean isBack, String style) throws WriterException, IOException {

		EnumMap<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		hints.put(EncodeHintType.ERROR_CORRECTION, ERRORCORRECTION);
		hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		hints.put(EncodeHintType.MARGIN, margin == null ? 0 : margin);

		QRCode code = Encoder.encode(content, ErrorCorrectionLevel.M, hints);
		QRHelper bitMatrixHelper = renderResult(code, size, margin);
		ByteMatrix byteMatrix = bitMatrixHelper.getByteMatrix();

		// BitMatrix bitMatrix = new QRCodeWriter().encode(content,
		// BarcodeFormat.QR_CODE, size, size, hints);

		int matrixW = byteMatrix.getWidth();
		int matrixH = byteMatrix.getHeight();
		int leftPadding = bitMatrixHelper.getLeftPadding();
		int topPadding = bitMatrixHelper.getTopPadding();

		// 矩阵信息缩放比例
		int infoSize = bitMatrixHelper.getMultiple();

		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);

		// 绘制的背景色
		// 绘制前置色

		// 探测图形配置如不为空
		/*
		 * if (detectInnerColor != null || detectOutColor != null) { if
		 * (detectInnerColor == null) { detectInnerColor = detectOutColor; } else if
		 * (detectOutColor == null) { detectOutColor = detectInnerColor; } }
		 */

		Graphics2D g2d = image.createGraphics();

		// 当二维码中的透明区域，不填充时，如下设置，可以让图片中的透明度覆盖背景色
		g2d.setComposite(AlphaComposite.Src);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// 直接背景铺满整个图
		g2d.setColor(backColor == null ? Color.WHITE : backColor);
		if (isBack) {
			g2d.fillRect(0, 0, size, size);
		} else {
			g2d.fillRect(0, 0, 0, 0);
		}

		// 探测图形的大小
		int detectCornerSize = byteMatrix.get(0, 5) == 1 ? 7 : 5;

		for (int x = 0; x < matrixW; x++) {
			for (int y = 0; y < matrixH; y++) {
				if (byteMatrix.get(x, y) == 0) {
					// 忽略不用渲染的像素点
					continue;
				}
				if (inDetectCornerArea(x, y, matrixW, matrixH, detectCornerSize)) {
					// 绘制三个位置探测图形
					drawDetectImg(g2d, matrixW, matrixH, leftPadding, topPadding, infoSize, detectCornerSize, x, y, detectOutColor == null ? detectInnerColor : detectOutColor, detectInnerColor == null ? detectOutColor : detectInnerColor);
				} else {
					g2d.setColor(foreColor == null ? Color.BLACK : foreColor);
					// 绘制二维码的点图
					drawDotImg(style, g2d, leftPadding, topPadding, infoSize, x, y);
				}
			}
		}
		g2d.dispose();

		// 一般这里先插入logo,便于控制尺寸,但不利于调整透明度显示
		if (!StringUtils.isBlank(logoPath)) {
			image = ImageUtil.insertLogoImage(image, logoPath);
		}

		if (!StringUtils.isBlank(backPath)) {
			image = ImageUtil.addBackImage(image, backPath);
		}

		return image;
	}

	// 探测图形设置
	// 分为左上 右上 和左下
	// private static void setDetectCorner(int detectCornerSize,String location,) {
	//
	// }

	/**
	 * <p>
	 * 功能描述: [判断对应的点是否是二维码举证探测图形中的点,且区分是哪个方位的点]
	 * </p>
	 * 
	 * @Title inDetectCornerArea
	 * @param x
	 * @param y
	 * @param matrixW
	 * @param matrixH
	 * @param detectCornerSize
	 * @return
	 */
	private static boolean inDetectCornerArea(int x, int y, int matrixW, int matrixH, int detectCornerSize) {
		if (x < detectCornerSize && y < detectCornerSize) {
			// 左上角
			// return "leftUp";
			return true;
		}

		if (x < detectCornerSize && y >= (matrixH - detectCornerSize)) {
			// 左下角
			// return "leftDown";
			return true;
		}

		if (x >= (matrixW - detectCornerSize) && y < detectCornerSize) {
			// 右上角
			// return "rightUp";
			return true;
		}

		return false;
	}

	/**
	 * 判断 (x,y) 对应的点是否为二维码举证探测图形中外面的框, 这个方法的调用必须在确认(x,y)对应的点在探测图形内
	 *
	 * @param x                目标点的x坐标
	 * @param y                目标点的y坐标
	 * @param matrixW          二维码矩阵宽
	 * @param matrixH          二维码矩阵高
	 * @param detectCornerSize 探测图形的大小
	 * @return
	 */
	private static boolean inOuterDetectCornerArea(int x, int y, int matrixW, int matrixH, int detectCornerSize) {
		if (x == 0 || x == detectCornerSize - 1 || x == (matrixW - 1) || x == (matrixW - detectCornerSize) || y == 0 || y == detectCornerSize - 1 || y == matrixH - 1 || y == (matrixH - detectCornerSize)) {
			// 外层的框
			return true;
		}

		return false;
	}

	/**
	 * 绘制探测图形
	 *
	 * @param qrCodeConfig     绘制参数
	 * @param g2               二维码画布
	 * @param bitMatrix        二维码矩阵
	 * @param matrixW          二维码矩阵宽
	 * @param matrixH          二维码矩阵高
	 * @param leftPadding      二维码左边留白距离
	 * @param topPadding       二维码上边留白距离
	 * @param infoSize         二维码矩阵中一个点对应的像素大小
	 * @param detectCornerSize 探测图形大小
	 * @param x                目标点x坐标
	 * @param y                目标点y坐标
	 * @param detectOutColor   探测图形外边圈的颜色
	 * @param detectInnerColor 探测图形内部圈的颜色
	 */
	private static void drawDetectImg(Graphics2D g2, int matrixW, int matrixH, int leftPadding, int topPadding, int infoSize, int detectCornerSize, int x, int y, Color detectOutColor, Color detectInnerColor) {

		if (inOuterDetectCornerArea(x, y, matrixW, matrixH, detectCornerSize)) {
			// 外层的框
			g2.setColor(detectOutColor);
		} else {
			// 内层的框
			g2.setColor(detectInnerColor);
		}

		g2.fillRect(leftPadding + x * infoSize, topPadding + y * infoSize, infoSize, infoSize);
	}

	private static void drawDotImg(String style, Graphics2D g2, int leftPadding, int topPadding, int infoSize, int xpoint, int ypoint) {
		int x = leftPadding + xpoint * infoSize;
		int y = topPadding + ypoint * infoSize;

		if (StringUtils.equalsIgnoreCase(style, "triangle")) {
			int px1[] = { x, x + (infoSize >> 1), x + infoSize };
			int py1[] = { y + infoSize, y, y + infoSize };
			g2.fillPolygon(px1, py1, 3);
		} else if (StringUtils.equalsIgnoreCase(style, "miamond")) {
			int cell4 = infoSize >> 2;
			int cell2 = infoSize >> 1;
			int px2[] = { x + cell4, x + infoSize - cell4, x + infoSize, x + cell2, x };
			int py2[] = { y, y, y + cell2, y + infoSize, y + cell2 };
			g2.fillPolygon(px2, py2, 5);
		} else if (StringUtils.equalsIgnoreCase(style, "sexangle")) {// sexangle
			int add = infoSize >> 2;
			int px3[] = { x + add, x + infoSize - add, x + infoSize, x + infoSize - add, x + add, x };
			int py3[] = { y, y, y + add + add, y + infoSize, y + infoSize, y + add + add };
			g2.fillPolygon(px3, py3, 6);
		} else if (StringUtils.equalsIgnoreCase(style, "octagon")) {// octagon
			int add1 = infoSize / 3;
			int px[] = { x + add1, x + infoSize - add1, x + infoSize, x + infoSize, x + infoSize - add1, x + add1, x, x };
			int py[] = { y, y, y + add1, y + infoSize - add1, y + infoSize, y + infoSize, y + infoSize - add1, y + add1 };
			g2.fillPolygon(px, py, 8);
		} else {// 其他情况 rect画矩阵或者其他图形stype未能匹配的情况
			g2.fillRect(x, y, infoSize, infoSize);
		}
		//		switch (style.toLowerCase()) {
		//		case "rect":
		//			g2.fillRect(x, y, infoSize, infoSize);
		//			break;
		//		case "circle":
		//			g2.fill(new Ellipse2D.Float(x, y, infoSize, infoSize));
		//			break;
		//		case "triangle":
		//			int px1[] = { x, x + (infoSize >> 1), x + infoSize };
		//			int py1[] = { y + infoSize, y, y + infoSize };
		//			g2.fillPolygon(px1, py1, 3);
		//			break;
		//		case "miamond":
		//			int cell4 = infoSize >> 2;
		//			int cell2 = infoSize >> 1;
		//			int px2[] = { x + cell4, x + infoSize - cell4, x + infoSize, x + cell2, x };
		//			int py2[] = { y, y, y + cell2, y + infoSize, y + cell2 };
		//			g2.fillPolygon(px2, py2, 5);
		//			break;
		//		case "sexangle":
		//			int add = infoSize >> 2;
		//			int px3[] = { x + add, x + infoSize - add, x + infoSize, x + infoSize - add, x + add, x };
		//			int py3[] = { y, y, y + add + add, y + infoSize, y + infoSize, y + add + add };
		//			g2.fillPolygon(px3, py3, 6);
		//			break;
		//		case "octagon":
		//			int add1 = infoSize / 3;
		//			int px[] = { x + add1, x + infoSize - add1, x + infoSize, x + infoSize, x + infoSize - add1, x + add1, x,
		//					x };
		//			int py[] = { y, y, y + add1, y + infoSize - add1, y + infoSize, y + infoSize, y + infoSize - add1,
		//					y + add1 };
		//			g2.fillPolygon(px, py, 8);
		//			break;
		//		default:
		//			g2.fillRect(x, y, infoSize, infoSize);
		//			break;
		//		}
	}

	/**
	 * <p>
	 * 功能描述: [重写构建二维码矩阵方法]
	 * </p>
	 * 
	 * @Title renderResult
	 * @param code
	 * @param size      二维码大小
	 * @param quietZone 预留白边0~4
	 * @return
	 */
	private static QRHelper renderResult(QRCode code, Integer size, int quietZone) {
		ByteMatrix input = code.getMatrix();
		if (input == null) {
			throw new IllegalStateException();
		}

		int inputWidth = input.getWidth();
		int inputHeight = input.getHeight();
		int qrWidth = inputWidth + (quietZone * 2);
		int qrHeight = inputHeight + (quietZone * 2);

		// 白边过多时, 缩放
		int scale = calculateScale(qrWidth, size);

		int padding = 0;
		if (scale > 0) {
			// 计算边框留白
			padding = (size - qrWidth * scale) / QUIET_ZONE_SIZE * quietZone;
			// size = qrWidth * scale + padding;
		}

		int outputWidth = Math.max(scale > 0 ? qrWidth * scale + padding : size, qrWidth);
		int outputHeight = Math.max(scale > 0 ? qrWidth * scale + padding : size, qrHeight);

		int multiple = Math.min(outputWidth / qrWidth, outputHeight / qrHeight);
		int leftPadding = (outputWidth - (inputWidth * multiple)) / 2;
		int topPadding = (outputHeight - (inputHeight * multiple)) / 2;

		QRHelper res = new QRHelper();
		res.setByteMatrix(input);
		res.setLeftPadding(leftPadding);
		res.setTopPadding(topPadding);
		res.setMultiple(multiple);
		res.setWidth(outputWidth);
		res.setHeight(outputHeight);

		return res;
	}

	/**
	 * 如果留白超过15% , 则需要缩放 (15% 可以根据实际需要进行修改)
	 *
	 * @param qrCodeSize 二维码大小
	 * @param expectSize 期望输出大小
	 * @return 返回缩放比例, <= 0 则表示不缩放, 否则指定缩放参数
	 */
	private static int calculateScale(int qrCodeSize, int expectSize) {
		if (qrCodeSize >= expectSize) {
			return 0;
		}

		int scale = expectSize / qrCodeSize;
		int abs = expectSize - scale * qrCodeSize;
		if (abs < expectSize * 0.1) {
			return 0;
		}

		return scale;
	}
}
