package zhongjyuan.domain.utils.scp;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.trilead.ssh2.ChannelCondition;
import com.trilead.ssh2.Connection;
import com.trilead.ssh2.Session;
import com.trilead.ssh2.StreamGobbler;

/**
 * @ClassName: SCPUtil
 * @Description: SCP工具
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午5:02:17
 * @Copyright: @2021 zhongjyuan.com
 */
public class SCPUtil {

	public static String excuteShell(String ip, int port, String user, String password, String commands) {

		String outStr = "";
		String outErr = "";
		Session session = null;

		Connection con = new Connection(ip, port);
		try {
			con.connect();// 建立连接
			con.authenticateWithPassword(user, password);

			session = con.openSession();
			session.execCommand(commands);

			InputStream stdOut = new StreamGobbler(session.getStdout());
			InputStream stdErr = new StreamGobbler(session.getStderr());

			outErr = processStream(stdErr, Charset.defaultCharset().toString());
			outStr = processStream(stdOut, Charset.defaultCharset().toString());

			session.waitForCondition(ChannelCondition.EXIT_STATUS, (long) 1000 * 5 * 60);
			session.getExitStatus();

			outStr = outErr == null || outErr == "" ? "1" : "-1";
		} catch (IOException e) {

		} finally {
			if (session != null) {
				session.close();
			}
			con.close();
		}

		return outStr;
	}

	private static String processStream(InputStream in, String charset) throws IOException {

		byte[] buf = new byte[1024];

		StringBuilder sb = new StringBuilder();
		while (in.read(buf) != -1) {
			sb.append(new String(buf, charset));
		}

		return sb.toString();
	}
}
