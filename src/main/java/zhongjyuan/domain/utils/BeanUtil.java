package zhongjyuan.domain.utils;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @className: BeanUtil
 * @description: Bean工具
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:59:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class BeanUtil extends BeanUtils {

	/**
	 * @title deepConvert
	 * @author zhongjyuan
	 * @description 转换[深克隆]
	 * @param <S> 来源对象 - 泛型对象
	 * @param <T> 目标对象 - 泛型对象
	 * @param source 来源对象
	 * @param targetClass 目标对象类型
	 * @return T
	 * @throws
	 */
	public static <S, T> T deepConvert(S source, Class<T> targetClass) {
		String json = JSON.toJSONString(source);
		return (T) JSON.parseObject(json, targetClass);
	}

	/**
	 * @title convert
	 * @author zhongjyuan
	 * @description 转换[浅克隆]
	 * @param <S> 来源对象 - 泛型对象
	 * @param <T> 目标对象 - 泛型对象
	 * @param source 来源对象
	 * @param target 目标对象
	 * @return T
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public static <S, T> T convert(S source, T target) {
		JSONObject jsonObject = (JSONObject) JSON.toJSON(source);
		return (T) jsonObject.toJavaObject(target.getClass());
	}

	/**
	 * @title convertToNull
	 * @author zhongjyuan
	 * @description 转换 - 赋值Null[浅克隆]
	 * @param <S> 来源对象 - 泛型对象
	 * @param <T> 目标对象 - 泛型对象
	 * @param source 来源对象
	 * @param targetClass 目标对象类型
	 * @param columns 赋值属性集
	 * @return T
	 * @throws
	 */
	public static <S, T> T convertToNull(S source, Class<T> targetClass, String... columns) {
		JSONObject jsonObject = (JSONObject) JSON.toJSON(source);
		JSONObject resultJson = new JSONObject();
		for (String column : columns) {
			if (ObjectUtils.isNotEmpty(jsonObject.get(column))) {
				resultJson.put(column, null);
			}
		}
		return (T) JSON.toJavaObject(resultJson, targetClass);
	}

	/**
	 * @title convertToNull
	 * @author zhongjyuan
	 * @description 转换 - 赋值Null值[浅克隆]
	 * @param <S> 来源对象 - 泛型对象
	 * @param <T> 目标对象 - 泛型对象
	 * @param source 来源对象
	 * @param target 目标对象
	 * @param columns 赋值属性集
	 * @return T
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public static <S, T> T convertToNull(S source, T target, String... columns) {
		JSONObject jsonObject = (JSONObject) JSON.toJSON(source);
		JSONObject resultJson = new JSONObject();
		for (String column : columns) {
			if (ObjectUtils.isNotEmpty(jsonObject.get(column))) {
				resultJson.put(column, null);
			}
		}
		return (T) JSON.toJavaObject(resultJson, target.getClass());
	}

	/**
	 * @title copyProperties
	 * @author zhongjyuan
	 * @description 复制[浅克隆]
	 * @param target 目标对象
	 * @param sources 来源对象集
	 * @return Object
	 * @throws
	 */
	public static Object copyProperties(Object target, Object... sources) {
		for (Object source : sources) {
			copyProperties(source, target);
		}
		return target;
	}

	/**
	 * @title copyProperties
	 * @author zhongjyuan
	 * @description 复制[浅克隆]
	 * @param <T> 目标对象 - 泛型对象
	 * @param targetClass 目标对象类型
	 * @param sources 来源对象集合
	 * @return T
	 * @throws
	 */
	public static <T> T copyProperties(Class<T> targetClass, Object... sources) {
		T target = getTargetInstance(targetClass);
		for (Object source : sources) {
			copyProperties(source, target);
		}
		return target;
	}

	/**
	 * @title copyListProperties
	 * @author zhongjyuan
	 * @description 复制[浅克隆]
	 * @param <S> 来源对象 - 泛型对象
	 * @param <T> 目标对象 - 泛型对象
	 * @param sources 来源对象集
	 * @param target 目标对象
	 * @return List<T>
	 * @throws
	 */
	public static <S, T> List<T> copyListProperties(List<S> sources, Supplier<T> target) {
		if (CollectionUtils.isEmpty(sources)) {
			return new ArrayList<T>(0);
		}
		List<T> list = new ArrayList<>(sources.size());
		for (S source : sources) {
			T t = target.get();
			copyProperties(source, t);
			list.add(t);
		}
		return list;
	}

	/**
	 * @title copyPropertiesIgnoreNull
	 * @author zhongjyuan
	 * @description 复制 - 忽略Null值[浅克隆]
	 * @param source 来源对象
	 * @param target 目标对象
	 * @throws
	 */
	public static void copyPropertiesIgnoreNull(Object source, Object target) {
		BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
	}

	/**
	 * @title getNullPropertyNames
	 * @author zhongjyuan
	 * @description 获取对象为Null的属性名集合
	 * @param source 来源对象
	 * @return String[] 属性名集合
	 * @throws
	 */
	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null) {
				emptyNames.add(pd.getName());
			}
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

	/**
	 * @title convertToMap
	 * @author zhongjyuan
	 * @description 转换Map[将对象转成Map集]
	 * @param object 对象
	 * @return Map<String,Object>
	 * @throws
	 */
	public static Map<String, Object> convertToMap(Object object) {
		Map<String, Object> result = new LinkedHashMap<String, Object>();

		Class<?> clazz = object.getClass();
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			String fieldName = field.getName();
			Object fieldValue = null;
			try {
				fieldValue = field.get(object);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				fieldValue = null;
			}
			result.put(fieldName, fieldValue);
		}

		return result;
	}

	/**
	 * @title convertToMap
	 * @author zhongjyuan
	 * @description 转换Map[将集合对象某个属性转成Map集]
	 * @param <K> Map键 - 泛型对象
	 * @param <V> Map值 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Map<K,V>
	 * @throws
	 * 
	 *         <pre>
	 *         List<ZhongjyuanDTO> zhongjyuanList = zhongjyuanService.selectList();
	 *         Map<Integer, ZhongjyuanDTO> userIdMap = BeanUtil.convertToMap(zhongjyuanList, "userId");
	 *         </pre>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> convertToMap(List<V> objects, String key) {
		Map<K, V> map = new HashMap<K, V>();

		if (CollectionUtils.isEmpty(objects))
			return map;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}
			field.setAccessible(true);
			for (Object object : objects) {
				map.put((K) field.get(object), (V) object);
			}
		} catch (Exception e) {
			return map;
		}
		return map;
	}

	/**
	 * @title convertToMapList
	 * @author zhongjyuan
	 * @description 转换Map[将集合对象某个属性转成Map集]
	 * @param <K> Map键 - 泛型对象
	 * @param <V> Map值 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Map<K,List<V>>
	 * @throws
	 * 
	 *         <pre>
	 *         List<ShopDTO> shopList = shopService.queryShops();
	 *         Map<Integer, List<ShopDTO>> city2Shops = BeanUtil.convertToMapList(shopList, "cityId");
	 *         </pre>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, List<V>> convertToMapList(List<V> objects, String key) {
		Map<K, List<V>> map = new HashMap<K, List<V>>();

		if (CollectionUtils.isEmpty(objects))
			return map;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}
			field.setAccessible(true);
			for (Object object : objects) {
				K k = (K) field.get(object);
				map.computeIfAbsent(k, k1 -> new ArrayList<>());
				map.get(k).add((V) object);
			}
		} catch (Exception e) {
			return map;
		}
		return map;
	}

	/**
	 * @title convertToSet
	 * @author zhongjyuan
	 * @description 转换Set[将集合对象某个属性转成集合]
	 * @param <K> 集合对象 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Set<K>
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public static <K> Set<K> convertToSet(List<?> objects, String key) {
		Set<K> set = new HashSet<K>();

		if (CollectionUtils.isEmpty(objects))
			return set;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}

			field.setAccessible(true);
			for (Object object : objects) {
				set.add((K) field.get(object));
			}
		} catch (Exception e) {
			return set;
		}
		return set;
	}

	/**
	 * @title getProperty
	 * @author zhongjyuan
	 * @description 获取属性值
	 * @param object 对象
	 * @param fieldName 属性名称
	 * @return Object
	 * @throws
	 */
	public static Object getProperty(Object object, String fieldName) {
		try {
			Field field = deepFindField(object.getClass(), fieldName);
			if (field != null) {
				field.setAccessible(true);
				return field.get(object);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	/**
	 * @title setProperty
	 * @author zhongjyuan
	 * @description 设置属性值
	 * @param object 对象
	 * @param fieldName 属性名称
	 * @param value 属性值
	 * @throws
	 */
	public static void setProperty(Object object, String fieldName, Object value) {
		try {
			Field field = deepFindField(object.getClass(), fieldName);
			if (field != null) {
				field.setAccessible(true);
				field.set(object, value);
			}
		} catch (Exception e) {

		}
	}

	/**
	 * @title describe
	 * @author zhongjyuan
	 * @description JavaBean 转map<String,String>
	 * @param <T>
	 * @param bean
	 * @return Map<String,String>
	 * @throws
	 */
	public static <T> Map<String, String> describe(T bean) {
		Map<String, String> resultMap = null;
		try {
			resultMap = org.apache.commons.beanutils.BeanUtils.describe(bean);
			//去掉多余属性class
			resultMap.remove("class");
		} catch (Exception e) {

		}
		return resultMap;
	}

	/**
	 * @title describeObject
	 * @author zhongjyuan
	 * @description JavaBean 转map<String,Object>
	 * @param <T>
	 * @param bean
	 * @return Map<String,Object>
	 * @throws
	 */
	public static <T> Map<String, Object> describeObject(T bean) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (bean != null) {
			BeanMap beanMap = new BeanMap(bean);
			for (Object key : beanMap.keySet()) {
				map.put(key.toString(), beanMap.get(key));
			}
			//去掉多余属性class
			map.remove("class");
		}
		return map;
	}

	/**
	 * @title mapToJavaBean
	 * @author zhongjyuan
	 * @description Map转JavaBean
	 * @param <T>
	 * @param sourceData
	 * @param targetClass
	 * @return T
	 * @throws
	 */
	public static <T> T mapToJavaBean(Map<String, ? extends Object> sourceData, Class<T> targetClass) {
		T destClasss = getTargetInstance(targetClass);
		try {
			org.apache.commons.beanutils.BeanUtils.populate(destClasss, sourceData);
		} catch (Exception e) {

		}
		return destClasss;
	}

	/**
	 * @title getTargetInstance
	 * @author zhongjyuan
	 * @description 获取目标对象实例
	 * @param <T> 目标对象 - 泛型对象
	 * @param target 目标对象
	 * @return T
	 * @throws
	 */
	private static <T> T getTargetInstance(Class<T> target) {
		try {
			return target.newInstance();
		} catch (InstantiationException e) {

		} catch (IllegalAccessException e) {

		}
		return null;
	}

	/**
	 * @title deepFindField
	 * @author zhongjyuan
	 * @description 递归查找字段
	 * @param clazz 对象类型
	 * @param key 属性名称
	 * @return Field
	 * @throws
	 */
	private static Field deepFindField(Class<?> clazz, String key) {
		Field field = null;
		while (!clazz.getName().equals(Object.class.getName())) {
			try {
				field = clazz.getDeclaredField(key);
				if (field != null) {
					break;
				}
			} catch (Exception e) {
				clazz = clazz.getSuperclass();
			}
		}
		return field;
	}
}
