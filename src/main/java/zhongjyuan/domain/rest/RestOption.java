package zhongjyuan.domain.rest;

import java.io.Serializable;

/**
 * @className: RestOption
 * @description: Rest请求明细对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:57:36
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestOption implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields key: 键
	 */
	protected String key;

	/**
	 * @fields value: 值
	 */
	protected String value;

	/**
	 * @fields description: 描述
	 */
	protected String description;

	/**
	 * @title: RestOption
	 * @author: zhongjyuan
	 * @description: Rest请求明细对象
	 * @throws
	 */
	public RestOption() {

	}

	/**
	 * @title: RestOption
	 * @author: zhongjyuan
	 * @description: Rest请求明细对象
	 * @param key 键
	 * @throws
	 */
	public RestOption(String key) {
		this.key = key;
	}

	/**
	 * @title: RestOption
	 * @author: zhongjyuan
	 * @description: Rest请求明细对象
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public RestOption(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * @title: RestOption
	 * @author: zhongjyuan
	 * @description: Rest请求明细对象
	 * @param key 键
	 * @param value 值
	 * @param description 描述
	 * @throws
	 */
	public RestOption(String key, String value, String description) {
		this.key = key;
		this.value = value;
		this.description = description;
	}

	/**
	 * @title: getKey
	 * @author: zhongjyuan
	 * @description: 获取键
	 * @return 键
	 * @throws
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title: setKey
	 * @author: zhongjyuan
	 * @description: 设置键
	 * @param key 键
	 * @throws
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title: getValue
	 * @author: zhongjyuan
	 * @description: 获取值
	 * @return 值
	 * @throws
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @title: setValue
	 * @author: zhongjyuan
	 * @description: 设置值
	 * @param value 值
	 * @throws
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @title: getDescription
	 * @author: zhongjyuan
	 * @description: 获取描述
	 * @return 描述
	 * @throws
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title: setDescription
	 * @author: zhongjyuan
	 * @description: 设置描述 
	 * @param description 描述
	 * @throws
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
