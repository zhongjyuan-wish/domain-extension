package zhongjyuan.domain.rest;

import java.io.Serializable;

/**
 * @className: RestAuthorization
 * @description: Rest请求认证对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:57:58
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestAuthorization implements Serializable {

	private static final long serialVersionUID = 1L;

	// Current|Basic|OAuth2.0
	/**
	 * @fields type: 类型[Current|Basic|OAuth2.0]
	 */
	protected String type;

	// Basic UserName
	/**
	 * @fields userName: 用户名
	 */
	protected String userName;

	// Basic Password
	/**
	 * @fields password: 密码
	 */
	protected String password;

	// Current Token
	/**
	 * @fields currentToken: 当前令牌
	 */
	protected String currentToken;

	// variable|param|header
	/**
	 * @fields tokenTo: 令牌位置[variable|param|header]
	 */
	protected String tokenTo;

	// Bearer
	/**
	 * @fields headerPrefix: Header前缀
	 */
	protected String headerPrefix;

	// OAuth2.0 CheckUrl
	/**
	 * @fields checkRequest: 校验请求
	 */
	protected RestRequest checkRequest;

	// OAuth2.0 GenerateUrl
	/**
	 * @fields generateRequest: 生成请求
	 */
	protected RestRequest generateRequest;

	// OAuth2.0 ExchangeUrl
	/**
	 * @fields exchangeRequest: 变更请求
	 */
	protected RestRequest exchangeRequest;

	/**
	 * @title: RestAuthorization
	 * @author: zhongjyuan
	 * @description: Rest请求认证对象
	 * @throws
	 */
	public RestAuthorization() {

	}

	/**
	 * @title: RestAuthorization
	 * @author: zhongjyuan
	 * @description: Rest请求认证对象
	 * @param tokenTo 令牌位置[variable|param|header]
	 * @param userName 用户名
	 * @param password 密码
	 * @param headerPrefix Header前缀
	 * @throws
	 */
	public RestAuthorization(String tokenTo, String userName, String password, String headerPrefix) {
		this.setType("Basic");
		this.setTokenTo(tokenTo);
		this.setUserName(userName);
		this.setPassword(password);
		this.setHeaderPrefix(headerPrefix);
	}

	/**
	 * @title: RestAuthorization
	 * @author: zhongjyuan
	 * @description: Rest请求认证对象
	 * @param tokenTo 令牌位置[variable|param|header]
	 * @param currentToken 当前令牌
	 * @param headerPrefix Header前缀
	 * @throws
	 */
	public RestAuthorization(String tokenTo, String currentToken, String headerPrefix) {
		this.setType("Current");
		this.setTokenTo(tokenTo);
		this.setCurrentToken(currentToken);
		this.setHeaderPrefix(headerPrefix);
	}

	/**
	 * @title:  getType
	 * @description: 获取类型
	 * @return: 类型[Current|Basic|OAuth2.0]
	 */
	public String getType() {
		return type;
	}

	/**
	 * @title:  setType
	 * @description: 设置类型
	 * @param type 类型[Current|Basic|OAuth2.0]
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @title:  getUserName
	 * @description: 获取用户名
	 * @return: 用户名
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @title:  setUserName
	 * @description: 设置用户名
	 * @param userName 用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @title:  getPassword
	 * @description: 获取密码
	 * @return: 密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @title:  setPassword
	 * @description: 设置密码
	 * @param password 密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @title:  getCurrentToken
	 * @description: 获取当前令牌
	 * @return: 当前令牌
	 */
	public String getCurrentToken() {
		return currentToken;
	}

	/**
	 * @title:  setCurrentToken
	 * @description: 设置当前令牌
	 * @param currentToken 当前令牌
	 */
	public void setCurrentToken(String currentToken) {
		this.currentToken = currentToken;
	}

	/**
	 * @title:  getTokenTo
	 * @description: 获取令牌位置
	 * @return: 令牌位置[variable|param|header]
	 */
	public String getTokenTo() {
		return tokenTo;
	}

	/**
	 * @title:  setTokenTo
	 * @description: 设置令牌位置
	 * @param tokenTo 令牌位置[variable|param|header]
	 */
	public void setTokenTo(String tokenTo) {
		this.tokenTo = tokenTo;
	}

	/**
	 * @title:  getHeaderPrefix
	 * @description: 获取Header前缀
	 * @return: Header前缀
	 */
	public String getHeaderPrefix() {
		return headerPrefix;
	}

	/**
	 * @title:  setHeaderPrefix
	 * @description: 设置Header前缀
	 * @param headerPrefix Header前缀
	 */
	public void setHeaderPrefix(String headerPrefix) {
		this.headerPrefix = headerPrefix;
	}

	/**
	 * @title:  getCheckRequest
	 * @description: 获取校验请求
	 * @return: 校验请求
	 */
	public RestRequest getCheckRequest() {
		return checkRequest;
	}

	/**
	 * @title:  setCheckRequest
	 * @description: 设置校验请求
	 * @param checkRequest 校验请求
	 */
	public void setCheckRequest(RestRequest checkRequest) {
		this.checkRequest = checkRequest;
	}

	/**
	 * @title:  getGenerateRequest
	 * @description: 获取生成请求
	 * @return: 生成请求
	 */
	public RestRequest getGenerateRequest() {
		return generateRequest;
	}

	/**
	 * @title:  setGenerateRequest
	 * @description: 设置生成请求
	 * @param generateRequest 生成请求
	 */
	public void setGenerateRequest(RestRequest generateRequest) {
		this.generateRequest = generateRequest;
	}

	/**
	 * @title:  getExchangeRequest
	 * @description: 获取变更请求
	 * @return: 变更请求
	 */
	public RestRequest getExchangeRequest() {
		return exchangeRequest;
	}

	/**
	 * @title:  setExchangeRequest
	 * @description: 设置变更请求
	 * @param exchangeRequest 变更请求
	 */
	public void setExchangeRequest(RestRequest exchangeRequest) {
		this.exchangeRequest = exchangeRequest;
	}
}
