package zhongjyuan.domain.rest;

import java.io.Serializable;

/**
 * @className: RestBody
 * @description: Rest请求Body对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:57:17
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields mediaType: 媒体类型
	 */
	protected String mediaType;

	/**
	 * @fields content: 正文
	 */
	protected String content;

	/**
	 * @title: RestBody
	 * @author: zhongjyuan
	 * @description: Rest请求Body对象
	 * @throws
	 */
	public RestBody() {

	}

	/**
	 * @title: RestBody
	 * @author: zhongjyuan
	 * @description: Rest请求Body对象
	 * @param mediaType 媒体类型
	 * @throws
	 */
	public RestBody(String mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * @title: RestBody
	 * @author: zhongjyuan
	 * @description: Rest请求Body对象
	 * @param mediaType 媒体类型
	 * @param content 正文
	 * @throws
	 */
	public RestBody(String mediaType, String content) {
		this.mediaType = mediaType;
		this.content = content;
	}

	/**
	 * @title: getMediaType
	 * @author: zhongjyuan
	 * @description: 获取媒体类型
	 * @return 媒体类型
	 * @throws
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * @title: setMediaType
	 * @author: zhongjyuan
	 * @description: 设置媒体类型
	 * @param mediaType 媒体类型
	 * @throws
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * @title: getContent
	 * @author: zhongjyuan
	 * @description: 获取正文
	 * @return 正文
	 * @throws
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @title: setContent
	 * @author: zhongjyuan
	 * @description: 设置正文
	 * @param content 正文
	 * @throws
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
