package zhongjyuan.domain.rest;

/**
 * @className: RestMediaType
 * @description: 媒体类型
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:53:21
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum RestMediaType {

	/**
	 * @fields HTML: HTML
	 */
	HTML("HTML", "text/html"),

	/**
	 * @fields TEXT: TEXT
	 */
	TEXT("Text", "text/plain"),

	/**
	 * @fields XML: XML
	 */
	XML("XML", "application/xml"),

	/**
	 * @fields JSON: JSON
	 */
	JSON("JSON", "application/json"),

	/**
	 * @fields JAVASCRIPT: JAVASCRIPT
	 */
	JAVASCRIPT("JavaScript", "application/javascript"),

	;

	/**
	 * @fields code: 编码
	 */
	private String code;

	/**
	 * @fields description: 描述
	 */
	private String description;

	/**
	 * @title: RestMediaType
	 * @author: zhongjyuan
	 * @description: 媒体类型
	 * @param code 编码
	 * @param description 描述
	 * @throws
	 */
	private RestMediaType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: getDescription
	 * @author: zhongjyuan
	 * @description: 获取描述
	 * @return 描述
	 * @throws
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: setDescription
	 * @author: zhongjyuan
	 * @description: 设置描述
	 * @param description 描述
	 * @throws
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
