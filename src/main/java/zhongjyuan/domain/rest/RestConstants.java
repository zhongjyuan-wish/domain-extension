package zhongjyuan.domain.rest;

/**
 * @className: RestConstants
 * @description: Rest常量对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:56:09
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestConstants {

	/**
	 * @fields HTTP: HTTP
	 */
	public final static String HTTP = "HTTP";

	/**
	 * @fields HTTPS: HTTPS
	 */
	public final static String HTTPS = "HTTPS";

	/**
	 * @fields GET: GET
	 */
	public final static String GET = "GET";

	/**
	 * @fields PUT: PUT
	 */
	public final static String PUT = "PUT";

	/**
	 * @fields POST: POST
	 */
	public final static String POST = "POST";

	/**
	 * @fields DELETE: DELETE
	 */
	public final static String DELETE = "DELETE";

	/**
	 * @fields CURRENT: CURRENT
	 */
	public final static String CURRENT = "CURRENTTOKEN";

	/**
	 * @fields BASIC: BASIC
	 */
	public final static String BASIC = "BASIC";

	/**
	 * @fields OAUTH2: OAUTH2
	 */
	public final static String OAUTH2 = "OAUTH2.0";

	/**
	 * @fields PARAM: PARAM
	 */
	public final static String PARAM = "PARAM";

	/**
	 * @fields HEADER: HEADER
	 */
	public final static String HEADER = "HEADER";

	/**
	 * @fields VARIABLE: VARIABLE
	 */
	public final static String VARIABLE = "VARIABLE";

	/**
	 * @fields TARGET_ARRAY: TARGET_ARRAY
	 */
	public final static String TARGET_ARRAY = "Array";

	/**
	 * @fields TARGET_OBJECT: TARGET_OBJECT
	 */
	public final static String TARGET_OBJECT = "Object";

	/**
	 * @fields TARGET_STRING: TARGET_STRING
	 */
	public final static String TARGET_STRING = "String";

	/**
	 * @fields HEADER_PREFIX: HEADER_PREFIX
	 */
	public final static String HEADER_PREFIX = "Bearer";
}
