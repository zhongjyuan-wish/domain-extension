package zhongjyuan.domain.rest;

import java.io.Serializable;

/**
 * @className: RestModel
 * @description: Rest请求响应模型对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:57:00
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestModel implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields type: 类型
	 */
	protected String type;

	/**
	 * @fields length: 长度
	 */
	protected int length;

	/**
	 * @fields precision: 精度
	 */
	protected int precision;

	/**
	 * @fields rowIndex: 行号
	 */
	protected int rowIndex;

	/**
	 * @title:  getCode
	 * @description: 获取编码
	 * @return: 编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title:  setCode
	 * @description: 设置编码
	 * @param code 编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title:  getName
	 * @description: 获取名称
	 * @return: 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title:  setName
	 * @description: 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title:  getType
	 * @description: 获取类型
	 * @return: 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * @title:  setType
	 * @description: 设置类型
	 * @param type 类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @title:  getLength
	 * @description: 获取长度
	 * @return: 长度
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @title:  setLength
	 * @description: 设置长度
	 * @param length 长度
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @title:  getPrecision
	 * @description: 获取精度
	 * @return: 精度
	 */
	public int getPrecision() {
		return precision;
	}

	/**
	 * @title:  setPrecision
	 * @description: 设置精度
	 * @param precision 精度
	 */
	public void setPrecision(int precision) {
		this.precision = precision;
	}

	/**
	 * @title:  getRowIndex
	 * @description: 获取行号
	 * @return: 行号
	 */
	public int getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title:  setRowIndex
	 * @description: 设置行号
	 * @param rowIndex 行号
	 */
	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}
}
