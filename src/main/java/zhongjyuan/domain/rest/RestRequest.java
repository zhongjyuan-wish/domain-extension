package zhongjyuan.domain.rest;

import java.io.Serializable;
import java.util.List;

/**
 * @className: RestRequest
 * @description: Request请求对象
 * @author: zhongjyuan
 * @date: 2022年12月5日 下午6:58:25
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	// 请求url
	/**
	 * @fields url: url
	 */
	protected String url;

	// 请求方法
	/**
	 * @fields method: 方法
	 */
	protected String method;

	// 请求协议
	/**
	 * @fields protocol: 协议
	 */
	protected String protocol;

	// 认证对象
	/**
	 * @fields authorization: 认证对象
	 */
	protected RestAuthorization authorization;

	// 请求占位符
	/**
	 * @fields variables: 占位符集
	 */
	protected List<RestOption> variables;

	// 请求参数
	/**
	 * @fields params: 参数集
	 */
	protected List<RestOption> params;

	// 请求头参数
	/**
	 * @fields headers: 请求头集
	 */
	protected List<RestOption> headers;

	// 请求体
	/**
	 * @fields body: 请求体
	 */
	protected RestBody body;

	// 获取目标对象
	/**
	 * @fields targetDataKey: 目标对象键
	 */
	protected String targetDataKey;

	// 目标对象是否集合
	/**
	 * @fields targetDataType: 目标对象类型
	 */
	protected String targetDataType = "Object";

	// 请求响应对象
	/**
	 * @fields models: 响应模型集
	 */
	protected List<RestModel> models;

	/**
	 * @title:  getUrl
	 * @description: 获取Url
	 * @return: Url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @title:  setUrl
	 * @description: 设置Url
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @title:  getMethod
	 * @description: 获取方法
	 * @return: 方法
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @title:  setMethod
	 * @description: 设置方法
	 * @param method 方法
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @title:  getProtocol
	 * @description: 获取协议
	 * @return: 协议
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @title:  setProtocol
	 * @description: 设置协议
	 * @param protocol 协议
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @title:  getAuthorization
	 * @description: 获取认证对象
	 * @return: 认证对象
	 */
	public RestAuthorization getAuthorization() {
		return authorization;
	}

	/**
	 * @title:  setAuthorization
	 * @description: 设置认证对象
	 * @param authorization 认证对象
	 */
	public void setAuthorization(RestAuthorization authorization) {
		this.authorization = authorization;
	}

	/**
	 * @title:  getVariables
	 * @description: 获取占位符集
	 * @return: 占位符集
	 */
	public List<RestOption> getVariables() {
		return variables;
	}

	/**
	 * @title: getVariable
	 * @author: zhongjyuan
	 * @description: 获取占位符
	 * @param index 索引
	 * @return 占位符
	 * @throws
	 */
	public RestOption getVariable(int index) {
		return variables.get(index);
	}

	/**
	 * @title:  setVariables
	 * @description: 设置占位符集
	 * @param variables 占位符集
	 */
	public void setVariables(List<RestOption> variables) {
		this.variables = variables;
	}

	/**
	 * @title: setVariable
	 * @author: zhongjyuan
	 * @description: 设置占位符
	 * @param variable 占位符
	 * @throws
	 */
	public void setVariable(RestOption variable) {
		this.variables.add(variable);
	}

	/**
	 * @title:  getParams
	 * @description: 获取参数集
	 * @return: 参数集
	 */
	public List<RestOption> getParams() {
		return params;
	}

	/**
	 * @title: getParam
	 * @author: zhongjyuan
	 * @description: 获取参数
	 * @param index 索引
	 * @return 参数
	 * @throws
	 */
	public RestOption getParam(int index) {
		return params.get(index);
	}

	/**
	 * @title:  setParams
	 * @description: 设置参数集
	 * @param params 参数集
	 */
	public void setParams(List<RestOption> params) {
		this.params = params;
	}

	/**
	 * @title: setParam
	 * @author: zhongjyuan
	 * @description: 设置参数
	 * @param param 参数
	 * @throws
	 */
	public void setParam(RestOption param) {
		this.params.add(param);
	}

	/**
	 * @title:  getHeaders
	 * @description: 获取请求头集
	 * @return: 请求头集
	 */
	public List<RestOption> getHeaders() {
		return headers;
	}

	/**
	 * @title: getHeader
	 * @author: zhongjyuan
	 * @description: 获取请求头
	 * @param index 索引
	 * @return 请求头
	 * @throws
	 */
	public RestOption getHeader(int index) {
		return headers.get(index);
	}

	/**
	 * @title:  setHeaders
	 * @description: 设置请求头集
	 * @param headers 请求头集
	 */
	public void setHeaders(List<RestOption> headers) {
		this.headers = headers;
	}

	/**
	 * @title: setHeader
	 * @author: zhongjyuan
	 * @description: 设置请求头
	 * @param header 请求头
	 * @throws
	 */
	public void setHeader(RestOption header) {
		this.headers.add(header);
	}

	/**
	 * @title:  getBody
	 * @description: 获取请求体
	 * @return: 请求体
	 */
	public RestBody getBody() {
		return body;
	}

	/**
	 * @title:  setBody
	 * @description: 设置请求体
	 * @param body 请求体
	 */
	public void setBody(RestBody body) {
		this.body = body;
	}

	/**
	 * @title:  getTargetDataKey
	 * @description: 获取目标数据键
	 * @return: 目标数据键
	 */
	public String getTargetDataKey() {
		return targetDataKey;
	}

	/**
	 * @title:  setTargetDataKey
	 * @description: 设置目标数据键
	 * @param targetDataKey 目标数据键
	 */
	public void setTargetDataKey(String targetDataKey) {
		this.targetDataKey = targetDataKey;
	}

	/**
	 * @title:  getTargetDataType
	 * @description: 获取目标数据类型
	 * @return: 目标数据类型
	 */
	public String getTargetDataType() {
		return targetDataType;
	}

	/**
	 * @title:  setTargetDataType
	 * @description: 设置目标数据类型
	 * @param targetDataType 目标数据类型
	 */
	public void setTargetDataType(String targetDataType) {
		this.targetDataType = targetDataType;
	}

	/**
	 * @title:  getModels
	 * @description: 获取响应模型集
	 * @return: 响应模型集
	 */
	public List<RestModel> getModels() {
		return models;
	}

	/**
	 * @title: getModel
	 * @author: zhongjyuan
	 * @description: 获取响应模型
	 * @param index 索引
	 * @return 响应模型
	 * @throws
	 */
	public RestModel getModel(int index) {
		return models.get(index);
	}

	/**
	 * @title:  setModels
	 * @description: 设置响应模型集
	 * @param models 响应模型集
	 */
	public void setModels(List<RestModel> models) {
		this.models = models;
	}

	/**
	 * @title: setModel
	 * @author: zhongjyuan
	 * @description: 设置响应模型
	 * @param model 响应模型
	 * @throws
	 */
	public void setModel(RestModel model) {
		this.models.add(model);
	}
}
