package zhongjyuan.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.annotation.JSONField;

import zhongjyuan.domain.plugin.IProvider;
import zhongjyuan.domain.relate.Relate;

/**
 * @className: Context
 * @description: 上下文对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午3:01:43
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Context extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields DEFAULT_PLUGIN_CODE: 默认插件编码字段名称常量
	 */
	protected static final String DEFAULT_PLUGIN_CODE = "defaultPluginCode";

	/**
	 * @fields code: 编码
	 */
	protected String code = Constant.FRAMEWORK_NAME;

	/**
	 * @fields name: 名称
	 */
	protected String name = Constant.FRAMEWORK_NAME;

	/**
	 * @fields time: 时间戳
	 */
	protected Long time = System.currentTimeMillis();

	/**
	 * @fields success: 是否成功
	 */
	protected Boolean success = false;

	/**
	 * @fields data: 数据对象
	 */
	protected Object data = null;

	/**
	 * @fields message: 消息对象
	 */
	protected Object message = "FAIL.";

	/**
	 * @fields description: 描述对象
	 */
	protected Object description = "PROVIDER FAIL.";

	/**
	 * @fields contextId: 上下文主键
	 */
	protected String contextId = UUID.randomUUID().toString();

	/**
	 * @fields executionId: 执行主键[重试机制]
	 */
	protected String executionId = UUID.randomUUID().toString();

	/**
	 * @fields defaultPluginCode: 默认插件编码
	 */
	protected String defaultPluginCode = Constant.FRAMEWORK_NAME;

	/**
	 * @fields request: HttpServletRequest对象
	 */
	@JSONField(
		serialize = false)
	protected HttpServletRequest request = null;

	/**
	 * @fields response: HttpServletResponse对象
	 */
	@JSONField(
		serialize = false)
	protected HttpServletResponse response = null;

	/**
	 * @fields relates: 关联对象集合
	 */
	@JSONField(
		serialize = false)
	protected List<Relate> relates = new ArrayList<Relate>();

	/**
	 * @fields properties: 属性Map集
	 */
	@JSONField(
		serialize = false)
	protected Map<String, Object> properties = new HashMap<String, Object>();

	/**
	 * @fields providers: 提供者Map集
	 */
	@JSONField(
		serialize = false)
	protected Map<String, IProvider> providers = new HashMap<String, IProvider>();

	/**
	 * @fields models: 模型Map集
	 */
	@JSONField(
		serialize = false)
	protected Map<String, AbstractModel> models = new HashMap<String, AbstractModel>();

	/**
	 * @fields traces: 上下文跟踪对象集合
	 */
	@JSONField(
		serialize = false)
	protected Map<String, List<ContextTrace>> traces = new HashMap<String, List<ContextTrace>>();

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @throws
	 */
	public Context() {

	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param properties 属性Map集
	 * @throws
	 */
	public Context(Map<String, Object> properties) {
		this.properties = properties;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param properties 属性Map集
	 * @param providers 提供者Map集
	 * @throws
	 */
	public Context(Map<String, Object> properties, Map<String, IProvider> providers) {
		this.providers = providers;
		this.properties = properties;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param properties 属性Map集
	 * @param providers 提供者Map集
	 * @param models 模型Map集
	 * @throws
	 */
	public Context(Map<String, Object> properties, Map<String, IProvider> providers, Map<String, AbstractModel> models) {
		this.models = models;
		this.providers = providers;
		this.properties = properties;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param request Request对象
	 * @param response Response对象
	 * @throws
	 */
	public Context(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param request Request对象
	 * @param response Response对象
	 * @param properties 属性Map集
	 * @throws
	 */
	public Context(HttpServletRequest request, HttpServletResponse response, Map<String, Object> properties) {
		this.request = request;
		this.response = response;

		this.properties = properties;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param request Request对象
	 * @param response Response对象
	 * @param properties 属性Map集
	 * @param providers 提供者Map集
	 * @throws
	 */
	public Context(HttpServletRequest request, HttpServletResponse response, Map<String, Object> properties, Map<String, IProvider> providers) {
		this.request = request;
		this.response = response;

		this.providers = providers;
		this.properties = properties;
	}

	/**
	 * @title Context
	 * @author zhongjyuan
	 * @description 上下文对象
	 * @param request Request对象
	 * @param response Response对象
	 * @param properties 属性Map集
	 * @param providers 提供者Map集
	 * @param models 模型Map集
	 * @throws
	 */
	public Context(HttpServletRequest request, HttpServletResponse response, Map<String, Object> properties, Map<String, IProvider> providers, Map<String, AbstractModel> models) {
		this.request = request;
		this.response = response;

		this.models = models;
		this.providers = providers;
		this.properties = properties;
	}

	/**
	 * @title load
	 * @author zhongjyuan
	 * @description 装载
	 * @param context 上下文对象
	 * @return Context 上下文对象
	 * @throws
	 */
	public Context load(Context context) {
		if (context != null)
			BeanUtils.copyProperties(context, this);

		if (this.properties != null && this.properties.containsKey(Constant.MODULE_CODE_L_A))
			setCode(this.properties.get(Constant.MODULE_CODE_L_A).toString());

		if (StringUtils.isEmpty(this.code) && ObjectUtils.isNotEmpty(this.request))
			setCode(this.request.getParameter(Constant.MODULE_CODE_L_A));

		if (this.properties != null && this.properties.containsKey(Constant.MODULE_NAME_L_A))
			setName(this.properties.get(Constant.MODULE_NAME_L_A).toString());

		if (StringUtils.isEmpty(this.name) && ObjectUtils.isNotEmpty(this.request))
			setName(this.request.getParameter(Constant.MODULE_NAME_L_A));

		if (this.properties != null && this.properties.containsKey(Constant.CONTEXT_ID_L_A))
			setContextId(this.properties.get(Constant.CONTEXT_ID_L_A).toString());

		if (StringUtils.isEmpty(this.contextId) && ObjectUtils.isNotEmpty(this.request))
			setContextId(this.request.getParameter(Constant.CONTEXT_ID_L_A));

		if (this.properties != null && this.properties.containsKey(Constant.EXECUTION_ID_L_A))
			setExecutionId(this.properties.get(Constant.EXECUTION_ID_L_A).toString());

		if (StringUtils.isEmpty(this.executionId) && ObjectUtils.isNotEmpty(this.request))
			setExecutionId(this.request.getParameter(Constant.EXECUTION_ID_L_A));

		if (this.properties != null && this.properties.containsKey(DEFAULT_PLUGIN_CODE))
			setDefaultPluginCode(this.properties.get(DEFAULT_PLUGIN_CODE).toString());

		if (StringUtils.isEmpty(this.defaultPluginCode) && ObjectUtils.isNotEmpty(this.request))
			setDefaultPluginCode(this.request.getParameter(DEFAULT_PLUGIN_CODE));

		return this;
	}

	/**
	 * @title getCode
	 * @author zhongjyuan
	 * @description 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title setCode
	 * @author zhongjyuan
	 * @description 设置编码
	 * @param code 编码
	 * @return 上下文对象
	 * @throws
	 */
	public Context setCode(String code) {
		return setCode(code, true);
	}

	/**
	 * @title setCode
	 * @author zhongjyuan
	 * @description 设置编码
	 * @param code 编码
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setCode(String code, Boolean cover) {
		cover = (cover != null && cover) ? cover : StringUtils.isEmpty(this.code);

		if (cover != null && cover)
			this.code = code;

		return this;
	}

	/**
	 * @title getName
	 * @author zhongjyuan
	 * @description 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title setName
	 * @author zhongjyuan
	 * @description 设置名称
	 * @param name 名称
	 * @return 上下文对象
	 * @throws
	 */
	public Context setName(String name) {
		return setName(name, true);
	}

	/**
	 * @title setName
	 * @author zhongjyuan
	 * @description 设置名称
	 * @param name 名称
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setName(String name, Boolean cover) {
		cover = (cover != null && cover) ? cover : StringUtils.isEmpty(this.name);

		if (cover != null && cover)
			this.name = name;

		return this;
	}

	/**
	 * @title getTime
	 * @author zhongjyuan
	 * @description 获取时间戳
	 * @return 时间戳
	 * @throws
	 */
	public Long getTime() {
		return time;
	}

	/**
	 * @title setTime
	 * @author zhongjyuan
	 * @description 设置时间戳
	 * @param time 时间戳
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTime(Long time) {
		return setTime(time, true);
	}

	/**
	 * @title setTime
	 * @author zhongjyuan
	 * @description 设置时间戳
	 * @param time 时间戳
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTime(Long time, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.time);

		if (cover != null && cover)
			this.time = time;

		return this;
	}

	/**
	 * @title success
	 * @author zhongjyuan
	 * @description 是否成功
	 * @return 是否成功
	 * @throws
	 */
	public Boolean success() {
		return success;
	}

	/**
	 * @title setSuccess
	 * @author zhongjyuan
	 * @description 设置是否成功
	 * @param success 是否成功
	 * @return 上下文对象
	 * @throws
	 */
	public Context setSuccess(Boolean success) {
		return setSuccess(success, true);
	}

	/**
	 * @title setSuccess
	 * @author zhongjyuan
	 * @description 设置是否成功
	 * @param success 是否成功
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setSuccess(Boolean success, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.success);

		if (cover != null && cover)
			this.success = success;

		return this;
	}

	/**
	 * @title getData
	 * @author zhongjyuan
	 * @description 获取数据
	 * @return 数据
	 * @throws
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @title setData
	 * @author zhongjyuan
	 * @description 设置数据
	 * @param data 数据
	 * @return 上下文对象
	 * @throws
	 */
	public Context setData(Object data) {
		return setData(data, true);
	}

	/**
	 * @title setData
	 * @author zhongjyuan
	 * @description 设置数据
	 * @param data 数据
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setData(Object data, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.data);

		if (cover != null && cover)
			this.data = data;

		return this;
	}

	/**
	 * @title getMessage
	 * @author zhongjyuan
	 * @description 获取消息
	 * @return 消息
	 * @throws
	 */
	public Object getMessage() {
		return message;
	}

	/**
	 * @title setMessage
	 * @author zhongjyuan
	 * @description 设置消息
	 * @param message 消息
	 * @return 上下文对象
	 * @throws
	 */
	public Context setMessage(Object message) {
		return setMessage(message, true);
	}

	/**
	 * @title setMessage
	 * @author zhongjyuan
	 * @description 设置消息
	 * @param message 消息
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setMessage(Object message, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.message);

		if (cover != null && cover)
			this.message = message;

		return this;
	}

	/**
	 * @title getDescription
	 * @author zhongjyuan
	 * @description 获取描述
	 * @return 描述
	 * @throws
	 */
	public Object getDescription() {
		return description;
	}

	/**
	 * @title setDescription
	 * @author zhongjyuan
	 * @description 设置描述
	 * @param description 描述
	 * @return 上下文对象
	 * @throws
	 */
	public Context setDescription(Object description) {
		return setDescription(description, true);
	}

	/**
	 * @title setDescription
	 * @author zhongjyuan
	 * @description 设置描述
	 * @param description 描述
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setDescription(Object description, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.description);

		if (cover != null && cover)
			this.description = description;

		return this;
	}

	/**
	 * @title getContextId
	 * @author zhongjyuan
	 * @description 获取上下文主键
	 * @return 上下文主键
	 * @throws
	 */
	public String getContextId() {
		return contextId;
	}

	/**
	 * @title refreshContextId
	 * @author zhongjyuan
	 * @description 属性上下文主键
	 * @return 上下文对象
	 * @throws
	 */
	public Context refreshContextId() {
		this.contextId = UUID.randomUUID().toString();
		return this;
	}

	/**
	 * @title setContextId
	 * @author zhongjyuan
	 * @description 设置上下文主键
	 * @param contextId 上下文主键
	 * @return 上下文对象
	 * @throws
	 */
	public Context setContextId(String contextId) {
		return setContextId(contextId, true);
	}

	/**
	 * @title setContextId
	 * @author zhongjyuan
	 * @description 设置上下文主键
	 * @param contextId 上下文主键
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setContextId(String contextId, Boolean cover) {
		cover = (cover != null && cover) ? cover : StringUtils.isEmpty(this.contextId);

		if (cover != null && cover)
			this.contextId = contextId;

		return this;
	}

	/**
	 * @title getExecutionId
	 * @author zhongjyuan
	 * @description 获取执行主键
	 * @return 执行主键
	 * @throws
	 */
	public String getExecutionId() {
		return executionId;
	}

	/**
	 * @title refreshExecutionId
	 * @author zhongjyuan
	 * @description 刷新执行主键
	 * @return 上下文对象
	 * @throws
	 */
	public Context refreshExecutionId() {
		this.executionId = UUID.randomUUID().toString();
		return this;
	}

	/**
	 * @title setExecutionId
	 * @author zhongjyuan
	 * @description 设置执行主键
	 * @param executionId 执行主键
	 * @return 上下文对象
	 * @throws
	 */
	public Context setExecutionId(String executionId) {
		return setExecutionId(executionId, true);
	}

	/**
	 * @title setExecutionId
	 * @author zhongjyuan
	 * @description 设置执行主键
	 * @param executionId 执行主键
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setExecutionId(String executionId, Boolean cover) {
		cover = (cover != null && cover) ? cover : StringUtils.isEmpty(this.executionId);

		if (cover != null && cover)
			this.executionId = executionId;

		return this;
	}

	/**
	 * @title getDefaultPluginCode
	 * @author zhongjyuan
	 * @description 获取默认插件编码
	 * @return 默认插件编码
	 * @throws
	 */
	public String getDefaultPluginCode() {
		return defaultPluginCode;
	}

	/**
	 * @title setDefaultPluginCode
	 * @author zhongjyuan
	 * @description 设置默认插件编码
	 * @param defaultPluginCode 默认插件编码
	 * @return 上下文对象
	 * @throws
	 */
	public Context setDefaultPluginCode(String defaultPluginCode) {
		return setDefaultPluginCode(defaultPluginCode, true);
	}

	/**
	 * @title setDefaultPluginCode
	 * @author zhongjyuan
	 * @description 设置默认插件编码
	 * @param defaultPluginCode 默认插件编码
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setDefaultPluginCode(String defaultPluginCode, Boolean cover) {
		cover = (cover != null && cover) ? cover : StringUtils.isEmpty(this.defaultPluginCode);

		if (cover != null && cover)
			this.defaultPluginCode = defaultPluginCode;

		return this;
	}

	/**
	 * @title cleanRequest
	 * @author zhongjyuan
	 * @description 清除请求对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanRequest() {
		this.request = null;
		return this;
	}

	/**
	 * @title getRequest
	 * @author zhongjyuan
	 * @description 获取请求对象
	 * @return 请求对象
	 * @throws
	 */
	public HttpServletRequest getRequest() {
		return this.request;
	}

	/**
	 * @title setRequest
	 * @author zhongjyuan
	 * @description 设置请求对象
	 * @param request 请求对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRequest(HttpServletRequest request) {
		return setRequest(request, true);
	}

	/**
	 * @title setRequest
	 * @author zhongjyuan
	 * @description 设置请求对象
	 * @param request 请求对象
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRequest(HttpServletRequest request, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.request);

		if (cover != null && cover)
			this.request = request;

		return this;
	}

	/**
	 * @title cleanResponse
	 * @author zhongjyuan
	 * @description 清除响应对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanResponse() {
		this.response = null;
		return this;
	}

	/**
	 * @title getResponse
	 * @author zhongjyuan
	 * @description 获取响应对象
	 * @return 响应对象
	 * @throws
	 */
	public HttpServletResponse getResponse() {
		return this.response;
	}

	/**
	 * @title setResponse
	 * @author zhongjyuan
	 * @description 设置响应对象
	 * @param response 响应对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context setResponse(HttpServletResponse response) {
		return setResponse(response, true);
	}

	/**
	 * @title setResponse
	 * @author zhongjyuan
	 * @description 设置响应对象
	 * @param response 响应对象
	 * @param cover 覆盖[默认true,原本无值时直接默认覆盖]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setResponse(HttpServletResponse response, Boolean cover) {
		cover = (cover != null && cover) ? cover : ObjectUtils.isEmpty(this.response);

		if (cover != null && cover)
			this.response = response;

		return this;
	}

	/**
	 * @title cleanRelates
	 * @author zhongjyuan
	 * @description 清除关联对象集合
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanRelates() {
		this.relates.clear();
		return this;
	}

	/**
	 * @title getRelates
	 * @author zhongjyuan
	 * @description 获取关联对象集合
	 * @return 关联对象集合
	 * @throws
	 */
	public List<Relate> getRelates() {
		return relates;
	}

	/**
	 * @title setRelate
	 * @author zhongjyuan
	 * @description 设置关联对象
	 * @param relate 关联对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRelate(Relate relate) {
		return setRelate(relate, false);
	}

	/**
	 * @title setRelate
	 * @author zhongjyuan
	 * @description 设置关联对象
	 * @param relate 关联对象
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRelate(Relate relate, Boolean clean) {
		if (this.relates == null || this.relates.size() < 1)
			this.relates = new ArrayList<Relate>();

		if (clean != null && clean)
			this.relates.clear();

		this.relates.add(relate);

		return this;
	}

	/**
	 * @title setRelates
	 * @author zhongjyuan
	 * @description 设置关联对象集合
	 * @param relates 关联对象集合
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRelates(List<Relate> relates) {
		return setRelates(relates, false);
	}

	/**
	 * @title setRelates
	 * @author zhongjyuan
	 * @description 设置关联对象集合
	 * @param relates 关联对象集合
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setRelates(List<Relate> relates, Boolean clean) {
		if (this.relates == null || this.relates.size() < 1)
			this.relates = new ArrayList<Relate>();

		if (clean != null && clean)
			this.relates.clear();

		this.relates.addAll(relates);

		return this;
	}

	/**
	 * @title cleanTraces
	 * @author zhongjyuan
	 * @description 清除跟踪对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanTraces() {
		this.traces.clear();
		return this;
	}

	/**
	 * @title cleanTraces
	 * @author zhongjyuan
	 * @description 清除跟踪对象
	 * @param executionId 执行主键
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanTraces(String executionId) {
		this.traces.remove(executionId);
		return this;
	}

	/**
	 * @title getTraces
	 * @author zhongjyuan
	 * @description 获取跟踪对象集合
	 * @return 跟踪对象Map集
	 * @throws
	 */
	public Map<String, List<ContextTrace>> getTraces() {
		return this.traces;
	}

	/**
	 * @title getTraces
	 * @author zhongjyuan
	 * @description 获取跟踪对象集合
	 * @return 跟踪对象集合
	 * @throws
	 */
	public List<ContextTrace> getTraces(String executionId) {
		if (this.traces != null && this.traces.size() > 0)
			return traces.get(executionId);
		return null;
	}

	/**
	 * @title setTrace
	 * @author zhongjyuan
	 * @description 设置跟踪对象
	 * @param trace 跟踪对象
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTrace(ContextTrace trace) {
		return setTrace(trace, false);
	}

	/**
	 * @title setTrace
	 * @author zhongjyuan
	 * @description 设置跟踪对象
	 * @param trace 跟踪对象
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTrace(ContextTrace trace, Boolean clean) {
		if (this.traces == null || this.traces.size() < 1)
			this.traces = new HashMap<String, List<ContextTrace>>();

		List<ContextTrace> contextTraces = new ArrayList<ContextTrace>();
		if (this.traces.containsKey(this.executionId))
			contextTraces = this.traces.get(this.executionId);

		if (clean != null && clean)
			contextTraces.clear();

		contextTraces.add(trace);

		this.traces.put(this.executionId, contextTraces);

		return this;
	}

	/**
	 * @title setTraces
	 * @author zhongjyuan
	 * @description 设置跟踪对象集合
	 * @param traces 跟踪对象集合
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTraces(List<ContextTrace> traces) {
		return setTraces(traces, false);
	}

	/**
	 * @title setTraces
	 * @author zhongjyuan
	 * @description 设置跟踪对象集合
	 * @param traces 跟踪对象集合
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setTraces(List<ContextTrace> traces, Boolean clean) {
		if (this.traces == null || this.traces.size() < 1)
			this.traces = new HashMap<String, List<ContextTrace>>();

		List<ContextTrace> contextTraces = new ArrayList<ContextTrace>();
		if (this.traces.containsKey(this.executionId))
			contextTraces = this.traces.get(this.executionId);

		if (clean != null && clean)
			contextTraces.clear();

		contextTraces.addAll(traces);

		this.traces.put(this.executionId, contextTraces);

		return this;
	}

	/**
	 * @title: cleanProperties
	 * @author: zhongjyuan
	 * @description: 清除属性
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanProperties() {
		this.properties.clear();
		return this;
	}

	/**
	 * @title getProperties
	 * @author zhongjyuan
	 * @description 获取属性Map集
	 * @return 属性Map集
	 * @throws
	 */
	public Map<String, Object> getProperties() {
		return this.properties;
	}

	/**
	 * @title getPropertie
	 * @author zhongjyuan
	 * @description 获取属性
	 * @param key 键
	 * @return 值
	 * @throws
	 */
	public Object getPropertie(String key) {
		if (this.properties != null && this.properties.size() > 0)
			return this.properties.get(key);
		return null;
	}

	/**
	 * @title setPropertie
	 * @author zhongjyuan
	 * @description 设置属性
	 * @param key 键
	 * @param value 值
	 * @return 上下文对象
	 * @throws
	 */
	public Context setPropertie(String key, Object value) {
		return setPropertie(key, value, false);
	}

	/**
	 * @title setPropertie
	 * @author zhongjyuan
	 * @description 设置属性
	 * @param key 键
	 * @param value 值
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setPropertie(String key, Object value, Boolean clean) {
		if (this.properties == null || this.properties.size() < 1)
			this.properties = new HashMap<String, Object>();

		if (clean != null && clean)
			this.properties.clear();

		this.properties.put(key, value);
		return this;
	}

	/**
	 * @title setProperties
	 * @author zhongjyuan
	 * @description 设置属性Map集
	 * @param properties 属性Map集
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProperties(Map<String, Object> properties) {
		return setProperties(properties, false);
	}

	/**
	 * @title setProperties
	 * @author zhongjyuan
	 * @description 设置属性Map集
	 * @param properties 属性Map集
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProperties(Map<String, Object> properties, Boolean clean) {
		if (this.properties == null || this.properties.size() < 1)
			this.properties = new HashMap<String, Object>();

		if (clean != null && clean)
			this.properties.clear();

		this.properties.putAll(properties);

		return this;
	}

	/**
	 * @title cleanProviders
	 * @author zhongjyuan
	 * @description 清除提供者
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanProviders() {
		this.providers.clear();
		return this;
	}

	/**
	 * @title getProvider
	 * @author zhongjyuan
	 * @description 获取提供者
	 * @param key 键
	 * @return 提供者接口对象
	 * @throws
	 */
	public IProvider getProvider(String key) {
		if (this.providers != null && this.providers.size() > 0)
			return this.providers.get(key);
		return null;
	}

	/**
	 * @title getProvider
	 * @author zhongjyuan
	 * @description 获取提供者
	 * @param <Provider> 提供者泛型对象
	 * @param provider 提供者对象
	 * @return 提供者对象
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public <Provider extends IProvider> Provider getProvider(Provider provider) {
		if (this.providers != null && this.providers.size() > 0)
			return (Provider) this.providers.get(provider.getClass().getName());
		return null;
	}

	/**
	 * @title getProviders
	 * @author zhongjyuan
	 * @description 获取提供者Map集
	 * @return 提供者Map集
	 * @throws
	 */
	public Map<String, IProvider> getProviders() {
		return this.providers;
	}

	/**
	 * @title setProvider
	 * @author zhongjyuan
	 * @description 设置提供者
	 * @param key 键
	 * @param value 值
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProvider(String key, IProvider value) {
		return setProvider(key, value, false);
	}

	/**
	 * @title setProvider
	 * @author zhongjyuan
	 * @description 设置提供者
	 * @param key 键
	 * @param value 值
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProvider(String key, IProvider value, Boolean clean) {
		if (this.providers == null || this.providers.size() < 1)
			this.providers = new HashMap<String, IProvider>();

		if (clean != null && clean)
			this.providers.clear();

		this.providers.put(key, value);

		return this;
	}

	/**
	 * @title setProviders
	 * @author zhongjyuan
	 * @description 设置提供者Map集
	 * @param providers 提供者Map集
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProviders(Map<String, IProvider> providers) {
		return setProviders(providers, false);
	}

	/**
	 * @title setProviders
	 * @author zhongjyuan
	 * @description 设置提供者Map集
	 * @param providers 提供者Map集
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setProviders(Map<String, IProvider> providers, Boolean clean) {
		if (this.providers == null || this.providers.size() < 1)
			this.providers = new HashMap<String, IProvider>();

		if (clean != null && clean)
			this.providers.clear();

		this.providers.putAll(providers);

		return this;
	}

	/**
	 * @title cleanModels
	 * @author zhongjyuan
	 * @description 清除模型对象Map集
	 * @return 上下文对象
	 * @throws
	 */
	public Context cleanModels() {
		this.models.clear();
		return this;
	}

	/**
	 * @title getModel
	 * @author zhongjyuan
	 * @description 获取模型对象
	 * @param key 键
	 * @return 模型抽象对象
	 * @throws
	 */
	public AbstractModel getModel(String key) {
		if (this.models != null && this.models.size() > 0)
			return this.models.get(key);
		return null;
	}

	/**
	 * @title getModel
	 * @author zhongjyuan
	 * @description 获取模型对象
	 * @param <Model> 模型泛型对象
	 * @param model 模型对象
	 * @return 模型泛型对象
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public <Model extends AbstractModel> Model getModel(Model model) {
		if (this.models != null && this.models.size() > 0)
			return (Model) this.models.get(model.getClass().getName());
		return null;
	}

	/**
	 * @title getModels
	 * @author zhongjyuan
	 * @description 获取模型对象Map集
	 * @return 模型对象Map集
	 * @throws
	 */
	public Map<String, AbstractModel> getModels() {
		return this.models;
	}

	/**
	 * @title setModel
	 * @author zhongjyuan
	 * @description 设置模型对象
	 * @param key 键
	 * @param value 值
	 * @return 上下文对象
	 * @throws
	 */
	public Context setModel(String key, AbstractModel value) {
		return setModel(key, value, false);
	}

	/**
	 * @title setModel
	 * @author zhongjyuan
	 * @description 设置模型对象
	 * @param key 键
	 * @param value 值
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setModel(String key, AbstractModel value, Boolean clean) {
		if (this.models == null || this.models.size() < 1)
			this.models = new HashMap<String, AbstractModel>();

		if (clean != null && clean)
			this.models.clear();

		this.models.put(key, value);

		return this;
	}

	/**
	 * @title setModels
	 * @author zhongjyuan
	 * @description 设置模型对象Map集
	 * @param models 模型对象Map集
	 * @return 上下文对象
	 * @throws
	 */
	public Context setModels(Map<String, AbstractModel> models) {
		return setModels(models, false);
	}

	/**
	 * @title setModels
	 * @author zhongjyuan
	 * @description 设置模型对象Map集
	 * @param models 模型对象Map集
	 * @param clean 清除[默认false]
	 * @return 上下文对象
	 * @throws
	 */
	public Context setModels(Map<String, AbstractModel> models, Boolean clean) {
		if (this.models == null || this.models.size() < 1)
			this.models = new HashMap<String, AbstractModel>();

		if (clean != null && clean)
			this.models.clear();

		this.models.putAll(models);

		return this;
	}
}
