package zhongjyuan.domain.exception;

import java.text.MessageFormat;

import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.response.ResponseResult;

/**
 * @className: RestException
 * @description: 请求异常对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午10:44:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RestException extends BusinessException {

	private static final long serialVersionUID = 1L;

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 */
	public RestException() {
		super();
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象 
	 * @param throwable 异常抛出对象
	 */
	public RestException(Throwable throwable) {
		super(throwable);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param message 异常消息
	 */
	public RestException(String message) {
		super(message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 */
	public RestException(Throwable throwable, String message) {
		super(throwable, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public RestException(String message, Object... args) {
		super(message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public RestException(Throwable throwable, String message, Object... args) {
		super(throwable, message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param code 编码
	 * @param message 消息
	 */
	public RestException(String code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码
	 * @param message 消息
	 */
	public RestException(Throwable throwable, String code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @Title RestException
	 * @Author zhongjyuan
	 * @Description 请求异常对象
	 * @Param @param code 响应接口对象
	 * @Throws
	 */
	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public RestException(IResponseCode code) {
		super(code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public RestException(Throwable throwable, IResponseCode code) {
		super(throwable, code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public RestException(IResponseCode code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public RestException(Throwable throwable, IResponseCode code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public RestException(IResponseCode code, Object... args) {
		super(code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public RestException(Throwable throwable, IResponseCode code, Object... args) {
		super(throwable, code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public RestException(ResponseResult<?> responseResult) {
		super(responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: RestException
	 * @author: zhongjyuan
	 * @description: 请求异常对象
	 * @param throwable 异常抛出对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public RestException(Throwable throwable, ResponseResult<?> responseResult) {
		super(throwable, responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}
}